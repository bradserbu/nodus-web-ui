export class TestDriveFakeDb {
    public static testdrive = {
        providers: [
            {
                'name': 'Amazon Web Services',
                'link': 'https://aws.amazon.com',
                'description': 'Amazon Web Services (AWS) is a subsidiary of Amazon.com that provides on-demand cloud computing platforms to individuals, companies and governments.',
                'authentication': [
                    {
                        'id': 'aws_access_key',
                        'control': 'input',
                        'label': 'Access Key',
                        'type': 'text',
                        'required': true
                    },
                    {
                        'id': 'aws_secret_key',
                        'control': 'input',
                        'label': 'Secret Key',
                        'type': 'text',
                        'required': true
                    }
                ],
                'burst_config': [
                    {
                        'id': 'aws_region',
                        'control': 'mat-select',
                        'label': 'Region',
                        'options': [
                            {
                                "displayName": "US East 1",
                                "name": "us-east-1",
                            },
                            {
                                "displayName": "US East 2",
                                "name": "us-east-2",
                            },
                            {
                                "displayName": "US West 1",
                                "name": "us-west-1",
                            },
                            {
                                "displayName": "US West 2",
                                "name": "us-west-2",
                            },
                            {
                                "displayName": "Asia Pacific South 1",
                                "name": "ap-south-1",
                            },
                            {
                                "displayName": "Asia Pacific Southeast 1",
                                "name": "ap-southeast-1",
                            },
                            {
                                "displayName": "Asia Pacific Southeast 2",
                                "name": "ap-southeast-2",
                            },
                            {
                                "displayName": "Asia Pacific Northeast 1",
                                "name": "ap-northeast-1",
                            },
                            {
                                "displayName": "Asia Pacific Northeast 2",
                                "name": "ap-northeast-2",
                            },
                            {
                                "displayName": "Canada Central 1",
                                "name": "ca-central-1",
                            },
                            {
                                "displayName": "China North 1",
                                "name": "cn-north-1",
                            },
                            {
                                "displayName": "EU Central 1",
                                "name": "eu-central-1",
                            },
                            {
                                "displayName": "EU West 1",
                                "name": "eu-west-1",
                            },
                            {
                                "displayName": "EU West 2",
                                "name": "eu-west-2",
                            },
                            {
                                "displayName": "EU West 3",
                                "name": "eu-west-3",
                            },
                            {
                                "displayName": "South America 1",
                                "name": "sa-east-1",
                            },
                            {
                                "displayName": "GovCloud (US)",
                                "name": "us-gov-west-1",
                            }
                        ]
                    }
                ],
                'instance_sizes': [
                    {
                        "displayName": "t2.nano - vCPU: 1, Mem (GB): 0.50",
                        "value": "t2.nano",
                    },
                    {
                        "displayName": "t2.micro - vCPU: 1, Mem (GB): 1",
                        "value": "t2.micro",
                    },
                    {
                        "displayName": "t2.small - vCPU: 1, Mem (GB): 2",
                        "value": "t2.small",
                    },
                    {
                        "displayName": "t2.medium - vCPU: 2, Mem (GB): 4",
                        "value": "t2.medium",
                    },
                    {
                        "displayName": "t2.large - vCPU: 2, Mem (GB): 8",
                        "value": "t2.large",
                    },
                    {
                        "displayName": "t2.xlarge - vCPU: 4, Mem (GB): 16",
                        "value": "t2.xlarge",
                    },
                    {
                        "displayName": "t2.2xlarge - vCPU: 8, Mem (GB): 32",
                        "value": "t2.2xlarge",
                    },
                    {
                        "displayName": "t3.nano - vCPU: 2, Mem (GB): 0.5",
                        "value": "t3.nano",
                    },
                    {
                        "displayName": "t3.micro - vCPU: 2, Mem (GB): 1",
                        "value": "t3.micro",
                    },
                    {
                        "displayName": "t3.small - vCPU: 2, Mem (GB): 2",
                        "value": "t3.small",
                    },
                    {
                        "displayName": "t3.medium - vCPU: 2, Mem (GB): 4",
                        "value": "t3.medium",
                    },
                    {
                        "displayName": "t3.large - vCPU: 2, Mem (GB): 8",
                        "value": "t3.large",
                    },
                    {
                        "displayName": "t3.xlarge - vCPU: 4, Mem (GB): 16",
                        "value": "t3.xlarge",
                    },
                    {
                        "displayName": "t3.2xlarge - vCPU: 8, Mem (GB): 32",
                        "value": "t3.2xlarge",
                    },
                    {
                        "displayName": "m5.large - vCPU: 2, Mem (GB): 8",
                        "value": "m5.large",
                    },
                    {
                        "displayName": "m5.xlarge - vCPU: 4, Mem (GB): 16",
                        "value": "m5.xlarge",
                    },
                    {
                        "displayName": "m5.2xlarge - vCPU: 8, Mem (GB): 32",
                        "value": "m5.2xlarge",
                    },
                    {
                        "displayName": "m5.4xlarge - vCPU: 16, Mem (GB): 64",
                        "value": "m5.4xlarge",
                    },
                    {
                        "displayName": "m5.12xlarge - vCPU: 48, Mem (GB): 192",
                        "value": "m5.12xlarge",
                    },
                    {
                        "displayName": "m5.24xlarge - vCPU: 96, Mem (GB): 384",
                        "value": "m5.24xlarge",
                    },
                    {
                        "displayName": "m5d.large - vCPU: 2, Mem (GB): 8",
                        "value": "m5d.large",
                    },
                    {
                        "displayName": "m5d.xlarge - vCPU: 4, Mem (GB): 16",
                        "value": "m5d.xlarge",
                    },
                    {
                        "displayName": "m5d.2xlarge - vCPU: 8, Mem (GB): 32",
                        "value": "m5d.2xlarge",
                    },
                    {
                        "displayName": "m5d.4xlarge - vCPU: 16, Mem (GB): 64",
                        "value": "m5d.4xlarge",
                    },
                    {
                        "displayName": "m5d.12xlarge - vCPU: 48, Mem (GB): 192",
                        "value": "m5d.12xlarge",
                    },
                    {
                        "displayName": "m5d.24xlarge - vCPU: 96, Mem (GB): 384",
                        "value": "m5d.24xlarge",
                    },
                    {
                        "displayName": "m4.large - vCPU: 2, Mem (GB): 8",
                        "value": "m4.large",
                    },
                    {
                        "displayName": "m4.xlarge - vCPU: 4, Mem (GB): 16",
                        "value": "m4.xlarge",
                    },
                    {
                        "displayName": "m4.2xlarge - vCPU: 8, Mem (GB): 32",
                        "value": "m4.2xlarge",
                    },
                    {
                        "displayName": "m4.4xlarge - vCPU: 16, Mem (GB): 64",
                        "value": "m4.4xlarge",
                    },
                    {
                        "displayName": "m4.10xlarge - vCPU: 40, Mem (GB): 160",
                        "value": "m4.10xlarge",
                    },
                    {
                        "displayName": "m4.16xlarge - vCPU: 64, Mem (GB): 256",
                        "value": "m4.16xlarge",
                    }
                ]
            },
            {
                'name': 'Google Cloud',
                'link': 'https://cloud.google.com/',
                'description': 'Google Cloud Platform, offered by Google, is a suite of cloud computing services that runs on the same infrastructure that Google uses internally for its end-user products.',
                'authentication': [],
                'burst_config': [],
                'instance_sizes': [
                    {
                        "displayName": "n1-standard-1 - vCPU: 1, Mem (GB): 3.75",
                        "value": "n1-standard-1",
                    },
                    {
                        "displayName": "n1-standard-2 - vCPU: 2, Mem (GB): 7.50",
                        "value": "n1-standard-2",
                    },
                    {
                        "displayName": "n1-standard-4 - vCPU: 4, Mem (GB): 15",
                        "value": "n1-standard-4",
                    },
                    {
                        "displayName": "n1-standard-8 - vCPU: 8, Mem (GB): 30",
                        "value": "n1-standard-8",
                    },
                    {
                        "displayName": "n1-standard-16 - vCPU: 16, Mem (GB): 60",
                        "value": "n1-standard-16",
                    },
                    {
                        "displayName": "n1-standard-32 - vCPU: 32, Mem (GB): 120",
                        "value": "n1-standard-32",
                    },
                    {
                        "displayName": "n1-standard-64 - vCPU: 64, Mem (GB): 240",
                        "value": "n1-standard-64",
                    },
                    {
                        "displayName": "n1-standard-96 - vCPU: 96, Mem (GB): 360",
                        "value": "n1-standard-96",
                    },
                    {
                        "displayName": "n1-highmem-2 - vCPU: 2, Mem (GB): 13",
                        "value": "n1-highmem-2",
                    },
                    {
                        "displayName": "n1-highmem-4 - vCPU: 4, Mem (GB): 26",
                        "value": "n1-highmem-4",
                    },
                    {
                        "displayName": "n1-highmem-8 - vCPU: 8, Mem (GB): 52",
                        "value": "n1-highmem-8",
                    },
                    {
                        "displayName": "n1-highmem-16 - vCPU: 16, Mem (GB): 104",
                        "value": "n1-highmem-16",
                    },
                    {
                        "displayName": "n1-highmem-32 - vCPU: 32, Mem (GB): 208",
                        "value": "n1-highmem-32",
                    },
                    {
                        "displayName": "n1-highmem-64 - vCPU: 64, Mem (GB): 416",
                        "value": "n1-highmem-64",
                    },
                    {
                        "displayName": "n1-highmem-96 - vCPU: 96, Mem (GB): 624",
                        "value": "n1-highmem-96",
                    },
                    {
                        "displayName": "n1-highcpu-2 - vCPU: 2, Mem (GB): 1.80",
                        "value": "n1-highcpu-2",
                    },
                    {
                        "displayName": "n1-highcpu-4 - vCPU: 4, Mem (GB): 3.60",
                        "value": "n1-highcpu-4",
                    },
                    {
                        "displayName": "n1-highcpu-8 - vCPU: 8, Mem (GB): 7.20",
                        "value": "n1-highcpu-8",
                    },
                    {
                        "displayName": "n1-highcpu-16 - vCPU: 16, Mem (GB): 14.40",
                        "value": "n1-highcpu-16",
                    },
                    {
                        "displayName": "n1-highcpu-32 - vCPU: 32, Mem (GB): 28.80",
                        "value": "n1-highcpu-32",
                    },
                    {
                        "displayName": "n1-highcpu-64 - vCPU: 64, Mem (GB): 57.60",
                        "value": "n1-highcpu-64",
                    },
                    {
                        "displayName": "n1-highcpu-96 - vCPU: 96, Mem (GB): 86.40",
                        "value": "n1-highcpu-96",
                    }
                ]
            },
            {
                'name': 'Huawei Cloud',
                'link': 'https://huaweicloud.com',
                'description': 'Huawei Cloud provides customers with stable, reliable, secure, and sustainably growing cloud services.',
                'authentication': [],
                'burst_config': [],
                'instance_sizes': [
                    {
                        "displayName": "s2.small.1 - vCPU: 1, Mem (GB): 1",
                        "value": "s2.small.1",
                    },
                    {
                        "displayName": "s2.medium.2 - vCPU: 1, Mem (GB): 2",
                        "value": "s2.medium.2",
                    },
                    {
                        "displayName": "s2.medium.4 - vCPU: 1, Mem (GB): 4",
                        "value": "s2.medium.4",
                    },
                    {
                        "displayName": "s2.large.2 - vCPU: 2, Mem (GB): 4",
                        "value": "s2.large.2",
                    },
                    {
                        "displayName": "s2.large.4 - vCPU: 2, Mem (GB): 8",
                        "value": "s2.large.4",
                    },
                    {
                        "displayName": "s2.xlarge.2 - vCPU: 4, Mem (GB): 8",
                        "value": "s2.xlarge.2",
                    },
                    {
                        "displayName": "s2.xlarge.4 - vCPU: 4, Mem (GB): 16",
                        "value": "s2.xlarge.4",
                    },
                    {
                        "displayName": "s2.2xlarge.2 - vCPU: 8, Mem (GB): 16",
                        "value": "s2.2xlarge.2",
                    },
                    {
                        "displayName": "s2.2xlarge.4 - vCPU: 8, Mem (GB): 32",
                        "value": "s2.2xlarge.4",
                    },
                    {
                        "displayName": "s2.4xlarge.2 - vCPU: 16, Mem (GB): 32",
                        "value": "s2.4xlarge.2",
                    },
                    {
                        "displayName": "s2.4xlarge.4 - vCPU: 16, Mem (GB): 64",
                        "value": "s2.4xlarge.4",
                    }
                ]
            },
            {
                'name': 'Microsoft Azure',
                'link': 'https://azure.microsoft.com',
                'description': 'Microsoft Azure (formerly Windows Azure) is a cloud computing service created by Microsoft for building, testing, deploying, and managing applications and services.',
                'authentication': [],
                'burst_config': [
                    {
                        'id': 'azure_region',
                        'control': 'mat-select',
                        'label': 'Region',
                        'options': [
                            {
                                "displayName": "East Asia",
                                "id": "/subscriptions/81e6c09c-a257-4775-ae6e-70dec06baf29/locations/eastasia",
                                "latitude": "22.267",
                                "longitude": "114.188",
                                "name": "eastasia",
                                "subscriptionId": null
                            },
                            {
                                "displayName": "Southeast Asia",
                                "id": "/subscriptions/81e6c09c-a257-4775-ae6e-70dec06baf29/locations/southeastasia",
                                "latitude": "1.283",
                                "longitude": "103.833",
                                "name": "southeastasia",
                                "subscriptionId": null
                            },
                            {
                                "displayName": "Central US",
                                "id": "/subscriptions/81e6c09c-a257-4775-ae6e-70dec06baf29/locations/centralus",
                                "latitude": "41.5908",
                                "longitude": "-93.6208",
                                "name": "centralus",
                                "subscriptionId": null
                            },
                            {
                                "displayName": "East US",
                                "id": "/subscriptions/81e6c09c-a257-4775-ae6e-70dec06baf29/locations/eastus",
                                "latitude": "37.3719",
                                "longitude": "-79.8164",
                                "name": "eastus",
                                "subscriptionId": null
                            },
                            {
                                "displayName": "East US 2",
                                "id": "/subscriptions/81e6c09c-a257-4775-ae6e-70dec06baf29/locations/eastus2",
                                "latitude": "36.6681",
                                "longitude": "-78.3889",
                                "name": "eastus2",
                                "subscriptionId": null
                            },
                            {
                                "displayName": "West US",
                                "id": "/subscriptions/81e6c09c-a257-4775-ae6e-70dec06baf29/locations/westus",
                                "latitude": "37.783",
                                "longitude": "-122.417",
                                "name": "westus",
                                "subscriptionId": null
                            },
                            {
                                "displayName": "North Central US",
                                "id": "/subscriptions/81e6c09c-a257-4775-ae6e-70dec06baf29/locations/northcentralus",
                                "latitude": "41.8819",
                                "longitude": "-87.6278",
                                "name": "northcentralus",
                                "subscriptionId": null
                            },
                            {
                                "displayName": "South Central US",
                                "id": "/subscriptions/81e6c09c-a257-4775-ae6e-70dec06baf29/locations/southcentralus",
                                "latitude": "29.4167",
                                "longitude": "-98.5",
                                "name": "southcentralus",
                                "subscriptionId": null
                            },
                            {
                                "displayName": "North Europe",
                                "id": "/subscriptions/81e6c09c-a257-4775-ae6e-70dec06baf29/locations/northeurope",
                                "latitude": "53.3478",
                                "longitude": "-6.2597",
                                "name": "northeurope",
                                "subscriptionId": null
                            },
                            {
                                "displayName": "West Europe",
                                "id": "/subscriptions/81e6c09c-a257-4775-ae6e-70dec06baf29/locations/westeurope",
                                "latitude": "52.3667",
                                "longitude": "4.9",
                                "name": "westeurope",
                                "subscriptionId": null
                            },
                            {
                                "displayName": "Japan West",
                                "id": "/subscriptions/81e6c09c-a257-4775-ae6e-70dec06baf29/locations/japanwest",
                                "latitude": "34.6939",
                                "longitude": "135.5022",
                                "name": "japanwest",
                                "subscriptionId": null
                            },
                            {
                                "displayName": "Japan East",
                                "id": "/subscriptions/81e6c09c-a257-4775-ae6e-70dec06baf29/locations/japaneast",
                                "latitude": "35.68",
                                "longitude": "139.77",
                                "name": "japaneast",
                                "subscriptionId": null
                            },
                            {
                                "displayName": "Brazil South",
                                "id": "/subscriptions/81e6c09c-a257-4775-ae6e-70dec06baf29/locations/brazilsouth",
                                "latitude": "-23.55",
                                "longitude": "-46.633",
                                "name": "brazilsouth",
                                "subscriptionId": null
                            },
                            {
                                "displayName": "Australia East",
                                "id": "/subscriptions/81e6c09c-a257-4775-ae6e-70dec06baf29/locations/australiaeast",
                                "latitude": "-33.86",
                                "longitude": "151.2094",
                                "name": "australiaeast",
                                "subscriptionId": null
                            },
                            {
                                "displayName": "Australia Southeast",
                                "id": "/subscriptions/81e6c09c-a257-4775-ae6e-70dec06baf29/locations/australiasoutheast",
                                "latitude": "-37.8136",
                                "longitude": "144.9631",
                                "name": "australiasoutheast",
                                "subscriptionId": null
                            },
                            {
                                "displayName": "South India",
                                "id": "/subscriptions/81e6c09c-a257-4775-ae6e-70dec06baf29/locations/southindia",
                                "latitude": "12.9822",
                                "longitude": "80.1636",
                                "name": "southindia",
                                "subscriptionId": null
                            },
                            {
                                "displayName": "Central India",
                                "id": "/subscriptions/81e6c09c-a257-4775-ae6e-70dec06baf29/locations/centralindia",
                                "latitude": "18.5822",
                                "longitude": "73.9197",
                                "name": "centralindia",
                                "subscriptionId": null
                            },
                            {
                                "displayName": "West India",
                                "id": "/subscriptions/81e6c09c-a257-4775-ae6e-70dec06baf29/locations/westindia",
                                "latitude": "19.088",
                                "longitude": "72.868",
                                "name": "westindia",
                                "subscriptionId": null
                            },
                            {
                                "displayName": "Canada Central",
                                "id": "/subscriptions/81e6c09c-a257-4775-ae6e-70dec06baf29/locations/canadacentral",
                                "latitude": "43.653",
                                "longitude": "-79.383",
                                "name": "canadacentral",
                                "subscriptionId": null
                            },
                            {
                                "displayName": "Canada East",
                                "id": "/subscriptions/81e6c09c-a257-4775-ae6e-70dec06baf29/locations/canadaeast",
                                "latitude": "46.817",
                                "longitude": "-71.217",
                                "name": "canadaeast",
                                "subscriptionId": null
                            },
                            {
                                "displayName": "UK South",
                                "id": "/subscriptions/81e6c09c-a257-4775-ae6e-70dec06baf29/locations/uksouth",
                                "latitude": "50.941",
                                "longitude": "-0.799",
                                "name": "uksouth",
                                "subscriptionId": null
                            },
                            {
                                "displayName": "UK West",
                                "id": "/subscriptions/81e6c09c-a257-4775-ae6e-70dec06baf29/locations/ukwest",
                                "latitude": "53.427",
                                "longitude": "-3.084",
                                "name": "ukwest",
                                "subscriptionId": null
                            },
                            {
                                "displayName": "West Central US",
                                "id": "/subscriptions/81e6c09c-a257-4775-ae6e-70dec06baf29/locations/westcentralus",
                                "latitude": "40.890",
                                "longitude": "-110.234",
                                "name": "westcentralus",
                                "subscriptionId": null
                            },
                            {
                                "displayName": "West US 2",
                                "id": "/subscriptions/81e6c09c-a257-4775-ae6e-70dec06baf29/locations/westus2",
                                "latitude": "47.233",
                                "longitude": "-119.852",
                                "name": "westus2",
                                "subscriptionId": null
                            },
                            {
                                "displayName": "Korea Central",
                                "id": "/subscriptions/81e6c09c-a257-4775-ae6e-70dec06baf29/locations/koreacentral",
                                "latitude": "37.5665",
                                "longitude": "126.9780",
                                "name": "koreacentral",
                                "subscriptionId": null
                            },
                            {
                                "displayName": "Korea South",
                                "id": "/subscriptions/81e6c09c-a257-4775-ae6e-70dec06baf29/locations/koreasouth",
                                "latitude": "35.1796",
                                "longitude": "129.0756",
                                "name": "koreasouth",
                                "subscriptionId": null
                            },
                            {
                                "displayName": "France Central",
                                "id": "/subscriptions/81e6c09c-a257-4775-ae6e-70dec06baf29/locations/francecentral",
                                "latitude": "46.3772",
                                "longitude": "2.3730",
                                "name": "francecentral",
                                "subscriptionId": null
                            },
                            {
                                "displayName": "France South",
                                "id": "/subscriptions/81e6c09c-a257-4775-ae6e-70dec06baf29/locations/francesouth",
                                "latitude": "43.8345",
                                "longitude": "2.1972",
                                "name": "francesouth",
                                "subscriptionId": null
                            },
                            {
                                "displayName": "Australia Central",
                                "id": "/subscriptions/81e6c09c-a257-4775-ae6e-70dec06baf29/locations/australiacentral",
                                "latitude": "-35.3075",
                                "longitude": "149.1244",
                                "name": "australiacentral",
                                "subscriptionId": null
                            },
                            {
                                "displayName": "Australia Central 2",
                                "id": "/subscriptions/81e6c09c-a257-4775-ae6e-70dec06baf29/locations/australiacentral2",
                                "latitude": "-35.3075",
                                "longitude": "149.1244",
                                "name": "australiacentral2",
                                "subscriptionId": null
                            }
                        ]
                    }
                ],
                'instance_sizes': [
                    {
                        "displayName": "Standard_DS1_v2 - vCPU: 1, Mem (GB): 3.50",
                        "value": "Standard_DS1_v2",
                    },
                    {
                        "displayName": "Standard_DS2_v2 - vCPU: 2, Mem (GB): 7",
                        "value": "Standard_DS2_v2",
                    },
                    {
                        "displayName": "Standard_DS3_v2 - vCPU: 4, Mem (GB): 14",
                        "value": "Standard_DS3_v2",
                    },
                    {
                        "displayName": "Standard_DS4_v2 - vCPU: 8, Mem (GB): 28",
                        "value": "Standard_DS4_v2",
                    },
                    {
                        "displayName": "Standard_DS5_v2 - vCPU: 16, Mem (GB): 56",
                        "value": "Standard_DS5_v2",
                    }
                ]
            },
            {
                'name': 'Open Telekom Cloud',
                'link': 'https://otc.t-systems.com',
                'description': 'Open Telekom Cloud provides compute, storage, network and security services from a public cloud – available at short notice, and scalable to your needs.',
                'authentication': [],
                'burst_config': [],
                'instance_sizes': [
                    {
                        "displayName": "s2.small.1 - vCPU: 1, Mem (GB): 1",
                        "value": "s2.small.1",
                    },
                    {
                        "displayName": "s2.medium.2 - vCPU: 1, Mem (GB): 2",
                        "value": "s2.medium.2",
                    },
                    {
                        "displayName": "s2.medium.4 - vCPU: 1, Mem (GB): 4",
                        "value": "s2.medium.4",
                    },
                    {
                        "displayName": "s2.large.2 - vCPU: 2, Mem (GB): 4",
                        "value": "s2.large.2",
                    },
                    {
                        "displayName": "s2.large.4 - vCPU: 2, Mem (GB): 8",
                        "value": "s2.large.4",
                    },
                    {
                        "displayName": "s2.xlarge.2 - vCPU: 4, Mem (GB): 8",
                        "value": "s2.xlarge.2",
                    },
                    {
                        "displayName": "s2.xlarge.4 - vCPU: 4, Mem (GB): 16",
                        "value": "s2.xlarge.4",
                    },
                    {
                        "displayName": "s2.2xlarge.2 - vCPU: 8, Mem (GB): 16",
                        "value": "s2.2xlarge.2",
                    },
                    {
                        "displayName": "s2.2xlarge.4 - vCPU: 8, Mem (GB): 32",
                        "value": "s2.2xlarge.4",
                    },
                    {
                        "displayName": "s2.4xlarge.2 - vCPU: 16, Mem (GB): 32",
                        "value": "s2.4xlarge.2",
                    },
                    {
                        "displayName": "s2.4xlarge.4 - vCPU: 16, Mem (GB): 64",
                        "value": "s2.4xlarge.4",
                    }
                ]
            },
            {
                'name': 'Oracle Cloud',
                'link': 'https://cloud.oracle.com',
                'description': 'Oracle Cloud is a cloud computing service offered by Oracle Corporation providing servers, storage, network, applications and services through a global network.',
                'authentication': [],
                'burst_config': [],
                'instance_sizes': [
                    {
                        "displayName": "VM.Standard2.1 - vCPU: 1, Mem (GB): 15",
                        "value": "VM.Standard2.1",
                    },
                    {
                        "displayName": "VM.Standard2.2 - vCPU: 2, Mem (GB): 30",
                        "value": "VM.Standard2.2",
                    },
                    {
                        "displayName": "VM.Standard2.4 - vCPU: 4, Mem (GB): 60",
                        "value": "VM.Standard2.4",
                    },
                    {
                        "displayName": "VM.Standard2.8 - vCPU: 8, Mem (GB): 120",
                        "value": "VM.Standard2.8",
                    },
                    {
                        "displayName": "VM.Standard2.16 - vCPU: 16, Mem (GB): 240",
                        "value": "VM.Standard2.16",
                    },
                    {
                        "displayName": "VM.Standard2.24 - vCPU: 24, Mem (GB): 320",
                        "value": "VM.Standard2.24",
                    }
                ]
            }/*,
            {
                'name': 'T-Systems Cloud',
                'link': 'https://cloud.t-systems.com',
                'description': 'T-Systems is a German global IT services and consulting company headquartered in Frankfurt. Founded in 2000, it is a subsidiary of Deutsche Telekom.',
                'authentication': [],
                'burst_config': []
            }*/
        ],
        jobs: [
            {
                'name': 'Custom Job',
                'subtitle': 'Configurable',
                'description': 'Upload or enter your custom job script(s) to submit to the queue.',
                'type': '',
                'script': '',
                'icons': []
            },
            {
                'name': 'Test Job',
                'subtitle': 'Default',
                'description': 'This is a simple 30 seconds sleep job, useful for testing and demonstrating basic cloud bursting functionality.',
                'type': 'script_text',
                'script': 'echo sleep 30',
                'icons': []
            },
            {
                'name': 'MPI Benchmarks',
                'subtitle': 'Performance Testing',
                'description': 'This job tests the performance of a cluster system, including node performance, network latency, and throughput.',
                'type': 'script_text',
                'script': '',
                'icons': [
                    {
                        "src": "assets/images/logos/openmpi.png",
                        "alt": "openmpi"
                    }
                ]
            }/*
            {
                'name': 'Job Array',
                'subtitle': 'Configurable',
                'description':'Easily submit a high number of identical jobs operating on different data sets.',
                'type': '',
                'script': '',
                'icons': []
            },*/
            /*,
            {
                'name': 'Moab Job',
                'subtitle': 'Configurable',
                'description': 'Use Moab\'s native application templates to create and configure your HPC jobs.',
                'type': '',
                'script': '',
                'icons': []
            }*/
        ],
        templates: [
            {
                'name': 'Free Form',
                'subtitle': 'Default',
                'description': 'This is the free form template from Moab, useful for creating a job from scratch.',
                'type': 'script_text',
                'script': 'echo sleep 30',
                'icons': []
            },
            {
                'name': 'Container Form',
                'subtitle': 'Container Default',
                'description': 'This form provides a template for running jobs inside a Docker container.',
                'type': 'script_text',
                'script': '',
                'icons': [
                    {
                        "src": "assets/images/logos/openmpi.png",
                        "alt": "openmpi"
                    }
                ]
            }
        ],
        stackOptions: [
            {
                'displayName': 'Apache',
                'value': 'apache',
                'icon': 'assets/images/packages/apache.png'
            },
            {
                'displayName': 'Nginx',
                'value': 'nginx',
                'icon': 'assets/images/packages/nginx.png'
            },
            {
                'displayName': 'Python',
                'value': 'python',
                'icon': 'assets/images/packages/python.png'
            }
        ]
    };
}

import { InMemoryDbService } from 'angular-in-memory-web-api';
import { TestDriveFakeDb } from "./testdrive";

export class DbService implements InMemoryDbService
{
    createDb(): any
    {
        return {
            // Test Drive
            'test-drive' : TestDriveFakeDb.testdrive,
        };
    }
}

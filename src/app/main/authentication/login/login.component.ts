import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import {FuseConfigService} from '@fuse/services/config.service';
import {fuseAnimations} from '@fuse/animations';
import {AuthenticationService} from "../authentication.service";
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {MatSnackBar} from '@angular/material';

import {environment} from "../../../../environments/environment";

@Component({
    selector: 'login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: fuseAnimations
})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;

    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        public snackBar: MatSnackBar,
        private _fuseConfigService: FuseConfigService,
        private _formBuilder: FormBuilder,
        private _authenticationService: AuthenticationService,
        private router: Router,
        private _httpClient: HttpClient
    ) {
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar: {
                    hidden: true
                },
                toolbar: {
                    hidden: true
                },
                footer: {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.loginForm = this._formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        });
    }


    login(): void {
        const user = {
            'username': this.loginForm.value.username,
            'password': this.loginForm.value.password
        };

        this._httpClient.post(`${environment.apiURL}/login`, user)
            .subscribe((response: any) => {
                this._authenticationService.setToken(response.data);
                this.router.navigateByUrl('');
            }, error => {
                if (typeof error.error.error != 'undefined') {
                    this.showNotification('You have entered an invalid username or password.', 60000, 'warning');
                }
                else
                    this.showNotification('ERROR: Login was Unsuccessful.', 60000, 'warning');
            });
    }

    showNotification(message: string, duration: number, type: string): void {
        if (localStorage.getItem('NOTIFICATION') == 'true') {
            this.snackBar.open(message, 'Dismiss', {
                duration: duration,
                panelClass: [type],
                horizontalPosition: 'end',
                verticalPosition: 'bottom'
            });
        }
    }
}

import {Component, OnDestroy, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators} from '@angular/forms';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {FuseConfigService} from '@fuse/services/config.service';
import {fuseAnimations} from '@fuse/animations';
import {HttpClient} from '@angular/common/http';
import {AuthenticationService} from "../authentication.service";
import {Router} from '@angular/router';

import {MatSnackBar} from '@angular/material';

import {environment} from "../../../../environments/environment";

@Component({
    selector: 'register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss'],
    animations: fuseAnimations
})
export class RegisterComponent implements OnInit, OnDestroy {
    registerForm: FormGroup;

    // Private
    private _unsubscribeAll: Subject<any>;

    constructor(
        public snackBar: MatSnackBar,
        private _fuseConfigService: FuseConfigService,
        private _formBuilder: FormBuilder,
        private _httpClient: HttpClient,
        private _authenticationService: AuthenticationService,
        private router: Router
    ) {
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar: {
                    hidden: true
                },
                toolbar: {
                    hidden: true
                },
                footer: {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.registerForm = this._formBuilder.group({
            username: ['', [Validators.required, usernameValidator]],
            name: ['', Validators.required],
            email: ['', [Validators.required, Validators.email]],
            company: ['', Validators.required],
            password: ['', Validators.required],
            passwordConfirm: ['', [Validators.required, confirmPasswordValidator]]
        });

        // Update the validity of the 'passwordConfirm' field
        // when the 'password' field changes
        this.registerForm.get('password').valueChanges
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(() => {
                this.registerForm.get('passwordConfirm').updateValueAndValidity();
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    register(): void {
        const user = {
            'username': this.registerForm.value.username,
            'name': this.registerForm.value.name,
	    'email': this.registerForm.value.email,
            'company': this.registerForm.value.company,
            'password': this.registerForm.value.password
        };

        this._httpClient.post(`${environment.apiURL}/registerUser`, user)
            .subscribe((response: any) => {
                this.showNotification('User account ' + user.username + ' was successfully created on the system.', 5000, 'notification');
                this.router.navigateByUrl('/auth/login');
            }, error => {
                if (typeof error.error.error != 'undefined') {
                    if ((error.error.error.message).includes("already exists"))
                        this.showNotification('Username "' + user.username + '" already exists. Please change the username and try again.', 60000, 'warning');
                }
                else
                    this.showNotification('ERROR: Registration was Unsuccessful.', 60000, 'warning');
            });
    }

    showNotification(message: string, duration: number, type: string): void {
        if (localStorage.getItem('NOTIFICATION') == 'true') {
            this.snackBar.open(message, 'Dismiss', {
                duration: duration,
                panelClass: [type],
                horizontalPosition: 'end',
                verticalPosition: 'bottom'
            });
        }
    }
}

/**
 * Confirm password validator
 *
 * @param {AbstractControl} control
 * @returns {ValidationErrors | null}
 */
export const confirmPasswordValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {

    if (!control.parent || !control) {
        return null;
    }

    const password = control.parent.get('password');
    const passwordConfirm = control.parent.get('passwordConfirm');

    if (!password || !passwordConfirm) {
        return null;
    }

    if (passwordConfirm.value === '') {
        return null;
    }

    if (password.value === passwordConfirm.value) {
        return null;
    }

    return {'passwordsNotMatching': true};
};

export const usernameValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
    if (!control.parent || !control)
        return null;

    const controlPattern = ['?', '!', '@', '(', ')', '[', ']', '{', '}', ',', ';', '=', '<', '>', '|', '+', '*', '#', '"', '$', '%', '&', '§', '~'];
    const username = control.parent.get('username').value || '';
    let isValid = username.split(" ").length === 1;

    var hasSpecialCharacter = 0;
    controlPattern.forEach(function(character) {
        hasSpecialCharacter = hasSpecialCharacter + username.includes(character);
    });
    isValid = isValid && (hasSpecialCharacter === 0);
    return isValid ? null : {'whitespace': true}
};

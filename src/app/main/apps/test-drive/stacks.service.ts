import {Injectable} from '@angular/core';
import {BehaviorSubject, Subject} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {MatSnackBar} from '@angular/material';

import {environment} from "../../../../environments/environment";

@Injectable()
export class StacksService {
    stacks: any;
    stackList: any;
    stackListAvailable: [];
    stackListDeployed: [];
    onStacksChanged: BehaviorSubject<any>;
    onStackListUpdated: BehaviorSubject<any>;
    onSearchTextChanged: Subject<any>;
    onFilterChanged: Subject<any>;

    searchText: string;
    filterBy: string;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        public snackBar: MatSnackBar,
        private _httpClient: HttpClient
    ) {
        this.onStacksChanged = new BehaviorSubject<any>([]);
        this.onStackListUpdated = new BehaviorSubject<any>([]);
        this.onSearchTextChanged = new Subject();
        this.onSearchTextChanged.subscribe(searchText => {
            this.searchText = searchText;
        });
        this.onFilterChanged = new Subject();
        this.onFilterChanged.subscribe(filter => {
            this.filterBy = filter;
        });
        this.getStacks();
        this.updateStackList();
    }

    /**
     * getStacks
     */
    getStacks(): Promise<any> {
        return new Promise((resolve, reject) => {
            const user = {
                'user_token': localStorage.getItem('TOKEN')
            };

            this._httpClient.post(`${environment.apiURL}/getStacks`, user)
                .subscribe((response: any) => {
                    this.stacks = response.data;
                    this.onStacksChanged.next(this.stacks);
                    resolve(this.stacks);
                }, error => {
                    if (error.error.error) {
                        if (error.error.error.message == "invalid token") {
                            localStorage.removeItem('TOKEN');
                            window.location.reload();
                        }
                    }
                }, reject);
        });
    }

    updateStackList(): Promise<any> {
        return new Promise((resolve, reject) => {
            const user = {
                'user_token': localStorage.getItem('TOKEN')
            };

            this._httpClient.post(`${environment.apiURL}/getStacks`, user)
                .subscribe((response: any) => {
                    this.stackList = response.data;

                    this.stackListAvailable = [];
                    this.stackListDeployed = [];

                    this.stackList.forEach(function (stack) {
                        switch (stack.state) {
                            case 'Available':
                                this.stackListAvailable.push(stack);
                                break;
                            case 'Deployed':
                                this.stackListDeployed.push(stack);
                                break;
                            default:
                                break;
                        }
                    }.bind(this));

                    const stacks = {
                        'stackList': this.stackList,
                        'stackListAvailable': this.stackListAvailable,
                        'stackListDeployed': this.stackListDeployed
                    };

                    this.onStackListUpdated.next(stacks);
                    resolve(this.stackList);
                }, error => {
                    if (error.error.error) {
                        if (error.error.error.message == "invalid token") {
                            localStorage.removeItem('TOKEN');
                            window.location.reload();
                        }
                    }
                }, reject);
        });
    }

    showNotification(message: string, duration: number, type: string): void {
        this.snackBar.open(message, 'Dismiss', {
            duration: duration,
            panelClass: [type],
            horizontalPosition: 'end',
            verticalPosition: 'bottom'
        });
    }

    logActivity(activity_type, activity_title, activity_icon, message) {
        const logActivity = {
            'user_token': localStorage.getItem('TOKEN'),
            'activity': {
                'type': activity_type,
                'title': {
                    'name': activity_title,
                    'icon': activity_icon
                },
                'message': message
            }
        };

        this._httpClient.post(`${environment.apiURL}/logActivity`, logActivity)
            .subscribe((response: any) => {
            });
    }
}
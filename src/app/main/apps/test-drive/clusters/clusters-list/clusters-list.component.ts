import {Component, OnDestroy, OnInit, ViewEncapsulation, ViewChild} from '@angular/core';
import {Subject} from 'rxjs';
import {fuseAnimations} from "../../../../../../@fuse/animations";
import {ClustersService} from "../../clusters.services";
import {takeUntil} from 'rxjs/operators';
import {MatSort, MatTableDataSource, MatDialog, MatDialogRef, MatSnackBar} from '@angular/material';
import {HttpClient} from '@angular/common/http';
import {FuseUtils} from '@fuse/utils';
import {Router} from '@angular/router';
import {FuseConfirmDialogComponent} from "../../../../../../@fuse/components/confirm-dialog/confirm-dialog.component";
import {animate, state, style, transition, trigger} from '@angular/animations';

import {LogsDialogComponent} from "../../dialogs/logs-dialog/logs-dialog.component";

@Component({
    selector: 'clusters-list',
    templateUrl: './clusters-list.component.html',
    styleUrls: ['./clusters-list.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: [
        trigger('detailExpand', [
            state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
            state('expanded', style({height: '*'})),
            transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)'))
        ]),
        fuseAnimations
    ]
})

export class ClustersListComponent implements OnInit, OnDestroy {
    displayedColumns = ['icon', 'name', 'provider', 'size', 'state', 'buttons'];
    displayedNodeColumns = ['counter', 'hostname', 'cores', 'state', 'status'];
    dataSource: MatTableDataSource<ClusterData>;
    dataSourceNodes: MatTableDataSource<any>;

    allClusters: ClusterData[] = [];
    clusters_aws: ClusterData[] = [];
    clusters_google: ClusterData[] = [];
    clusters_azure: ClusterData[] = [];
    clusters_oracle: ClusterData[] = [];
    clusters_huawei: ClusterData[] = [];
    clusters_otc: ClusterData[] = [];
    selectedClusters: ClusterData[] = [];
    clusterDetails: ClusterData;
    searchText: string;
    filterBy: string;

    dialogRef: any;
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;

    @ViewChild(MatSort) sort: MatSort;

    private _unsubscribeAll: Subject<any>;

    constructor(
        private _matDialog: MatDialog,
        private _clustersService: ClustersService,
        private _httpClient: HttpClient,
        private router: Router,
        public snackBar: MatSnackBar
    ) {
        this.clusterDetails = {
            'id': '',
            'name': '',
            'state': ''
        };
        this.filterBy = "all";
        this._unsubscribeAll = new Subject();
        this.dataSource = new MatTableDataSource(this.selectedClusters);
    }

    ngOnInit(): void {
        //this.sort.sort(<MatSortable>({id: 'name', start: 'asc'}

        this._clustersService.onClusterListUpdated
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((clusters) => {
                this.allClusters = clusters.clusterList;
                this.clusters_aws = clusters.clusterListAws;
                this.clusters_google = clusters.clusterListGoogle;
                this.clusters_azure = clusters.clusterListAzure;
                this.clusters_oracle = clusters.clusterListOracle;
                this.clusters_huawei = clusters.clusterListHuawei;
                this.clusters_otc = clusters.clusterListOtc;
                this.updateClustersSelection();
            });

        this._clustersService.onSearchTextChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((searchText) => {
                this.searchText = searchText;
                this.updateClustersSelection();
            });

        this._clustersService.onFilterChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((filter) => {
                this.filterBy = filter;
                this.updateClustersSelection();
            });

        /*if (this.allClusters.length == 0) {
            this.dialogRef = this._matDialog.open(WelcomeDialogComponent, {
                panelClass: 'welcome-dialog',
                data: {
                    action: 'setup'
                },
                disableClose: true
            });
        }*/
    }

    ngOnDestroy(): void {
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    openClusterDashboard(cluster): void {
        if (cluster.state == 'Online') {
            this._clustersService.updateClusterSelection(cluster);
            this.router.navigateByUrl('/apps/nodus-cloud-platform');
        }
    }

    openNodeDashboard(node): void {
        if(node.state == 'Online') {
            this.router.navigateByUrl('/apps/nodus-cloud-platform/nodes/details');
        }
    }

    updateClustersSelection() {
        switch (this.filterBy) {
            case 'all':
                this.selectedClusters = this.allClusters;
                break;
            case 'Amazon Web Services':
                this.selectedClusters = this.clusters_aws;
                break;
            case 'Google Cloud':
                this.selectedClusters = this.clusters_google;
                break;
            case 'Microsoft Azure':
                this.selectedClusters = this.clusters_azure;
                break;
            case 'Oracle Cloud':
                this.selectedClusters = this.clusters_oracle;
                break;
            case 'Huawei Cloud':
                this.selectedClusters = this.clusters_huawei;
                break;
            case 'Open Telekom Cloud':
                this.selectedClusters = this.clusters_otc;
                break;
            default:
                this.selectedClusters = this.allClusters;
                break;
        }

        if (this.searchText && this.searchText !== '') {
            this.selectedClusters = FuseUtils.filterArrayByString(this.selectedClusters, this.searchText);
        }

        this.dataSource.data = this.selectedClusters;
        this.dataSource.sort = this.sort;
    }

    redeployCluster(cluster) {
        this._clustersService.redeployCluster(cluster);
        this.showNotification("The cluster is being redeployed.", 5000, 'notification');
    }

    destroyCluster(cluster) {
        this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to completely destroy the cluster? Please make sure to save any job output data from the cluster before destroying it.';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                this._clustersService.destroyCluster(cluster);
                this.showNotification("The cluster is being destroyed.", 5000, 'notification');
            }
            this.confirmDialogRef = null;
        });
    }

    getClusterDeployLogs(cluster) {
        this.dialogRef = this._matDialog.open(LogsDialogComponent, {
            panelClass: 'logs-dialog',
            data: {
                cluster: cluster
            }
        });
    }

    downloadClusterKey(cluster) {
        this._clustersService.downloadClusterKey(cluster);
    }

    updateClusterList() {
        this._clustersService.updateClusterList();
    }

    showClusterDetails(cluster) {
        const nodes = [
            {
                'hostname': "aws-cluster-head-node",
                'cores': 4,
                'state': "Online",
                'status': "Busy"
            },
            {
                'hostname': "aws-cluster-compute-node-1",
                'cores': 2,
                'state': "Provisioning",
                'status': "-"
            }
        ];
        if (this.clusterDetails.id == cluster.id)
            this.clusterDetails = {
                'id': '',
                'name': '',
                'state': ''
            };
        else {
            this.clusterDetails = cluster;
            this.dataSourceNodes = new MatTableDataSource(nodes);
        }
    }

    showNotification(message: string, duration: number, type: string): void {
        this.snackBar.open(message, 'Dismiss', {
            duration: duration,
            panelClass: [type],
            horizontalPosition: 'end',
            verticalPosition: 'bottom'
        });
    }
}

export interface ClusterData {
    id: string;
    name: string;
    state: string;
}
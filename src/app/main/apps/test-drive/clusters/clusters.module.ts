import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ClustersComponent} from "./clusters.component";
import {ClustersListComponent} from "./clusters-list/clusters-list.component";
import {ClustersSidebarComponent} from "./clusters-sidebar/clusters-sidebar.component";
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { CommonModule } from '@angular/common';
import {FlexLayoutModule} from '@angular/flex-layout'

import {
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatFormFieldModule,
    MatTableModule,
    MatCardModule,
    MatDialogModule,
    MatMenuModule,
    MatSnackBarModule,
    MatSortModule,
    MatTooltipModule
} from '@angular/material';

const routes: Routes = [
    {
        path: '**',
        component: ClustersComponent
    }
];

@NgModule({
    declarations: [
        ClustersComponent,
        ClustersListComponent,
        ClustersSidebarComponent
    ],
    imports: [
        RouterModule.forChild(routes),

        FlexLayoutModule,

        FormsModule,
        ReactiveFormsModule,
        CommonModule,

        MatButtonModule,
        MatIconModule,
        MatInputModule,
        MatFormFieldModule,
        MatTableModule,
        MatCardModule,
        MatDialogModule,
        MatMenuModule,
        MatSnackBarModule,
        MatSortModule,
        MatTooltipModule
    ]
})
export class ClustersModule
{

}
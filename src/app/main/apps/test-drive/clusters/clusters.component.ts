import {Component, ViewEncapsulation, OnInit, OnDestroy} from '@angular/core';
import {fuseAnimations} from '@fuse/animations';
import {FormControl} from '@angular/forms';
import {Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged, takeUntil} from 'rxjs/operators';
import {ClustersService} from "../clusters.services";
import {Router} from '@angular/router';
import {MatDialog} from '@angular/material';
import {CloudProviderFormDialogComponent} from "../dialogs/cloud-provider-form/cloud-provider-form.component";
import {SubmitJobFormDialogComponent} from "../dialogs/submit-job-form/submit-job-form.component";


/**
 * @title Data table with sorting, pagination, and filtering.
 */
@Component({
    selector: 'clusters',
    templateUrl: './clusters.component.html',
    styleUrls: ['./clusters.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})

export class ClustersComponent implements OnInit, OnDestroy{
    searchInput: FormControl;

    dialogRef: any;

    private _unsubscribeAll: Subject<any>;

    constructor(
        private _matDialog: MatDialog,
        private _clustersService: ClustersService,
        private router: Router
    ) {
        this.searchInput = new FormControl('');
        this._unsubscribeAll = new Subject();
    }

    ngOnInit(): void {
        this.searchInput.valueChanges
            .pipe(takeUntil(this._unsubscribeAll),
                debounceTime(300),
                distinctUntilChanged()
            )
            .subscribe(searchText => {
                this._clustersService.onSearchTextChanged.next(searchText);
            });
    }

    openDashboard(): void {
        this.router.navigateByUrl('/apps/nodus-cloud-platform');
    }

    openStackManagement(): void {
        this.router.navigateByUrl('/apps/nodus-cloud-platform/stacks');
    }

    createNewCluster(): void {
        this.dialogRef = this._matDialog.open(CloudProviderFormDialogComponent, {
            panelClass: 'cloud-provider-form-dialog',
            data: {
                action: 'new'
            }
        });
    }

    submitJob(): void {
        this.dialogRef = this._matDialog.open(SubmitJobFormDialogComponent, {
            panelClass: 'submit-job-form-dialog',
            data: {
                action: 'new'
            }
        });
    }

    ngOnDestroy(): void {
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

}

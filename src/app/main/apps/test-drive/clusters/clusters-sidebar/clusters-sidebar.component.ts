import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {ClustersService} from "../../clusters.services";

@Component({
    selector: 'clusters-sidebar',
    templateUrl: './clusters-sidebar.component.html',
    styleUrls: ['./clusters-sidebar.component.scss']
})
export class ClustersSidebarComponent implements OnInit, OnDestroy {
    filterBy: string;

    all_cluster_count: any;
    aws_cluster_count: any;
    google_cluster_count: any;
    azure_cluster_count: any;
    oracle_cluster_count: any;
    huawei_cluster_count: any;
    otc_cluster_count: any;

    private _unsubscribeAll: Subject<any>;

    constructor(
        private _clustersService: ClustersService
    ) {
        this.filterBy = 'all';
        this._unsubscribeAll = new Subject();
    }

    ngOnInit(): void {
        this._clustersService.onClusterListUpdated
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((clusters) => {
                if (clusters.clusterList) {
                    this.all_cluster_count = clusters.clusterList.length;
                    this.aws_cluster_count = clusters.clusterListAws.length;
                    this.google_cluster_count = clusters.clusterListGoogle.length;
                    this.azure_cluster_count = clusters.clusterListAzure.length;
                    this.oracle_cluster_count = clusters.clusterListOracle.length;
                    this.huawei_cluster_count = clusters.clusterListHuawei.length;
                    this.otc_cluster_count = clusters.clusterListOtc.length;
                }
            });
    }

    ngOnDestroy(): void {
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    changeFilter(filter): void {
        this.filterBy = filter;
        this._clustersService.onFilterChanged.next(this.filterBy);
    }


}
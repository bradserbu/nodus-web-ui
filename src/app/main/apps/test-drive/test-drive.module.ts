import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {
    MatButtonModule,
    MatTooltipModule,
    MatProgressSpinnerModule,
    MatCheckboxModule,
    MatMenuModule,
    MatPaginatorModule,
    MatSnackBarModule,
    MatTableModule,
    MatDatepickerModule,
    MatDialogModule,
    MatRadioModule,
    MatCardModule,
    MatFormFieldModule,
    MatSelectModule,
    MatToolbarModule,
    MatInputModule,
    MatSlideToggleModule,
    MatDividerModule,
    MatIconModule,
    MatTabsModule,
    MatProgressBarModule,
    MatListModule,
    MatSliderModule,
    MatStepperModule
} from '@angular/material';

import {ColorPickerModule} from 'ngx-color-picker'
import {FuseSharedModule} from '@fuse/shared.module';
import {FuseWidgetModule} from '@fuse/components/widget/widget.module';

import {ChartsModule} from 'ng2-charts';
import {NgxChartsModule} from '@swimlane/ngx-charts';

import {TestDriveService} from './test-drive.service';
import {TestDriveComponent} from './test-drive.component';
import {DashboardComponent} from './tabs/home/home.component';
import {ConfigurationComponent} from './tabs/configuration/configuration.component';
import {WorkloadComponent} from "./tabs/workload/workload.component";
import {WorkloadListComponent} from "./tabs/workload/workload-list/workload-list.component";
import {WorkloadSidebarComponent} from "./tabs/workload/workload-sidebar/workload-sidebar.component";
import {NodesComponent} from "./tabs/nodes/nodes.component";
import {NodesListComponent} from "./tabs/nodes/nodes-list/nodes-list.component";
import {NodesSidebarComponent} from "./tabs/nodes/nodes-sidebar/nodes-sidebar.component";
import {WelcomeDialogComponent} from "./dialogs/welcome-form/welcome-form.component";
import {TaskProgressComponent} from "./dialogs/task-progress/task-progress.component";
import {CloudProviderFormDialogComponent} from './dialogs/cloud-provider-form/cloud-provider-form.component';
import {SubmitJobFormDialogComponent} from "./dialogs/submit-job-form/submit-job-form.component";
import {JobDetailsFormDialogComponent} from "./dialogs/job-details-form/job-details-form.component";
import {StackDetailsFormComponent} from "./dialogs/stack-details-form/stack-details-form.component";
import {FuseConfirmDialogComponent} from "../../../../@fuse/components/confirm-dialog/confirm-dialog.component";
import {LogsDialogComponent} from "./dialogs/logs-dialog/logs-dialog.component";

import {StacksComponent} from "./tabs/stacks/stacks.component";
import {StacksSidebarComponent} from "./tabs/stacks/stacks-sidebar/stacks-sidebar.component";
import {StacksListComponent} from "./tabs/stacks/stacks-list/stacks-list.component";

import {NotifierModule} from 'angular-notifier';

import {Ng5SliderModule} from "../../../../assets/ng5-slider";


const routes = [
    {
        path: 'nodus-cloud-platform',
        component: TestDriveComponent,
        resolve: {
            profile: TestDriveService
        }
    },
    /*{
        path: 'nodus-cloud-platform/processes',
        loadChildren: './processes/processes.module#ProcessesModule'
    },*/
    {
        path: 'nodus-cloud-platform/clusters',
        loadChildren: './clusters/clusters.module#ClustersModule'
    },
    {
        path: 'nodus-cloud-platform/stacks',
        loadChildren: './stacks/stacks.module#StacksModule'
    }/*,
    {
        path: 'nodus-cloud-platform/nodes',
        loadChildren: './server-manager/server-manager.module#ServerManagerModule'
    }*/
];

@NgModule({
    declarations: [
        TestDriveComponent,
        DashboardComponent,
        ConfigurationComponent,
        WorkloadComponent,
        WorkloadListComponent,
        WorkloadSidebarComponent,
        NodesComponent,
        NodesListComponent,
        NodesSidebarComponent,
        WelcomeDialogComponent,
        TaskProgressComponent,
        CloudProviderFormDialogComponent,
        SubmitJobFormDialogComponent,
        JobDetailsFormDialogComponent,
        StackDetailsFormComponent,
        FuseConfirmDialogComponent,
        StacksComponent,
        StacksSidebarComponent,
        StacksListComponent,
        LogsDialogComponent
    ],
    imports: [
        RouterModule.forChild(routes),

        MatButtonModule,
        MatTooltipModule,
        MatProgressSpinnerModule,
        MatCheckboxModule,
        MatMenuModule,
        MatTableModule,
        MatPaginatorModule,
        MatSnackBarModule,
        MatDatepickerModule,
        MatDialogModule,
        MatRadioModule,
        MatCardModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatToolbarModule,
        MatSlideToggleModule,
        MatDividerModule,
        MatIconModule,
        MatTabsModule,
        MatProgressBarModule,
        MatListModule,
        MatSliderModule,
        MatStepperModule,

        ColorPickerModule,

        ChartsModule,
        NgxChartsModule,

        FuseSharedModule,
        FuseWidgetModule,

        // Notifier module
        NotifierModule.withConfig({
            position: {
                horizontal: {
                    position: "right",
                    distance: 25
                },
                vertical: {
                    position: "top",
                    distance: 70
                }
            },
            behaviour: {
                autoHide: 5000,
                stacking: 3
            }
        }),

        Ng5SliderModule
    ],
    providers: [
        TestDriveService
    ],
    entryComponents: [
        WelcomeDialogComponent,
        TaskProgressComponent,
        CloudProviderFormDialogComponent,
        SubmitJobFormDialogComponent,
        JobDetailsFormDialogComponent,
        StackDetailsFormComponent,
        FuseConfirmDialogComponent,
        LogsDialogComponent
    ]
})
export class TestDriveModule {
}

import {Component, OnDestroy, OnInit, ViewEncapsulation, ViewChild} from '@angular/core';
import {MatDialog} from '@angular/material';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {fuseAnimations} from '@fuse/animations';

import {CloudProviderFormDialogComponent} from './dialogs/cloud-provider-form/cloud-provider-form.component';
import {WelcomeDialogComponent} from "./dialogs/welcome-form/welcome-form.component";
import {SubmitJobFormDialogComponent} from "./dialogs/submit-job-form/submit-job-form.component";
import {TestDriveService} from "./test-drive.service";

import {NotifierService} from 'angular-notifier';
import {NotificationService} from "../../../main/apps/test-drive/notification.service";
import {ClustersService} from "./clusters.services";
import {HttpClient} from '@angular/common/http';

import {Subscription} from 'rxjs/Subscription'
import {TaskProgressComponent} from "./dialogs/task-progress/task-progress.component";
import {Router} from '@angular/router';

import {environment} from "../../../../environments/environment";

@Component({
    selector: 'test-drive',
    templateUrl: './test-drive.component.html',
    styleUrls: ['./test-drive.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class TestDriveComponent implements OnInit, OnDestroy {
    @ViewChild('customNotificationDefault1') customNotificationDefault1;
    @ViewChild('customNotificationWarning1') customNotificationWarning1;
    @ViewChild('customNotificationDefault2') customNotificationDefault2;
    @ViewChild('customNotificationWarning2') customNotificationWarning2;
    @ViewChild('customNotificationDefault3') customNotificationDefault3;
    @ViewChild('customNotificationWarning3') customNotificationWarning3;
    dialogRef: any;

    bursting_configuration: any;

    notification: any;
    notification_2: any;
    notification_3: any;

    sub: Subscription;

    selected_cluster: any;
    available_clusters: [];

    // Private
    private _unsubscribeAll: Subject<any>;
    private notification_counter: any;
    private readonly notifier: NotifierService;

    /**
     * Constructor
     */
    constructor(
        private _matDialog: MatDialog,
        private _testDriveService: TestDriveService,
        private _notifierService: NotifierService,
        private _notificationService: NotificationService,
        private _clusterService: ClustersService,
        private _httpClient: HttpClient,
        private router: Router
    ) {

        // Set the private defaults
        this._unsubscribeAll = new Subject();
        this.notification_counter = 1;
        this.notifier = _notifierService;
    }

    /**
     * On init
     */
    ngOnInit(): void {
        /*this.sub = this._notificationService.getNotifications()
            .subscribe(notification => {

                this._notificationService.logNotification(notification);

                if (this.notification_counter == 1) {
                    this.notification = notification;

                    if (localStorage.getItem('NOTIFICATION') == 'true') {
                        if (this.notification.type == "warning") {
                            this.notifier.show({
                                    message: "",
                                    type: "warning",
                                    template: this.customNotificationWarning1
                                }
                            );
                        }

                        else {
                            this.notifier.show({
                                    message: "",
                                    type: "default",
                                    template: this.customNotificationDefault1
                                }
                            );
                        }

                    }

                    this.notification_counter = 2;
                } else if(this.notification_counter == 2) {
                    this.notification_2 = notification;

                    if (localStorage.getItem('NOTIFICATION') == 'true') {
                        if (this.notification_2.type == "warning") {
                            this.notifier.show({
                                    message: "",
                                    type: "warning",
                                    template: this.customNotificationWarning2
                                }
                            );
                        }

                        else {
                            this.notifier.show({
                                    message: "",
                                    type: "default",
                                    template: this.customNotificationDefault2
                                }
                            );
                        }

                    }

                    this.notification_counter = 3;
                } else if(this.notification_counter == 3) {
                    this.notification_3 = notification;

                    if (localStorage.getItem('NOTIFICATION') == 'true') {
                        if (this.notification_3.type == "warning") {
                            this.notifier.show({
                                    message: "",
                                    type: "warning",
                                    template: this.customNotificationWarning3
                                }
                            );
                        }

                        else {
                            this.notifier.show({
                                    message: "",
                                    type: "default",
                                    template: this.customNotificationDefault3
                                }
                            );
                        }

                    }

                    this.notification_counter = 1;
                }

                this._notificationService.incrementNotificationCounter();
            });*/


        this._testDriveService.onBurstingConfigurationChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(bursting_configuration => {
                this.bursting_configuration = bursting_configuration;
            });

        this._clusterService.onClustersChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(clusters => {
               this.available_clusters = clusters;
            });

        this._clusterService.onSelectedClusterChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(selected_cluster => {
               this.selected_cluster = selected_cluster;
            });

        /*if (this.bursting_configuration.providers.length == 0) {
            this.dialogRef = this._matDialog.open(WelcomeDialogComponent, {
                panelClass: 'welcome-dialog',
                data: {
                    action: 'setup'
                },
                disableClose: true
            });
        }*/
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        //this.sub.unsubscribe();


        // Unsubscribe from all subscriptions
        //this._unsubscribeAll.next();
        //this._unsubscribeAll.complete();
    }

    openCloudProviderForm(): void {
        this.dialogRef = this._matDialog.open(CloudProviderFormDialogComponent, {
            panelClass: 'cloud-provider-form-dialog',
            data: {
                action: 'new'
            }
        });
    }

    openTaskProgress(): void {
        const data = {
            'user_token': localStorage.getItem('TOKEN'),
            'process_id': "TestProcess"
        };

        this._httpClient.post(`${environment.apiURL}/getProcessStatus`, data)
            .subscribe((response: any) => {
                this.dialogRef = this._matDialog.open(TaskProgressComponent, {
                    panelClass: 'task-progress',
                    data: {
                        process: response.data
                    }
                });
            });
    }

    openClusterManagement(): void {
        this.router.navigateByUrl('/apps/nodus-cloud-platform/clusters');
    }

    submitJob(): void {
        this.dialogRef = this._matDialog.open(SubmitJobFormDialogComponent, {
            panelClass: 'submit-job-form-dialog',
            data: {
                action: 'new'
            }
        });
    }

    hideNotification(notification): void {
        this.notifier.hide(notification.id);
        this._notificationService.decrementNotificationCounter();
    }

    selectCluster(cluster): void {
        this._clusterService.updateClusterSelection(cluster);
    }
}

import {Component, ViewEncapsulation} from '@angular/core';
import {fuseAnimations} from '@fuse/animations';

/**
 * @title Data table with sorting, pagination, and filtering.
 */
@Component({
    selector: 'stacks',
    templateUrl: './stacks.component.html',
    styleUrls: ['./stacks.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})

export class StacksComponent {
    constructor() {

    }


}

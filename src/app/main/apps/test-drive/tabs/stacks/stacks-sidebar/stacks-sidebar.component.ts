import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {StacksService} from "../../../stacks.service";

@Component({
    selector: 'stacks-sidebar',
    templateUrl: './stacks-sidebar.component.html',
    styleUrls: ['./stacks-sidebar.component.scss']
})
export class StacksSidebarComponent implements OnInit, OnDestroy {
    filterBy: string;

    all_stacks_count: any;
    available_stacks_count: any;
    deployed_stacks_count: any;

    private _unsubscribeAll: Subject<any>;

    constructor(
        private _stacksService: StacksService
    ) {
        this.filterBy = 'all';
        this._unsubscribeAll = new Subject();
    }

    ngOnInit(): void {
        this._stacksService.onStackListUpdated
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((stacks) => {
                if (stacks.stackList) {
                    this.all_stacks_count = stacks.stackList.length;
                    this.available_stacks_count = stacks.stackListAvailable.length;
                    this.deployed_stacks_count = stacks.stackListDeployed.length;
                }
            });
    }

    ngOnDestroy(): void {
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    changeFilter(filter): void {
        this.filterBy = filter;
        this._stacksService.onFilterChanged.next(this.filterBy);
    }


}
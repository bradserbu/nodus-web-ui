import {Component, OnDestroy, OnInit, ViewEncapsulation, ViewChild} from '@angular/core';
import {Subject} from 'rxjs';
import {fuseAnimations} from "../../../../../../../@fuse/animations";
import {takeUntil} from 'rxjs/operators';
import {MatSort, MatTableDataSource, MatDialog, MatDialogRef, MatSnackBar} from '@angular/material';
import {HttpClient} from '@angular/common/http';
import {FuseUtils} from '@fuse/utils';
import {Router} from '@angular/router';
import {FuseConfirmDialogComponent} from "../../../../../../../@fuse/components/confirm-dialog/confirm-dialog.component";
import {animate, state, style, transition, trigger} from '@angular/animations';
import {StacksService} from "../../../stacks.service";

@Component({
    selector: 'stacks-list',
    templateUrl: './stacks-list.component.html',
    styleUrls: ['./stacks-list.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: [
        trigger('detailExpand', [
            state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
            state('expanded', style({height: '*'})),
            transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)'))
        ]),
        fuseAnimations
    ]
})

export class StacksListComponent implements OnInit, OnDestroy {
    displayedColumns = ['icon', 'name', 'version', 'state', 'buttons'];
    dataSource: MatTableDataSource<StackData>;

    all_stacks: StackData[] = [];
    available_stacks: StackData[] = [];
    deployed_stacks: StackData[] = [];
    selectedStacks: StackData[] = [];
    stackDetails: StackData;
    searchText: string;
    filterBy: string;

    dialogRef: any;
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;

    @ViewChild(MatSort) sort: MatSort;

    private _unsubscribeAll: Subject<any>;

    constructor(
        private _matDialog: MatDialog,
        private _stacksService: StacksService,
        private _httpClient: HttpClient,
        private router: Router,
        public snackBar: MatSnackBar
    ) {
        this.stackDetails = {
            'id': '',
            'name': '',
            'state': ''
        };
        this.filterBy = "all";
        this._unsubscribeAll = new Subject();
        this.dataSource = new MatTableDataSource(this.selectedStacks);
    }

    ngOnInit(): void {
        //this.sort.sort(<MatSortable>({id: 'name', start: 'asc'}

        this._stacksService.onStackListUpdated
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((stacks) => {
                this.all_stacks = stacks.stackList;
                this.available_stacks = stacks.stackListAvailable;
                this.deployed_stacks = stacks.stackListDeployed;
                this.updateStacksSelection();
            });

        this._stacksService.onSearchTextChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((searchText) => {
                this.searchText = searchText;
                this.updateStacksSelection();
            });

        this._stacksService.onFilterChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((filter) => {
                this.filterBy = filter;
                this.updateStacksSelection();
            });
    }

    ngOnDestroy(): void {
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    updateStacksSelection() {
        switch (this.filterBy) {
            case 'all':
                this.selectedStacks = this.all_stacks;
                break;
            case 'Available':
                this.selectedStacks = this.available_stacks;
                break;
            case 'Deployed':
                this.selectedStacks = this.deployed_stacks;
                break;
            default:
                this.selectedStacks = this.all_stacks;
                break;
        }

        if (this.searchText && this.searchText !== '') {
            this.selectedStacks = FuseUtils.filterArrayByString(this.selectedStacks, this.searchText);
        }

        this.dataSource.data = this.selectedStacks;
        this.dataSource.sort = this.sort;
    }

    updateStackList() {
        this._stacksService.updateStackList();
    }

    showStackDetails(stack) {
        if (this.stackDetails.id == stack.id)
            this.stackDetails = {
                'id': '',
                'name': '',
                'state': ''
            };
        else {
            this.stackDetails = stack;
        }
    }

    showNotification(message: string, duration: number, type: string): void {
        this.snackBar.open(message, 'Dismiss', {
            duration: duration,
            panelClass: [type],
            horizontalPosition: 'end',
            verticalPosition: 'bottom'
        });
    }
}

export interface StackData {
    id: string;
    name: string;
    state: string;
}
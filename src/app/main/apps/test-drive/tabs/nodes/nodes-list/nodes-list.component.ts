import {Component, OnDestroy, OnInit, ViewEncapsulation, ViewChild} from '@angular/core';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {MatSort, MatTableDataSource} from '@angular/material';
import {TestDriveService} from "../../../test-drive.service";
import {ClustersService} from "../../../clusters.services";

@Component({
    selector: 'nodes-list',
    templateUrl: './nodes-list.component.html',
    styleUrls: ['./nodes-list.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class NodesListComponent implements OnInit, OnDestroy {
    displayedColumns = ['node_id', 'state', 'available_cores', 'available_threads', 'idle_purge_time'];
    dataSource: MatTableDataSource<NodeData>;

    all_nodes: NodeData[] = [];
    available_nodes: NodeData[] = [];
    busy_nodes: NodeData[] = [];
    selectedNodes: NodeData[] = [];
    filterBy: string;

    selectedCluster: any;


    @ViewChild(MatSort) sort: MatSort;

    private _unsubscribeAll: Subject<any>;

    constructor(
        private _testdriveService: TestDriveService,
        private _clusterService: ClustersService
    ) {
        this.filterBy = "all";
        this._unsubscribeAll = new Subject();
        this.selectedCluster = {};
        this.dataSource = new MatTableDataSource(this.selectedNodes);
    }

    ngOnInit(): void {
        this._testdriveService.onNodesListUpdated
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((nodes) => {
                this.all_nodes = nodes.nodeList;
                this.available_nodes = nodes.nodeListAvailable;
                this.busy_nodes = nodes.nodeListBusy;
                this.updateNodesSelection();
            });

        this._testdriveService.onNodesFilterChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((filter) => {
                this.filterBy = filter;
                this.updateNodesSelection();
            });

        this._clusterService.getSelectedCluster()
            .then(cluster => {
                this.selectedCluster = cluster;
            });
    }

    ngOnDestroy(): void {
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    updateNodesSelection() {
        switch (this.filterBy) {
            case 'all':
                this.selectedNodes = this.all_nodes;
                break;
            case 'Available':
                this.selectedNodes = this.available_nodes;
                break;
            case 'Busy':
                this.selectedNodes = this.busy_nodes;
                break;
            default:
                this.selectedNodes = this.all_nodes;
                break;
        }

        this.dataSource.data = this.selectedNodes;
        this.dataSource.sort = this.sort;
    }
}

export interface NodeData {
    id: string;
    name: string;
    state: string;
}
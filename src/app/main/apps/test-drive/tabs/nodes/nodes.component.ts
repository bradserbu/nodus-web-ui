import {Component, ViewEncapsulation} from '@angular/core';
import {fuseAnimations} from '@fuse/animations';

/**
 * @title Data table with sorting, pagination, and filtering.
 */
@Component({
    selector: 'nodes',
    templateUrl: './nodes.component.html',
    styleUrls: ['./nodes.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})

export class NodesComponent {
    constructor() {

    }


}

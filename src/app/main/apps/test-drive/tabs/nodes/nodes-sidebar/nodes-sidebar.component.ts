import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {TestDriveService} from "../../../test-drive.service";

@Component({
    selector: 'nodes-sidebar',
    templateUrl: './nodes-sidebar.component.html',
    styleUrls: ['./nodes-sidebar.component.scss']
})
export class NodesSidebarComponent implements OnInit, OnDestroy {
    filterBy: string;

    all_nodes_count: any;
    available_nodes_count: any;
    busy_nodes_count: any;

    private _unsubscribeAll: Subject<any>;

    constructor(
        private _testdriveService: TestDriveService
    ) {
        this.filterBy = 'all';
        this._unsubscribeAll = new Subject();
    }

    ngOnInit(): void {
        this._testdriveService.onNodesListUpdated
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((nodes) => {
                if (nodes.nodeList) {
                    this.all_nodes_count = nodes.nodeList.length;
                    this.available_nodes_count = nodes.nodeListAvailable.length;
                    this.busy_nodes_count = nodes.nodeListBusy.length;
                }
            });
    }

    ngOnDestroy(): void {
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    changeFilter(filter): void {
        this.filterBy = filter;
        this._testdriveService.onNodesFilterChanged.next(this.filterBy);
    }


}
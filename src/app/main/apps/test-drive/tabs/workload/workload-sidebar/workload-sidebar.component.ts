import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {TestDriveService} from "../../../test-drive.service";

@Component({
    selector: 'workload-sidebar',
    templateUrl: './workload-sidebar.component.html',
    styleUrls: ['./workload-sidebar.component.scss']
})
export class WorkloadSidebarComponent implements OnInit, OnDestroy {
    filterBy: string;

    all_jobs_count: any;
    running_jobs_count: any;
    queued_jobs_count: any;

    private _unsubscribeAll: Subject<any>;

    constructor(
        private _testdriveService: TestDriveService
    ) {
        this.filterBy = 'all';
        this._unsubscribeAll = new Subject();
    }

    ngOnInit(): void {
        this._testdriveService.onWorkloadListUpdated
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((workload) => {
                if (workload.jobList) {
                    this.all_jobs_count = workload.jobList.length;
                    this.running_jobs_count = workload.jobListRunning.length;
                    this.queued_jobs_count = workload.jobListQueued.length;
                }
            });
    }

    ngOnDestroy(): void {
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    changeFilter(filter): void {
        this.filterBy = filter;
        this._testdriveService.onWorkloadFilterChanged.next(this.filterBy);
    }


}
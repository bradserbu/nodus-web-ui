import {Component, ViewEncapsulation} from '@angular/core';
import {fuseAnimations} from '@fuse/animations';

/**
 * @title Data table with sorting, pagination, and filtering.
 */
@Component({
    selector: 'workload',
    templateUrl: './workload.component.html',
    styleUrls: ['./workload.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})

export class WorkloadComponent {
    constructor() {

    }


}

import {Component, OnDestroy, OnInit, ViewEncapsulation, ViewChild} from '@angular/core';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {MatSort, MatTableDataSource} from '@angular/material';
import {TestDriveService} from "../../../test-drive.service";

@Component({
    selector: 'workload-list',
    templateUrl: './workload-list.component.html',
    styleUrls: ['./workload-list.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class WorkloadListComponent implements OnInit, OnDestroy {
    displayedColumns = ['job_id', 'state', 'submit_time', 'start_time', 'compute_node', 'buttons'];
    dataSource: MatTableDataSource<JobData>;

    all_jobs: JobData[] = [];
    running_jobs: JobData[] = [];
    queued_jobs: JobData[] = [];
    selectedWorkload: JobData[] = [];
    filterBy: string;


    @ViewChild(MatSort) sort: MatSort;

    private _unsubscribeAll: Subject<any>;

    constructor(
        private _testdriveService: TestDriveService
    ) {
        this.filterBy = "all";
        this._unsubscribeAll = new Subject();
        this.dataSource = new MatTableDataSource(this.selectedWorkload);
    }

    ngOnInit(): void {
        this._testdriveService.onWorkloadListUpdated
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((workload) => {
                this.all_jobs = workload.jobList;
                this.running_jobs = workload.jobListRunning;
                this.queued_jobs = workload.jobListQueued;
                this.updateWorkloadSelection();
            });

        this._testdriveService.onWorkloadFilterChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((filter) => {
                this.filterBy = filter;
                this.updateWorkloadSelection();
            });
    }

    ngOnDestroy(): void {
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    updateWorkloadSelection() {
        switch (this.filterBy) {
            case 'all':
                this.selectedWorkload = this.all_jobs;
                break;
            case 'Running':
                this.selectedWorkload = this.running_jobs;
                break;
            case 'Queued':
                this.selectedWorkload = this.queued_jobs;
                break;
            default:
                this.selectedWorkload = this.all_jobs;
                break;
        }

        this.dataSource.data = this.selectedWorkload;
        this.dataSource.sort = this.sort;
    }
}

export interface JobData {
    id: string;
    name: string;
    state: string;
}
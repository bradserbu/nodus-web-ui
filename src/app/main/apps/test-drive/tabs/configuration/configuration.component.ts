import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {MatDialog, MatDialogRef, MatSnackBar} from '@angular/material';

import {fuseAnimations} from '@fuse/animations';
import {TestDriveService} from "../../test-drive.service";
import {FuseConfirmDialogComponent} from "../../../../../../@fuse/components/confirm-dialog/confirm-dialog.component";
import {HttpClient} from '@angular/common/http';

import {StackDetailsFormComponent} from "../../dialogs/stack-details-form/stack-details-form.component";
import {ClustersService} from "../../clusters.services";
import {Router} from '@angular/router';

import {environment} from "../../../../../../environments/environment";

@Component({
    selector: 'test-drive-configuration',
    templateUrl: './configuration.component.html',
    styleUrls: ['./configuration.component.scss'],
    animations: fuseAnimations
})
export class ConfigurationComponent implements OnInit, OnDestroy {
    dialogRef: any;
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;

    template_stacks: any;
    selected_stack: string;
    bursting_configuration: any;
    nodedeployvalue: number;
    nodedestroyvalue: number;
    deployed_nodes: any;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {TestDriveService} _profileService
     */
    constructor(
        public _matDialog: MatDialog,
        private _testDriveService: TestDriveService,
        private _clustersService: ClustersService,
        public snackBar: MatSnackBar,
        private _httpClient: HttpClient,
        private router: Router
    ) {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
        this.nodedeployvalue = 1;
        this.nodedestroyvalue = 0;
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this._testDriveService.onDashboardQuickViewChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(dashboard_quick_view => {
                this.deployed_nodes = dashboard_quick_view.nodes_total;
                this.nodedeployvalue = this.deployed_nodes;
            });

        this._testDriveService.onTemplateStacksChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(template_stacks => {
                this.template_stacks = template_stacks;
            });

        this._testDriveService.onBurstingConfigurationChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(bursting_configuration => {
                this.bursting_configuration = bursting_configuration;
            });

        this.selected_stack = "base_stack";
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }


    selectCloudProvider(provider): void {
        this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to select ' + provider.name + ' as your new configured burst provider? This will override any previous made selection.';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                const provider_data = {
                    'user_token': localStorage.getItem('TOKEN'),
                    'provider': provider.name
                };

                this._httpClient.post(`${environment.apiURL}/updateSelectedProvider`, provider_data)
                    .subscribe((response: any) => {
                        const title = provider.name;
                        const message = " set as new burst provider.";
                        this.showNotification(title + message, 5000, 'notification');
                        this.logActivity('cloud_provider', title, 'assets/images/providers/' + provider.name + '.png', message);
                        this._testDriveService.getBurstingConfiguration();
                    }, error => {
                        this.showNotification('ERROR: Updating Bursting Provider to ' + provider.name + ' was Unsuccessful.', 60000, 'warning');
                    });
            }
            this.confirmDialogRef = null;
        });
    }


    /**
     * removeCloudProvider
     * @param provider
     */
    removeCloudProvider(provider): void {
        this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to remove ' + provider.name + ' from your list of configured cloud providers?';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                /*const job = {
                    'job_id': 'Moab.' + workload.job_id
                };

                this._httpClient.post(`${environment.apiURL}/canceljob`, job)
                    .subscribe((response: any) => {
                        this.showNotification('Canceling Job:   ' + workload.job_id, 5000, 'notification');
                        this.getWorkload(this.user);
                    }, error => {
                        this.showNotification('ERROR: Cancellation Request for Job ' + workload.job_id + ' was Unsuccessful.', 60000, 'warning');
                    });*/
            }
            this.confirmDialogRef = null;
        });

    }

    showNotification(message: string, duration: number, type: string): void {
        this.snackBar.open(message, 'Dismiss', {
            duration: duration,
            panelClass: [type],
            horizontalPosition: 'end',
            verticalPosition: 'bottom'
        });
    }

    logActivity(activity_type, activity_title, activity_icon, message) {
        const logActivity = {
            'user_token': localStorage.getItem('TOKEN'),
            'activity': {
                'type': activity_type,
                'title': {
                    'name': activity_title,
                    'icon': activity_icon
                },
                'message': message
            }
        };

        this._httpClient.post(`${environment.apiURL}/logActivity`, logActivity)
            .subscribe((response: any) => {
            });
    }

    openStackDialog(stack): void {
        this.dialogRef = this._matDialog.open(StackDetailsFormComponent, {
            panelClass: 'stack-details-form-dialog',
            data: {
                stack: stack
            }
        })
    }

    deployNodes() {
        const data = {
            'user_token': localStorage.getItem('TOKEN'),
            'node_count': this.nodedeployvalue
        };

        this._testDriveService.deployClusterNodes(data);
        this.showNotification(this.nodedeployvalue + " new nodes are being deployed and added to your cluster.", 5000, 'notification');

        // reset controls
        this.nodedeployvalue = 0;
    }

    destroyNodes() {
        const data = {
            'user_token': localStorage.getItem('TOKEN'),
            'node_count': this.nodedestroyvalue
        };

        this._testDriveService.destroyClusterNodes(data);
        this.showNotification(this.nodedestroyvalue + " nodes are being destroyed and removed from your cluster.", 5000, 'notification');

        // reset controls
        this.nodedestroyvalue = 0;
    }

    destroyCluster() {
        this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to completely destroy the cluster? Please make sure to save any job output data from the cluster before destroying it.';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                const data = {
                    'user_token': localStorage.getItem('TOKEN'),
                    'cluster_type': this.bursting_configuration.selected_provider.name,
                    'cluster_id': this.bursting_configuration.selected_provider.id
                };

                this._testDriveService.destroyCluster(data);
                this.showNotification("The cluster is being destroyed.", 5000, 'notification');
            }
            this.confirmDialogRef = null;
        });
    }

    resizeCluster() {
        this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to resize the cluster? The cluster will be unavailable during the time it takes to resize and you will be redirected to the cluster management screen.';
        const cluster_size = this.nodedeployvalue;

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                this._clustersService.resizeCluster(cluster_size);
                this.showNotification("The cluster is being resized.", 5000, 'notification');
                this.router.navigateByUrl('/apps/nodus-cloud-platform/clusters');
            }
            this.confirmDialogRef = null;
        });
    }
}

import {Component, OnDestroy, OnInit} from '@angular/core';

import {fuseAnimations} from '@fuse/animations';

import {TestDriveService} from "../../test-drive.service";
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {MatDialog, MatSnackBar} from '@angular/material';
import {HttpClient} from '@angular/common/http';

import {JobDetailsFormDialogComponent} from "../../dialogs/job-details-form/job-details-form.component";
import {FuseProgressBarService} from "../../../../../../@fuse/components/progress-bar/progress-bar.service";

import {environment} from "../../../../../../environments/environment";

@Component({
    selector: 'test-drive-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss'],
    animations: fuseAnimations
})
export class DashboardComponent implements OnInit, OnDestroy {
    dashboard_quick_view: any;
    workload_nodes_progression: any;
    activities: any;

    progression_graph: any;

    dialogRef: any;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {TestDriveService} _testDriveService
     */
    constructor(
        private _testDriveService: TestDriveService,
        private _matDialog: MatDialog,
        public snackBar: MatSnackBar,
        private _httpClient: HttpClient,
        private _fuseProgressBarService: FuseProgressBarService
    ) {
        // Set the private defaults
        this._unsubscribeAll = new Subject();

        // Define Workload/Nodes Progression Graph
        this.progression_graph = {
            chartType: 'bar',
            labels: ['1m ago', '55s ago', '50s ago', '45s ago', '40s ago', '35s ago', '30s ago', '25s ago', '20s ago', '15s ago', '10s ago', '5s ago', 'Now'],
            colors: [
	    	{
                    borderColor: '#3949ab',
                    borderWidth: '1',
                    backgroundColor: '#3949ab',
                    pointBackgroundColor: '#3949ab',
                    pointHoverBackgroundColor: '#3949ab',
                    pointBorderColor: '#ffffff',
                    pointHoverBorderColor: '#ffffff'
                },
                {
                    borderColor: '#5d619b',
                    borderWidth: '1',
                    backgroundColor: '#5d619b',
                    pointBackgroundColor: '#5d619b',
                    pointHoverBackgroundColor: '#5d619b',
                    pointBorderColor: '#ffffff',
                    pointHoverBorderColor: '#ffffff'
                },
                {
                    borderColor: '#737a8a',
                    borderWidth: '1',
                    backgroundColor: '#737a8a',
                    pointBackgroundColor: '#737a8a',
                    pointHoverBackgroundColor: '#737a8a',
                    pointBorderColor: '#ffffff',
                    pointHoverBorderColor: '#ffffff'
                },
                {
                    borderColor: '#97e400',
                    borderWidth: '1',
                    backgroundColor: '#97e400',
                    pointBackgroundColor: '#97e400',
                    pointHoverBackgroundColor: '#97e400',
                    pointBorderColor: '#ffffff',
                    pointHoverBorderColor: '#ffffff'
                },
                {
                    borderColor: '#93c944',
                    borderWidth: '1',
                    backgroundColor: '#93c944',
                    pointBackgroundColor: '#93c944',
                    pointHoverBackgroundColor: '#93c944',
                    pointBorderColor: '#ffffff',
                    pointHoverBorderColor: '#ffffff'
                },
                {
                    borderColor: '#8cae61',
                    borderWidth: '1',
                    backgroundColor: '#8cae61',
                    pointBackgroundColor: '#8cae61',
                    pointHoverBackgroundColor: '#8cae61',
                    pointBorderColor: '#ffffff',
                    pointHoverBorderColor: '#ffffff'
                }
	    ],
            options: {
                spanGaps: false,
                legend: {
                    display: true,
                    onHover: function (e) {
                        e.target.style.cursor = 'pointer';
                    },
                    labels: {
                        fontColor: 'rgba(0,0,0,1)'
                    }
                },
                hover: {
                    onHover: function (e) {
                        var point = this.getElementAtEvent(e);
                        if (point.length) e.target.style.cursor = 'pointer';
                        else e.target.style.cursor = 'default';
                    }
                },
                maintainAspectRatio: false,
                tooltips: {
                    position: 'nearest',
                    mode: 'index',
                    intersect: false
                },
                layout: {
                    padding: {
                        left: 24,
                        right: 32
                    }
                },
                elements: {
                    point: {
                        radius: 4,
                        borderWidth: 2,
                        hoverRadius: 4,
                        hoverBorderWidth: 2
                    }
                },
                scales: {
                    xAxes: [
                        {
                            gridLines: {
                                display: false
                            },
                            ticks: {
                                fontColor: 'rgba(0,0,0,1)'
                            }
                        }
                    ],
                    yAxes: [
                        {
                            gridLines: {
                                tickMarkLength: 16
                            },
                            ticks: {
                                fontColor: 'rgba(0,0,0,1)',
                                beginAtZero: true,
                                stepSize: 5
                            }
                        }
                    ]
                },
                plugins: {
                    filler: {
                        propagate: false
                    }
                }
            }
        };
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this._testDriveService.onDashboardQuickViewChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(dashboard_quick_view => {
                this.dashboard_quick_view = dashboard_quick_view;
            });

        this._testDriveService.onWorkloadNodesProgressionChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(workload_nodes_progression => {
                this.workload_nodes_progression = workload_nodes_progression;
            });

        this._testDriveService.onActivitiesChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(activities => {
                this.activities = activities;
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }


    /**
     * openJobDetails
     * @param job
     */
    openJobDetails(job_id): void {
        this._fuseProgressBarService.show();

        const job = {
            user_token: localStorage.getItem('TOKEN'),
            job_id: job_id.split("(ID: ")[1].split(")")[0]
        };

        this._httpClient.post(`${environment.apiURL}/getJobDetails`, job)
            .subscribe((response: any) => {
                this._fuseProgressBarService.hide();

                const test = {
                    state: "COMPLETED",
                    std_output: response.data.data.job_output,
                    std_error: response.data.data.job_error
                };

                this.dialogRef = this._matDialog.open(JobDetailsFormDialogComponent, {
                    panelClass: 'job-details-form-dialog',
                    data: {
                        job_id: job_id,
                        job: test
                    }
                });
            }, error => {
                this._fuseProgressBarService.hide();
                this.showNotification('ERROR: Job Details Request was Unsuccessful.', 60000, 'warning');
            });
    }

    showNotification(message: string, duration: number, type: string): void {
        this.snackBar.open(message, 'Dismiss', {
            duration: duration,
            panelClass: [type],
            horizontalPosition: 'end',
            verticalPosition: 'bottom'
        });
    }
}

import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ProcessesComponent} from "./processes.component";
import {ProcessListComponent} from "./process-list/process-list.component";
import {ProcessSidebarComponent} from "./process-sidebar/process-sidebar.component";
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { CommonModule } from '@angular/common';

import {
    MatIconModule,
    MatInputModule,
    MatFormFieldModule,
    MatTableModule,
    MatCardModule,
    MatDialogModule
} from '@angular/material';

const routes: Routes = [
    {
        path: '**',
        component: ProcessesComponent
    }
];

@NgModule({
    declarations: [
        ProcessesComponent,
        ProcessListComponent,
        ProcessSidebarComponent
    ],
    imports: [
        RouterModule.forChild(routes),

        FormsModule,
        ReactiveFormsModule,
        CommonModule,

        MatIconModule,
        MatInputModule,
        MatFormFieldModule,
        MatTableModule,
        MatCardModule,
        MatDialogModule
    ]
})
export class ProcessesModule
{

}
import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {Subject} from 'rxjs';
import {fuseAnimations} from "../../../../../../@fuse/animations";
import {ProcessesService} from "../../processes.service";
import {takeUntil} from 'rxjs/operators';
import {MatTableDataSource, MatDialog} from '@angular/material';
import {TaskProgressComponent} from "../../dialogs/task-progress/task-progress.component";
import {HttpClient} from '@angular/common/http';
import {FuseUtils} from '@fuse/utils';

import {environment} from "../../../../../../environments/environment";

@Component({
    selector: 'process-list',
    templateUrl: './process-list.component.html',
    styleUrls: ['./process-list.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})

export class ProcessListComponent implements OnInit, OnDestroy {
    displayedColumns = ['process_id', 'name', 'state'];
    dataSource: MatTableDataSource<ProcessData>;

    allProcesses: ProcessData[] = [];
    selectedProcesses: ProcessData[] = [];
    searchText: string;
    filterBy: string;

    dialogRef: any;

    private _unsubscribeAll: Subject<any>;

    constructor(
        private _matDialog: MatDialog,
        private _processesService: ProcessesService,
        private _httpClient: HttpClient
    ) {
        this.filterBy = "active";
        this._unsubscribeAll = new Subject();
        this.dataSource = new MatTableDataSource(this.selectedProcesses);
    }

    ngOnInit(): void {
        this._processesService.onProcessesChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(processes => {
                this.allProcesses = processes;
                this.updateProcessSelection();
            });

        this._processesService.onSearchTextChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((searchText) => {
               this.searchText = searchText;
               this.updateProcessSelection();
            });

        this._processesService.onFilterChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((filter) => {
                this.filterBy = filter;
                this.updateProcessSelection();
            });
    }

    ngOnDestroy(): void {
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    openProcessDetails(process): void {
        const data = {
            'user_token': localStorage.getItem('TOKEN'),
            'process_id': process.id
        };

        this._httpClient.post(`${environment.apiURL}/getProcessStatus`, data)
            .subscribe((response: any) => {
                this.dialogRef = this._matDialog.open(TaskProgressComponent, {
                    panelClass: 'task-progress',
                    data: {
                        process: process,
                        title: process.name
                    }
                });
            });
    }

    updateProcessSelection() {
        if (this.filterBy === 'all')
            this.selectedProcesses = this.allProcesses;
        else {

            this.selectedProcesses = [];
            this.allProcesses.forEach(function (process) {
                if (process.state === this.filterBy)
                    this.selectedProcesses.push(process);
            }.bind(this));
        }

        if(this.searchText && this.searchText !== '') {
            this.selectedProcesses = FuseUtils.filterArrayByString(this.selectedProcesses, this.searchText);
        }

        this.dataSource.data = this.selectedProcesses;
    }
}

export interface ProcessData {
    process_id: string;
    name: string;
    state: string;
}
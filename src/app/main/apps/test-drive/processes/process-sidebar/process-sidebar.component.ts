import {Component} from '@angular/core';
import {ProcessesService} from "../../processes.service";

@Component({
    selector: 'process-sidebar',
    templateUrl: './process-sidebar.component.html',
    styleUrls: ['./process-sidebar.component.scss']
})
export class ProcessSidebarComponent
{
    filterBy: string;

    constructor(
        private _processService: ProcessesService
    ) {
        this.filterBy = 'active';
    }

    changeFilter(filter): void {
        this.filterBy = filter;
        this._processService.onFilterChanged.next(this.filterBy);
    }



}
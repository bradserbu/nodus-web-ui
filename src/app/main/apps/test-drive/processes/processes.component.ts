import {Component, ViewEncapsulation, OnInit, OnDestroy} from '@angular/core';
import {fuseAnimations} from '@fuse/animations';
import {FormControl} from '@angular/forms';
import {Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged, takeUntil} from 'rxjs/operators';
import {ProcessesService} from "../processes.service";
import {Router} from '@angular/router';


/**
 * @title Data table with sorting, pagination, and filtering.
 */
@Component({
    selector: 'processes',
    templateUrl: './processes.component.html',
    styleUrls: ['./processes.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})

export class ProcessesComponent implements OnInit, OnDestroy{
    searchInput: FormControl;

    private _unsubscribeAll: Subject<any>;

    constructor(
        private _processService: ProcessesService,
        private router: Router
    ) {
        this.searchInput = new FormControl('');
        this._unsubscribeAll = new Subject();
    }

    ngOnInit(): void {
        this.searchInput.valueChanges
            .pipe(takeUntil(this._unsubscribeAll),
                debounceTime(300),
                distinctUntilChanged()
            )
            .subscribe(searchText => {
                this._processService.onSearchTextChanged.next(searchText);
            });
    }

    openDashboard(): void {
        this.router.navigateByUrl('/apps/nodus-cloud-platform');
    }

    ngOnDestroy(): void {
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

}

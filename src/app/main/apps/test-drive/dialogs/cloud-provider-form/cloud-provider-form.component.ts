import {Component, Inject, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef, MatSnackBar} from '@angular/material';

import {ClustersService} from "../../clusters.services";
import {HttpClient} from '@angular/common/http';
import {FuseProgressBarService} from "../../../../../../@fuse/components/progress-bar/progress-bar.service";

import {LabelType, Options} from "../../../../../../assets/ng5-slider";

@Component({
    selector: 'cloud-provider-form-dialog',
    templateUrl: './cloud-provider-form.component.html',
    styleUrls: ['./cloud-provider-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class CloudProviderFormDialogComponent implements OnInit, OnDestroy {
    action: string;
    job: any;
    nodesNumber: any;
    providerForm: FormGroup;
    dialogTitle: string;
    slider: any;

    providers: any;
    provider_config: any;
    selected_provider_index: any;
    form_state: number;
    selected_file: any;

    cluster_name_exists: boolean;

    stack_options: any;


    minValue: number = 1;
    maxValue: number = 200;
    options: Options = {
        floor: 1,
        ceil: 250,
        disabled: false,
        translate: (value: number, label: LabelType): string => {
            switch (label) {
                case LabelType.Low:
                    return 'Nodes: ' + value;
                case LabelType.High:
                    return 'Max: ' + value;
                default:
                    return '' + value;
            }
        }
    };

    selectedValue: any;
    //azure_logged_in: boolean;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {MatDialogRef<CloudProviderFormDialogComponent>} matDialogRef
     * @param _data
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        public matDialogRef: MatDialogRef<CloudProviderFormDialogComponent>,
        public snackBar: MatSnackBar,
        @Inject(MAT_DIALOG_DATA) private _data: any,
        private _formBuilder: FormBuilder,
        private _clustersService: ClustersService,
        private _httpClient: HttpClient,
        private _fuseProgressBarService: FuseProgressBarService
    ) {
        this._unsubscribeAll = new Subject();
        this.action = _data.action;

        if (this.action == "on-demand" || this.action == "job-cluster") {
            this.job = _data.job;
            this.nodesNumber = this.job.job_nodes_count;
        } else {
            this.nodesNumber = 1;
        }

        const token = {
            'user_token': localStorage.getItem('TOKEN')
        };

        this.dialogTitle = 'Create Cluster - Provider Selection';

        this.slider = {
            minValue: 10,
            maxValue: 90,
            options: {
                floor: 0,
                ceil: 100,
                step: 1
            }
        };

        this.options.disabled = (this.action == 'on-demand');
        this.selected_provider_index = 0;
    }

    /**
     * On init
     */
    ngOnInit(): void {
        this._clustersService.onProvidersChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(providers => {
                this.providers = providers;
                this.provider_config = {};
                this.form_state = 0;
            });

        this._clustersService.onStackOptionsChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(stack_options => {
                this.stack_options = stack_options;
            });

        this.providerForm = this.createCloudProviderForm();
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Create the cloud provider form
     *
     * @returns {FormGroup}
     */
    createCloudProviderForm(): FormGroup {
        let control = {
            cloudProvider: new FormControl('', Validators.required),
            selectedCloudProvider: new FormControl(' '),
            clusterName: new FormControl(' '),
            clusterPurgeTime: new FormControl(' '),
            nodesNumber: new FormControl(' '),
            headNodeSize: new FormControl(' '),
            computeNodeSize: new FormControl(' '),
            clusterMoveData: new FormControl(' ')
        };

        control['bursting'] = new FormControl(' ');

        let i: number;
        for (i = 0; i < this.providers.length; i++) {
            let j: number;
            for (j = 0; j < this.providers[i].authentication.length; j++) {
                control[this.providers[i].authentication[j].id] = new FormControl(' ');
            }
        }

        for (i = 0; i < this.providers.length; i++) {
            let j: number;
            for (j = 0; j < this.providers[i].burst_config.length; j++) {
                control[this.providers[i].burst_config[j].id] = new FormControl(' ');
            }
        }

        return new FormGroup(control);
    }

    /**
     * Create the cloud provider form
     *
     * @returns {FormGroup}
     */
    createClusterConfigurationForm(): FormGroup {
        let control = {};
        if (this.action == "job-cluster") {
            control = {
                selectedCloudProvider: new FormControl({value: this.provider_config.provider, disabled: true}),
                clusterName: new FormControl('', Validators.required),
                clusterPurgeTime: new FormControl({value: 30, disabled: false}, Validators.required),
                nodesNumber: new FormControl({
                    value: this.job.job_nodes_count,
                    disabled: false
                }, [Validators.required, Validators.min(this.job.job_nodes_count)]),
                headNodeSize: new FormControl('', Validators.required),
                computeNodeSize: new FormControl('', Validators.required),
                clusterMoveData: new FormControl({value: false, disabled: false}, Validators.required)
            };
        } else if (this.action == "on-demand") {
            control = {
                selectedCloudProvider: new FormControl({value: this.provider_config.provider, disabled: true}),
                clusterName: new FormControl('', Validators.required),
                clusterPurgeTime: new FormControl({value: 0, disabled: false}, Validators.required),
                nodesNumber: new FormControl({value: this.job.job_nodes_count, disabled: false}, Validators.required),
                headNodeSize: new FormControl('', Validators.required),
                computeNodeSize: new FormControl('', Validators.required),
                clusterMoveData: new FormControl({value: true, disabled: false}, Validators.required)
            }
        } else {
            control = {
                selectedCloudProvider: new FormControl({value: this.provider_config.provider, disabled: true}),
                clusterName: new FormControl('', Validators.required),
                clusterPurgeTime: new FormControl({value: 30, disabled: false}, Validators.required),
                nodesNumber: new FormControl({value: 1, disabled: false}, Validators.required),
                headNodeSize: new FormControl('', Validators.required),
                computeNodeSize: new FormControl('', Validators.required),
                clusterMoveData: new FormControl({value: false, disabled: false}, Validators.required)
            };
        }

        control['bursting'] = new FormControl(' ');

        let i: number;
        for (i = 0; i < this.providers.length; i++) {
            let j: number;
            for (j = 0; j < this.providers[i].authentication.length; j++) {
                control[this.providers[i].authentication[j].id] = new FormControl(' ');
            }
        }

        for (i = 0; i < this.providers.length; i++) {
            let j: number;
            for (j = 0; j < this.providers[i].burst_config.length; j++) {
                control[this.providers[i].burst_config[j].id] = new FormControl(' ');
            }
        }

        return new FormGroup(control);
    }

    /**
     * Create the cloud provider form
     *
     * @returns {FormGroup}
     */
    createCloudProviderAuthenticationForm(): FormGroup {
        let control = {
            selectedCloudProvider: new FormControl({value: this.provider_config.provider, disabled: true}),
            clusterName: new FormControl({value: this.provider_config.cluster_name, disabled: true}),
            clusterPurgeTime: new FormControl({value: this.provider_config.cluster_purge_time, disabled: true}),
            nodesNumber: new FormControl({value: this.provider_config.cluster_size, disabled: true}),
            headNodeSize: new FormControl({value: this.provider_config.head_node_size, disabled: true}),
            computeNodeSize: new FormControl({value: this.provider_config.compute_node_size, disabled: true}),
            clusterMoveData: new FormControl({value: this.provider_config.cluster_move_data, disabled: true})
        };

        control['bursting'] = new FormControl(' ');

        let i: number;
        for (i = 0; i < this.providers.length; i++) {
            let j: number;
            for (j = 0; j < this.providers[i].authentication.length; j++) {
                if (this.providers[i].name == this.provider_config.provider) {
                    control[this.providers[i].authentication[j].id] = new FormControl('', Validators.required);
                } else {
                    control[this.providers[i].authentication[j].id] = new FormControl(' ');
                }
            }
        }

        for (i = 0; i < this.providers.length; i++) {
            let j: number;
            for (j = 0; j < this.providers[i].burst_config.length; j++) {
                control[this.providers[i].burst_config[j].id] = new FormControl(' ');
            }
        }

        return new FormGroup(control);
    }

    /**
     * Create the cloud provider form
     *
     * @returns {FormGroup}
     */
    createCloudProviderBurstingForm(): FormGroup {
        let control = {
            selectedCloudProvider: new FormControl({value: this.provider_config.provider, disabled: true}),
            clusterName: new FormControl({value: this.provider_config.cluster_name, disabled: true}),
            clusterPurgeTime: new FormControl({value: this.provider_config.cluster_purge_time, disabled: true}),
            nodesNumber: new FormControl({value: this.provider_config.cluster_size, disabled: true}),
            headNodeSize: new FormControl({value: this.provider_config.head_node_size, disabled: true}),
            computeNodeSize: new FormControl({value: this.provider_config.compute_node_size, disabled: true}),
            clusterMoveData: new FormControl({value: this.provider_config.cluster_move_data, disabled: true})
        };

        control['bursting'] = new FormControl(' ');

        let i: number;
        for (i = 0; i < this.providers.length; i++) {
            let j: number;
            for (j = 0; j < this.providers[i].authentication.length; j++) {
                control[this.providers[i].authentication[j].id] = new FormControl(' ');
            }
        }

        for (i = 0; i < this.providers.length; i++) {
            let j: number;
            for (j = 0; j < this.providers[i].burst_config.length; j++) {
                if (this.providers[i].name == this.provider_config.provider) {
                    control[this.providers[i].burst_config[j].id] = new FormControl('', Validators.required);
                } else {
                    control[this.providers[i].burst_config[j].id] = new FormControl(' ');
                }
            }
        }

        return new FormGroup(control);
    }

    showPreviousScreen(): void {
        switch (this.form_state) {
            case(1): {
                this.form_state--;
                this.cluster_name_exists = false;
                (<HTMLInputElement>document.getElementById('back-button')).disabled = true;
                this.showProviderSelectionForm();
                break;
            }
            case(2): {
                this.form_state--;
                document.getElementById('stackManagement').style.display = 'none';
                this.showClusterConfigurationForm(this.provider_config['provider']);
                break;
            }
            case(3): {
                this.form_state--;
                document.getElementById('next-button').style.display = 'block';
                document.getElementById('submit-google-button').style.display = 'none';
                (<HTMLInputElement>document.getElementById('submit-google-button')).disabled = true;
                document.getElementById('submit-oracle-button').style.display = 'none';
                (<HTMLInputElement>document.getElementById('submit-oracle-button')).disabled = true;
                document.getElementById('submit-azure-button').style.display = 'none';
                (<HTMLInputElement>document.getElementById('submit-azure-button')).disabled = true;
                document.getElementById('submit-huawei-button').style.display = 'none';
                (<HTMLInputElement>document.getElementById('submit-huawei-button')).disabled = true;
                document.getElementById('submit-otc-button').style.display = 'none';
                (<HTMLInputElement>document.getElementById('submit-otc-button')).disabled = true;
                //document.getElementById('deploy-azure-cluster').style.display = 'none';
                document.getElementById('google_authentication').style.display = 'none';
                document.getElementById('oracle_authentication').style.display = 'none';
                document.getElementById('azure_authentication').style.display = 'none';
                document.getElementById('huawei_authentication').style.display = 'none';
                document.getElementById('otc_authentication').style.display = 'none';
                //this.showStackConfigurationForm(this.provider_config.clusterName);
                this.showPreviousScreen();
                break;
            }
            case(4): {
                this.form_state--;
                document.getElementById('next-button').innerText = "NEXT";
                this.showAuthenticationForm();
                document.getElementById('providerProgress').style.display = "none";
                document.getElementById('formContent').style.display = "block";
                (<HTMLInputElement>document.getElementById('back-button')).disabled = false;
                (<HTMLInputElement>document.getElementById('next-button')).disabled = false;
                break;
            }
            default:
                break;
        }
    }

    showNextScreen(): void {
        switch (this.form_state) {
            case(0): {
                this.form_state++;
                (<HTMLInputElement>document.getElementById('back-button')).disabled = false;
                this.showClusterConfigurationForm(this.providerForm.value.cloudProvider);
                break;
            }
            case(1): {
                this.form_state++;
                this.showStackConfigurationForm(this.providerForm.value.clusterName);
                break;
            }
            case(2): {
                this.form_state++;
                this.showAuthenticationForm();
                break;
            }
            case(3): {
                this.form_state++;
                document.getElementById('next-button').innerText = "DEPLOY CLUSTER";
                this.showBurstConfigForm(this.provider_config['provider']);
                break;
            }
            case(4): {
                this.form_state++;
                let i: number;
                for (i = 0; i < this.providers.length; i++) {
                    let j: number;
                    for (j = 0; j < this.providers[i].burst_config.length; j++) {
                        if (this.providers[i].name == this.provider_config.provider) {

                            this.provider_config[this.providers[i].burst_config[j].id] = this.providerForm.value[this.providers[i].burst_config[j].id];
                        }
                    }
                }

                document.getElementById('providerProgress').style.display = "block";
                document.getElementById('formContent').style.display = "none";
                (<HTMLInputElement>document.getElementById('back-button')).disabled = true;
                (<HTMLInputElement>document.getElementById('next-button')).disabled = true;
                document.getElementById('next-button').innerText = "FINISH";
                this.dialogTitle = "Create Cluster - Deployment";

                if (this.action == "job-cluster") {
                    console.log("new job cluster");
                } else if (this.action == "on-demand") {
                    const data = {
                        'user_token': localStorage.getItem('TOKEN'),
                        'cluster_name': this.provider_config.cluster_name,
                        'cluster_purge_time': this.provider_config.cluster_purge_time,
                        'cluster_size': this.provider_config.cluster_size,
                        'head_node_size': this.provider_config.head_node_size,
                        'head_node_label': this.provider_config.head_node_label,
                        'compute_node_size': this.provider_config.compute_node_size,
                        'compute_node_label': this.provider_config.compute_node_label,
                        'move_data': this.provider_config.cluster_move_data,
                        'provider_config': this.provider_config,
                        'job': this.job
                    };

                    this._clustersService.createOnDemandCluster(data)
                        .then((response: any) => {
                            document.getElementById('progressBar').style.display = "none";
                            document.getElementById('progressCompleteIcon').style.display = "block";
                            document.getElementById('progressSubtitle').innerText = "Done.";
                            (<HTMLInputElement>document.getElementById('next-button')).disabled = false;
                        }, error => {
                            document.getElementById('progressBar').style.display = "none";
                            document.getElementById('progressErrorIcon').style.display = "block";
                            document.getElementById('progressSubtitle').innerText = "Error.";
                            (<HTMLInputElement>document.getElementById('back-button')).disabled = false;
                        });

                } else {
                    const data = {
                        'user_token': localStorage.getItem('TOKEN'),
                        'cluster_name': this.provider_config.cluster_name,
                        'cluster_purge_time': this.provider_config.cluster_purge_time,
                        'cluster_size': this.provider_config.cluster_size,
                        'head_node_size': this.provider_config.head_node_size,
                        'head_node_label': this.provider_config.head_node_label,
                        'compute_node_size': this.provider_config.compute_node_size,
                        'compute_node_label': this.provider_config.compute_node_label,
                        'move_data': this.provider_config.cluster_move_data,
                        'provider_config': this.provider_config
                    };

                    this._clustersService.createCluster(data)
                        .then((response: any) => {
                            document.getElementById('progressBar').style.display = "none";
                            document.getElementById('progressCompleteIcon').style.display = "block";
                            document.getElementById('progressSubtitle').innerText = "Done.";
                            (<HTMLInputElement>document.getElementById('next-button')).disabled = false;
                        }, error => {
                            document.getElementById('progressBar').style.display = "none";
                            document.getElementById('progressErrorIcon').style.display = "block";
                            document.getElementById('progressSubtitle').innerText = "Error.";
                            (<HTMLInputElement>document.getElementById('back-button')).disabled = false;
                        });
                }
                break;
            }
            case(5): {
                this.matDialogRef.close();
                break;
            }
            default:
                break;
        }
    }

    showProviderSelectionForm(): void {
        this.dialogTitle = 'Create Cluster - Provider Selection';
        document.getElementById('cloudProviderSelection').style.display = 'inline-flex';
        document.getElementById('cloudProviderDescription').style.display = 'block';
        document.getElementById('selectedCloudProvider').style.display = 'none';
        document.getElementById('clusterName').style.display = 'none';
        document.getElementById('clusterPurgeTime').style.display = 'none';
        document.getElementById('nodesNumberLabel').style.display = 'none';
        document.getElementById('nodesNumberSlider').style.display = 'none';
        document.getElementById('nodesNumber').style.display = 'none';
        document.getElementById('clusterMoveData').style.display = 'none';
        document.getElementById('headNodeSize').style.display = 'none';
        document.getElementById('computeNodeSize').style.display = 'none';
        this.providerForm = this.createCloudProviderForm();
    }

    showClusterConfigurationForm(selectedProvider): void {
        this.dialogTitle = 'Create Cluster - Configuration';
        this.provider_config['provider'] = selectedProvider;
        this.selected_provider_index = this.getSelectedProviderIndex();
        document.getElementById('cloudProviderSelection').style.display = 'none';
        document.getElementById('cloudProviderDescription').style.display = 'none';
        document.getElementById(this.provider_config['provider'] + "_authentication").style.display = 'none';
        document.getElementById('selectedCloudProvider').style.display = 'block';
        document.getElementById('clusterName').style.display = 'block';
        document.getElementById('clusterPurgeTime').style.display = 'block';
        document.getElementById('nodesNumberLabel').style.display = 'block';
        document.getElementById('nodesNumberSlider').style.display = 'block';
        document.getElementById('nodesNumber').style.display = 'block';
        document.getElementById('clusterMoveData').style.display = 'block';
        document.getElementById('headNodeSize').style.display = 'block';
        document.getElementById('computeNodeSize').style.display = 'block';
        this.providerForm = this.createClusterConfigurationForm();
    }

    showStackConfigurationForm(clusterName): void {
        //this.dialogTitle = 'Create Cluster - Stack Configuration';
        this.provider_config['cluster_name'] = clusterName;
        this.provider_config['cluster_purge_time'] = this.providerForm.value.clusterPurgeTime;
        this.provider_config['cluster_size'] = this.providerForm.value.nodesNumber;
        this.provider_config['head_node_size'] = this.providerForm.value.headNodeSize;
        this.provider_config['head_node_label'] = this.getInstanceLabel(this.provider_config.head_node_size);
        this.provider_config['compute_node_size'] = this.providerForm.value.computeNodeSize;
        this.provider_config['compute_node_label'] = this.getInstanceLabel(this.provider_config.compute_node_size);
        this.provider_config['cluster_move_data'] = this.providerForm.value.clusterMoveData;
        document.getElementById(this.provider_config.provider + "_bursting").style.display = 'none';
        document.getElementById('clusterName').style.display = 'none';
        document.getElementById('clusterPurgeTime').style.display = 'none';
        document.getElementById('nodesNumberLabel').style.display = 'none';
        document.getElementById('nodesNumberSlider').style.display = 'none';
        document.getElementById('nodesNumber').style.display = 'none';
        document.getElementById('clusterMoveData').style.display = 'none';
        document.getElementById('headNodeSize').style.display = 'none';
        document.getElementById('computeNodeSize').style.display = 'none';
        document.getElementById('stackManagement').style.display = 'block';
        this.showNextScreen();
    }

    showAuthenticationForm(): void {
        document.getElementById('stackManagement').style.display = 'none';
        if (this.provider_config.provider == "Google Cloud") {
            document.getElementById('google_authentication').style.display = 'block';
            document.getElementById('next-button').style.display = 'none';
            document.getElementById('submit-google-button').style.display = 'block';
        } else if (this.provider_config.provider == "Oracle Cloud") {
            document.getElementById('oracle_authentication').style.display = 'block';
            document.getElementById('next-button').style.display = 'none';
            document.getElementById('submit-oracle-button').style.display = 'block';
        } else if (this.provider_config.provider == "Microsoft Azure") {
            document.getElementById('azure_authentication').style.display = 'block';
            document.getElementById('next-button').style.display = 'none';
            document.getElementById('submit-azure-button').style.display = 'block';
        } else if (this.provider_config.provider == "Huawei Cloud") {
            document.getElementById('huawei_authentication').style.display = 'block';
            document.getElementById('next-button').style.display = 'none';
            document.getElementById('submit-huawei-button').style.display = 'block';
        } else if (this.provider_config.provider == "Open Telekom Cloud") {
            document.getElementById('otc_authentication').style.display = 'block';
            document.getElementById('next-button').style.display = 'none';
            document.getElementById('submit-otc-button').style.display = 'block';
        } else {
            document.getElementById(this.provider_config.provider + "_authentication").style.display = 'block';
        }
        this.providerForm = this.createCloudProviderAuthenticationForm();
    }

    showBurstConfigForm(selectedProvider): void {
        this.dialogTitle = 'Create Cluster - Provider Configuration';
        let i: number;
        for (i = 0; i < this.providers.length; i++) {
            let j: number;
            for (j = 0; j < this.providers[i].authentication.length; j++) {
                if (this.providers[i].name == this.provider_config.provider) {

                    this.provider_config[this.providers[i].authentication[j].id] = this.providerForm.value[this.providers[i].authentication[j].id];
                }
            }
        }
        document.getElementById(selectedProvider + "_authentication").style.display = 'none';
        document.getElementById(selectedProvider + "_bursting").style.display = 'block';
        this.providerForm = this.createCloudProviderBurstingForm();
    }

    showNotification(message: string, duration: number, type: string): void {
        this.snackBar.open(message, 'Dismiss', {
            duration: duration,
            panelClass: [type],
            horizontalPosition: 'end',
            verticalPosition: 'bottom'
        });
    }

    activateSubmitGoogle(event) {
        this.selected_file = event.target.files[0];
        (<HTMLInputElement>document.getElementById('submit-google-button')).disabled = false;
    }

    activateSubmitOracle(event) {
        this.selected_file = event.target.files[0];
        (<HTMLInputElement>document.getElementById('submit-oracle-button')).disabled = false;
    }

    activateSubmitAzure(event) {
        this.selected_file = event.target.files[0];
        (<HTMLInputElement>document.getElementById('submit-azure-button')).disabled = false;
    }

    activateSubmitHuawei(event) {
        this.selected_file = event.target.files[0];
        (<HTMLInputElement>document.getElementById('submit-huawei-button')).disabled = false;
    }

    activateSubmitOtc(event) {
        this.selected_file = event.target.files[0];
        (<HTMLInputElement>document.getElementById('submit-otc-button')).disabled = false;
    }

    submitGoogle() {
        document.getElementById('providerProgress').style.display = "block";
        document.getElementById('formContent').style.display = "none";
        document.getElementById('next-button').style.display = "block";
        document.getElementById('submit-google-button').style.display = "none";
        (<HTMLInputElement>document.getElementById('back-button')).disabled = true;
        (<HTMLInputElement>document.getElementById('next-button')).disabled = true;
        (<HTMLInputElement>document.getElementById('submit-google-button')).disabled = true;
        document.getElementById('next-button').innerText = "FINISH";
        this.dialogTitle = "Create Cluster - Provider Configuration";
        this.form_state = 3;


        var reader = new FileReader();
        reader.onload = function () {
            // @ts-ignore
            var obj = JSON.parse(reader.result);

            if (this.action == "job-cluster") {
                console.log("new job cluster google");
            } else if (this.action == "on-demand") {
                const data = {
                    'user_token': localStorage.getItem('TOKEN'),
                    'cluster_name': this.provider_config.cluster_name,
                    'cluster_purge_time': this.provider_config.cluster_purge_time,
                    'cluster_size': this.provider_config.cluster_size,
                    'head_node_size': this.provider_config.head_node_size,
                    'head_node_label': this.provider_config.head_node_label,
                    'compute_node_size': this.provider_config.compute_node_size,
                    'compute_node_label': this.provider_config.compute_node_label,
                    'move_data': this.provider_config.cluster_move_data,
                    'provider_config': {
                        'provider': 'Google Cloud',
                        'credentials': obj
                    },
                    'job': this.job
                };

                this._clustersService.createOnDemandCluster(data)
                    .then((response: any) => {
                        document.getElementById('progressBar').style.display = "none";
                        document.getElementById('progressCompleteIcon').style.display = "block";
                        document.getElementById('progressSubtitle').innerText = "Done.";
                        (<HTMLInputElement>document.getElementById('next-button')).disabled = false;
                    }, error => {
                        document.getElementById('progressBar').style.display = "none";
                        document.getElementById('progressErrorIcon').style.display = "block";
                        document.getElementById('progressSubtitle').innerText = "Error.";
                        (<HTMLInputElement>document.getElementById('back-button')).disabled = false;
                    });

            } else {
                const data = {
                    'user_token': localStorage.getItem('TOKEN'),
                    'cluster_name': this.provider_config.cluster_name,
                    'cluster_purge_time': this.provider_config.cluster_purge_time,
                    'cluster_size': this.provider_config.cluster_size,
                    'head_node_size': this.provider_config.head_node_size,
                    'head_node_label': this.provider_config.head_node_label,
                    'compute_node_size': this.provider_config.compute_node_size,
                    'compute_node_label': this.provider_config.compute_node_label,
                    'move_data': this.provider_config.cluster_move_data,
                    'provider_config': {
                        'provider': 'Google Cloud',
                        'credentials': obj
                    }
                };

                this._clustersService.createCluster(data)
                    .then((response: any) => {
                        document.getElementById('progressBar').style.display = "none";
                        document.getElementById('progressCompleteIcon').style.display = "block";
                        document.getElementById('progressSubtitle').innerText = "Done.";
                        (<HTMLInputElement>document.getElementById('next-button')).disabled = false;
                    }, error => {
                        document.getElementById('progressBar').style.display = "none";
                        document.getElementById('progressErrorIcon').style.display = "block";
                        document.getElementById('progressSubtitle').innerText = "Error.";
                        (<HTMLInputElement>document.getElementById('back-button')).disabled = false;
                    });
            }
        }.bind(this);

        reader.readAsText(this.selected_file);
    }


    submitOracle() {
        document.getElementById('providerProgress').style.display = "block";
        document.getElementById('formContent').style.display = "none";
        document.getElementById('next-button').style.display = "block";
        document.getElementById('submit-oracle-button').style.display = "none";
        (<HTMLInputElement>document.getElementById('back-button')).disabled = true;
        (<HTMLInputElement>document.getElementById('next-button')).disabled = true;
        (<HTMLInputElement>document.getElementById('submit-oracle-button')).disabled = true;
        document.getElementById('next-button').innerText = "FINISH";
        this.dialogTitle = "Add Cloud Provider - Configuration";
        this.form_state = 3;


        var reader = new FileReader();
        reader.onload = function () {
            // @ts-ignore
            var obj = JSON.parse(reader.result);

            if (this.action == "job-cluster") {
                console.log("new job cluster oracle");
            } else if (this.action == "on-demand") {
                const data = {
                    'user_token': localStorage.getItem('TOKEN'),
                    'cluster_name': this.provider_config.cluster_name,
                    'cluster_purge_time': this.provider_config.cluster_purge_time,
                    'cluster_size': this.provider_config.cluster_size,
                    'head_node_size': this.provider_config.head_node_size,
                    'head_node_label': this.provider_config.head_node_label,
                    'compute_node_size': this.provider_config.compute_node_size,
                    'compute_node_label': this.provider_config.compute_node_label,
                    'move_data': this.provider_config.cluster_move_data,
                    'provider_config': {
                        'provider': 'Oracle Cloud',
                        'credentials': obj
                    },
                    'job': this.job
                };

                this._clustersService.createOnDemandCluster(data)
                    .then((response: any) => {
                        document.getElementById('progressBar').style.display = "none";
                        document.getElementById('progressCompleteIcon').style.display = "block";
                        document.getElementById('progressSubtitle').innerText = "Done.";
                        (<HTMLInputElement>document.getElementById('next-button')).disabled = false;
                    }, error => {
                        document.getElementById('progressBar').style.display = "none";
                        document.getElementById('progressErrorIcon').style.display = "block";
                        document.getElementById('progressSubtitle').innerText = "Error.";
                        (<HTMLInputElement>document.getElementById('back-button')).disabled = false;
                    });

            } else {
                const data = {
                    'user_token': localStorage.getItem('TOKEN'),
                    'cluster_name': this.provider_config.cluster_name,
                    'cluster_purge_time': this.provider_config.cluster_purge_time,
                    'cluster_size': this.provider_config.cluster_size,
                    'head_node_size': this.provider_config.head_node_size,
                    'head_node_label': this.provider_config.head_node_label,
                    'compute_node_size': this.provider_config.compute_node_size,
                    'compute_node_label': this.provider_config.compute_node_label,
                    'move_data': this.provider_config.cluster_move_data,
                    'provider_config': {
                        'provider': 'Oracle Cloud',
                        'credentials': obj
                    }
                };

                this._clustersService.createCluster(data)
                    .then((response: any) => {
                        document.getElementById('progressBar').style.display = "none";
                        document.getElementById('progressCompleteIcon').style.display = "block";
                        document.getElementById('progressSubtitle').innerText = "Done.";
                        (<HTMLInputElement>document.getElementById('next-button')).disabled = false;
                    }, error => {
                        document.getElementById('progressBar').style.display = "none";
                        document.getElementById('progressErrorIcon').style.display = "block";
                        document.getElementById('progressSubtitle').innerText = "Error.";
                        (<HTMLInputElement>document.getElementById('back-button')).disabled = false;
                    });
            }
        }.bind(this);

        reader.readAsText(this.selected_file);
    }

    submitAzure() {
        document.getElementById('providerProgress').style.display = "block";
        document.getElementById('formContent').style.display = "none";
        document.getElementById('next-button').style.display = "block";
        document.getElementById('submit-azure-button').style.display = "none";
        (<HTMLInputElement>document.getElementById('back-button')).disabled = true;
        (<HTMLInputElement>document.getElementById('next-button')).disabled = true;
        (<HTMLInputElement>document.getElementById('submit-azure-button')).disabled = true;
        document.getElementById('next-button').innerText = "FINISH";
        this.dialogTitle = "Add Cloud Provider - Configuration";
        this.form_state = 3;


        var reader = new FileReader();
        reader.onload = function () {
            // @ts-ignore
            var obj = JSON.parse(reader.result);

            if (this.action == "job-cluster") {
                console.log("new job cluster azure");
            } else if (this.action == "on-demand") {
                const data = {
                    'user_token': localStorage.getItem('TOKEN'),
                    'cluster_name': this.provider_config.cluster_name,
                    'cluster_purge_time': this.provider_config.cluster_purge_time,
                    'cluster_size': this.provider_config.cluster_size,
                    'head_node_size': this.provider_config.head_node_size,
                    'head_node_label': this.provider_config.head_node_label,
                    'compute_node_size': this.provider_config.compute_node_size,
                    'compute_node_label': this.provider_config.compute_node_label,
                    'move_data': this.provider_config.cluster_move_data,
                    'provider_config': {
                        'provider': 'Microsoft Azure',
                        'credentials': obj
                    },
                    'job': this.job
                };

                this._clustersService.createOnDemandCluster(data)
                    .then((response: any) => {
                        document.getElementById('progressBar').style.display = "none";
                        document.getElementById('progressCompleteIcon').style.display = "block";
                        document.getElementById('progressSubtitle').innerText = "Done.";
                        (<HTMLInputElement>document.getElementById('next-button')).disabled = false;
                    }, error => {
                        document.getElementById('progressBar').style.display = "none";
                        document.getElementById('progressErrorIcon').style.display = "block";
                        document.getElementById('progressSubtitle').innerText = "Error.";
                        (<HTMLInputElement>document.getElementById('back-button')).disabled = false;
                    });

            } else {
                const data = {
                    'user_token': localStorage.getItem('TOKEN'),
                    'cluster_name': this.provider_config.cluster_name,
                    'cluster_purge_time': this.provider_config.cluster_purge_time,
                    'cluster_size': this.provider_config.cluster_size,
                    'head_node_size': this.provider_config.head_node_size,
                    'head_node_label': this.provider_config.head_node_label,
                    'compute_node_size': this.provider_config.compute_node_size,
                    'compute_node_label': this.provider_config.compute_node_label,
                    'move_data': this.provider_config.cluster_move_data,
                    'provider_config': {
                        'provider': 'Microsoft Azure',
                        'credentials': obj
                    }
                };

                this._clustersService.createCluster(data)
                    .then((response: any) => {
                        document.getElementById('progressBar').style.display = "none";
                        document.getElementById('progressCompleteIcon').style.display = "block";
                        document.getElementById('progressSubtitle').innerText = "Done.";
                        (<HTMLInputElement>document.getElementById('next-button')).disabled = false;
                    }, error => {
                        document.getElementById('progressBar').style.display = "none";
                        document.getElementById('progressErrorIcon').style.display = "block";
                        document.getElementById('progressSubtitle').innerText = "Error.";
                        (<HTMLInputElement>document.getElementById('back-button')).disabled = false;
                    });
            }
        }.bind(this);

        reader.readAsText(this.selected_file);
    }

    submitHuawei() {
        document.getElementById('providerProgress').style.display = "block";
        document.getElementById('formContent').style.display = "none";
        document.getElementById('next-button').style.display = "block";
        document.getElementById('submit-huawei-button').style.display = "none";
        (<HTMLInputElement>document.getElementById('back-button')).disabled = true;
        (<HTMLInputElement>document.getElementById('next-button')).disabled = true;
        (<HTMLInputElement>document.getElementById('submit-huawei-button')).disabled = true;
        document.getElementById('next-button').innerText = "FINISH";
        this.dialogTitle = "Add Cloud Provider - Configuration";
        this.form_state = 3;


        var reader = new FileReader();
        reader.onload = function () {
            // @ts-ignore
            var obj = JSON.parse(reader.result);

            if (this.action == "job-cluster") {
                console.log("new job cluster huawei");
            } else if (this.action == "on-demand") {
                const data = {
                    'user_token': localStorage.getItem('TOKEN'),
                    'cluster_name': this.provider_config.cluster_name,
                    'cluster_purge_time': this.provider_config.cluster_purge_time,
                    'cluster_size': this.provider_config.cluster_size,
                    'head_node_size': this.provider_config.head_node_size,
                    'head_node_label': this.provider_config.head_node_label,
                    'compute_node_size': this.provider_config.compute_node_size,
                    'compute_node_label': this.provider_config.compute_node_label,
                    'move_data': this.provider_config.cluster_move_data,
                    'provider_config': {
                        'provider': 'Huawei Cloud',
                        'credentials': obj
                    },
                    'job': this.job
                };

                this._clustersService.createOnDemandCluster(data)
                    .then((response: any) => {
                        document.getElementById('progressBar').style.display = "none";
                        document.getElementById('progressCompleteIcon').style.display = "block";
                        document.getElementById('progressSubtitle').innerText = "Done.";
                        (<HTMLInputElement>document.getElementById('next-button')).disabled = false;
                    }, error => {
                        document.getElementById('progressBar').style.display = "none";
                        document.getElementById('progressErrorIcon').style.display = "block";
                        document.getElementById('progressSubtitle').innerText = "Error.";
                        (<HTMLInputElement>document.getElementById('back-button')).disabled = false;
                    });

            } else {
                const data = {
                    'user_token': localStorage.getItem('TOKEN'),
                    'cluster_name': this.provider_config.cluster_name,
                    'cluster_purge_time': this.provider_config.cluster_purge_time,
                    'cluster_size': this.provider_config.cluster_size,
                    'head_node_size': this.provider_config.head_node_size,
                    'head_node_label': this.provider_config.head_node_label,
                    'compute_node_size': this.provider_config.compute_node_size,
                    'compute_node_label': this.provider_config.compute_node_label,
                    'move_data': this.provider_config.cluster_move_data,
                    'provider_config': {
                        'provider': 'Huawei Cloud',
                        'credentials': obj
                    }
                };

                this._clustersService.createCluster(data)
                    .then((response: any) => {
                        document.getElementById('progressBar').style.display = "none";
                        document.getElementById('progressCompleteIcon').style.display = "block";
                        document.getElementById('progressSubtitle').innerText = "Done.";
                        (<HTMLInputElement>document.getElementById('next-button')).disabled = false;
                    }, error => {
                        document.getElementById('progressBar').style.display = "none";
                        document.getElementById('progressErrorIcon').style.display = "block";
                        document.getElementById('progressSubtitle').innerText = "Error.";
                        (<HTMLInputElement>document.getElementById('back-button')).disabled = false;
                    });
            }
        }.bind(this);

        reader.readAsText(this.selected_file);
    }

    submitOtc() {
        document.getElementById('providerProgress').style.display = "block";
        document.getElementById('formContent').style.display = "none";
        document.getElementById('next-button').style.display = "block";
        document.getElementById('submit-otc-button').style.display = "none";
        (<HTMLInputElement>document.getElementById('back-button')).disabled = true;
        (<HTMLInputElement>document.getElementById('next-button')).disabled = true;
        (<HTMLInputElement>document.getElementById('submit-otc-button')).disabled = true;
        document.getElementById('next-button').innerText = "FINISH";
        this.dialogTitle = "Add Cloud Provider - Configuration";
        this.form_state = 3;


        var reader = new FileReader();
        reader.onload = function () {
            // @ts-ignore
            var obj = JSON.parse(reader.result);

            if (this.action == "job-cluster") {
                console.log("new job cluster otc");
            } else if (this.action == "on-demand") {
                const data = {
                    'user_token': localStorage.getItem('TOKEN'),
                    'cluster_name': this.provider_config.cluster_name,
                    'cluster_purge_time': this.provider_config.cluster_purge_time,
                    'cluster_size': this.provider_config.cluster_size,
                    'head_node_size': this.provider_config.head_node_size,
                    'head_node_label': this.provider_config.head_node_label,
                    'compute_node_size': this.provider_config.compute_node_size,
                    'compute_node_label': this.provider_config.compute_node_label,
                    'move_data': this.provider_config.cluster_move_data,
                    'provider_config': {
                        'provider': 'Open Telekom Cloud',
                        'credentials': obj
                    },
                    'job': this.job
                };

                this._clustersService.createOnDemandCluster(data)
                    .then((response: any) => {
                        document.getElementById('progressBar').style.display = "none";
                        document.getElementById('progressCompleteIcon').style.display = "block";
                        document.getElementById('progressSubtitle').innerText = "Done.";
                        (<HTMLInputElement>document.getElementById('next-button')).disabled = false;
                    }, error => {
                        document.getElementById('progressBar').style.display = "none";
                        document.getElementById('progressErrorIcon').style.display = "block";
                        document.getElementById('progressSubtitle').innerText = "Error.";
                        (<HTMLInputElement>document.getElementById('back-button')).disabled = false;
                    });

            } else {
                const data = {
                    'user_token': localStorage.getItem('TOKEN'),
                    'cluster_name': this.provider_config.cluster_name,
                    'cluster_purge_time': this.provider_config.cluster_purge_time,
                    'cluster_size': this.provider_config.cluster_size,
                    'head_node_size': this.provider_config.head_node_size,
                    'head_node_label': this.provider_config.head_node_label,
                    'compute_node_size': this.provider_config.compute_node_size,
                    'compute_node_label': this.provider_config.compute_node_label,
                    'move_data': this.provider_config.cluster_move_data,
                    'provider_config': {
                        'provider': 'Open Telekom Cloud',
                        'credentials': obj
                    }
                };

                this._clustersService.createCluster(data)
                    .then((response: any) => {
                        document.getElementById('progressBar').style.display = "none";
                        document.getElementById('progressCompleteIcon').style.display = "block";
                        document.getElementById('progressSubtitle').innerText = "Done.";
                        (<HTMLInputElement>document.getElementById('next-button')).disabled = false;
                    }, error => {
                        document.getElementById('progressBar').style.display = "none";
                        document.getElementById('progressErrorIcon').style.display = "block";
                        document.getElementById('progressSubtitle').innerText = "Error.";
                        (<HTMLInputElement>document.getElementById('back-button')).disabled = false;
                    });
            }
        }.bind(this);

        reader.readAsText(this.selected_file);
    }

    getSelectedProviderIndex() {
        for (let i = 0; i < this.providers.length; i++) {
            if (this.providers[i].name == this.provider_config.provider)
                return i;
        }
    }

    getInstanceLabel(instance_size) {
        for (let i = 0; i < this.providers[this.selected_provider_index].instance_sizes.length; i++) {
            if (this.providers[this.selected_provider_index].instance_sizes[i].value == instance_size)
                return this.providers[this.selected_provider_index].instance_sizes[i].displayName;
        }
    }

    checkClusterNameAvailability() {
        this._clustersService.checkClusterName(this.providerForm.value.clusterName)
            .then(result => {
                this.cluster_name_exists = result;
            });
    }
}

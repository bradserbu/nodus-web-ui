import {Component, Inject, ViewEncapsulation} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatDialog} from '@angular/material';
import {CloudProviderFormDialogComponent} from "../cloud-provider-form/cloud-provider-form.component";


@Component({
    selector: 'welcome-dialog',
    templateUrl: './welcome-form.component.html',
    styleUrls: ['./welcome-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class WelcomeDialogComponent {
    dialogRef: any;
    action: string;
    dialogTitle: string;

    user: any;

    /**
     * Constructor
     *
     * @param {MatDialogRef<CloudProviderFormDialogComponent>} matDialogRef
     * @param _data
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        private _matDialog: MatDialog,
        public matDialogRef: MatDialogRef<WelcomeDialogComponent>,
        @Inject(MAT_DIALOG_DATA) private _data: any
    ) {
        this.action = _data.action;
        this.dialogTitle = 'Getting Started';

        this.setUser();
    }

    /**
     * setUser
     */
    setUser(): void {
        const token = localStorage.getItem('TOKEN');
        if(token != null) {
            var base64Url = token.split('.')[1];
            var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
            this.user = JSON.parse(window.atob(base64));
        }
        else {
            this.user = {};
        }
    }

    openCloudProviderForm(): void {
        this.matDialogRef.close();
        /*this.dialogRef = this._matDialog.open(CloudProviderFormDialogComponent, {
            panelClass: 'cloud-provider-form-dialog',
            data: {
                action: 'new'
            }
        });*/
    }
}

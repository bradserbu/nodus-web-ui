import {Component, OnInit, OnDestroy, Inject, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef, MatSnackBar} from '@angular/material';

@Component({
    selector: 'stack-details-form-dialog',
    templateUrl: './stack-details-form.component.html',
    styleUrls: ['./stack-details-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class StackDetailsFormComponent implements OnInit, OnDestroy {
    stackDetailsForm: FormGroup;
    dialogTitle: string;

    stack: any;
    stack_values: any;
    stack_view: string;

    /**
     * Constructor
     *
     * @param {MatDialogRef<CloudProviderFormDialogComponent>} matDialogRef
     * @param _data
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        public matDialogRef: MatDialogRef<StackDetailsFormComponent>,
        public snackBar: MatSnackBar,
        @Inject(MAT_DIALOG_DATA) private _data: any,
        private _formBuilder: FormBuilder
    ) {
        this.stack = _data.stack;
        this.stack_values = [];

        this.dialogTitle = this.stack.name;
        this.stack_view = "list";
    }

    /**
     * On init
     */
    ngOnInit(): void {
        for (var key in this.stack.data) {
            if (this.stack.data.hasOwnProperty(key)) {
                const current_value = {
                  [key]: this.stack.data[key]
                };

                this.stack_values.push(current_value);
            }
        }

        this.stackDetailsForm = this.createStackDetailsForm();
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {

    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Create the submit job form
     *
     * @returns {FormGroup}
     */
    createStackDetailsForm(): FormGroup {
        const control = {
            stackName: new FormControl({value: this.stack.data.name, disabled: false}),
            stackVersion: new FormControl({value: this.stack.data.version, disabled: false}),
            stackImage: new FormControl({value: " ", disabled: false}),
            stackFiles: new FormControl({value: this.stack.data.files, disabled: false}),
            stackBootstrap: new FormControl({value: this.stack.data.bootstrap, disabled: false}),
            stackTasks: new FormControl({value: " ", disabled: false})
        };

        /*for (let i = 0; i < this.stack.data.tasks.length; i++) {
            control["task_" + i + "_type"] = new FormControl({value: this.stack.data.tasks[i].type, disabled: false});
        }*/

        return new FormGroup(control);
    }

}

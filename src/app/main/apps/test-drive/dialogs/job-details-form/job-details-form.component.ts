import {Component, OnInit, OnDestroy, Inject, ViewEncapsulation} from '@angular/core';
import {Subject} from 'rxjs';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef, MatSnackBar} from '@angular/material';

import {HttpClient} from '@angular/common/http';

@Component({
    selector: 'job-details-form-dialog',
    templateUrl: './job-details-form.component.html',
    styleUrls: ['./job-details-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class JobDetailsFormDialogComponent implements OnInit, OnDestroy {
    job_id: string;
    job: any;
    jobDetailsForm: FormGroup;
    dialogTitle: string;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {MatDialogRef<CloudProviderFormDialogComponent>} matDialogRef
     * @param _data
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        public matDialogRef: MatDialogRef<JobDetailsFormDialogComponent>,
        public snackBar: MatSnackBar,
        @Inject(MAT_DIALOG_DATA) private _data: any,
        private _formBuilder: FormBuilder,
        private _httpClient: HttpClient
    ) {
        this._unsubscribeAll = new Subject();

        this.dialogTitle = 'Details - ' + _data.job_id;
        this.job = _data.job;
    }

    /**
     * On init
     */
    ngOnInit(): void {
        this.jobDetailsForm = this.createJobDetailsForm();
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Create the submit job form
     *
     * @returns {FormGroup}
     */
    createJobDetailsForm(): FormGroup {
        return new FormGroup({
            jobState: new FormControl({value: this.job.state, disabled: false}),
            jobStdOutput: new FormControl({value: this.job.std_output, disabled: false}),
            jobStdError: new FormControl({value: this.job.std_error, disabled: false})
        });
    }
}

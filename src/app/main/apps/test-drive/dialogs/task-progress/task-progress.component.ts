import {Component, Inject, ViewEncapsulation, OnDestroy, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatDialog} from '@angular/material';
import { MatStepper } from '@angular/material';
import * as socketIo from 'socket.io-client';

import {Socket} from "../../interfaces";

import {environment} from "../../../../../../environments/environment";

@Component({
    selector: 'task-progress',
    templateUrl: './task-progress.component.html',
    styleUrls: ['./task-progress.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class TaskProgressComponent implements OnDestroy {
    @ViewChild('progressview') progressview: MatStepper;

    dialogRef: any;
    action: string;
    dialogTitle: string;
    process: any;
    socket: Socket;
    tasks: any;
    date: any;

    /**
     * Constructor
     *
     * @param {MatDialogRef<TaskProgressComponent>} matDialogRef
     * @param _data
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        private _matDialog: MatDialog,
        public matDialogRef: MatDialogRef<TaskProgressComponent>,
        @Inject(MAT_DIALOG_DATA) private _data: any
    ) {

        this.action = _data.action;
        this.process = _data.process;
        this.dialogTitle = _data.title;
        this.tasks = this.process.tasks;


        this.socket = socketIo.connect(`${environment.socketURL}`);
        //this.date = updateCurrentTime();
        //this.process.tasks.forEach(updateTask);

        this.socket.emit('monitor-process-status', this.process.id);
        this.socket.on(this.process.id, (process => {
            for(var i = 0; i < process.data.length; i++) {
                this.date = new Date().getTime();
                for (var task in this.tasks) {
                    if (process.data[i].state == 'Active')
                        this.progressview.selectedIndex = parseInt(task);
                    if (this.tasks[task].id == process.data[i].id) {
                        this.tasks[task] = process.data[i];
                        break;
                    }
                }
            }
        }));
        /*for (var i = 0; i < this.process.tasks.length; i++) {
            this.socket.on(this.process.tasks[i].id, (res) => {
                this.date = new Date().getTime();
                for (var task in this.tasks) {
                    if(res.data.state=='Active')
                        this.progressview.selectedIndex = parseInt(task);
                    if (this.tasks[task].id == res.data.id) {
                        this.tasks[task] = res.data;
                        break;
                    }
                }
            });
        }*/

        /*function updateCurrentTime() {
            return new Date().getTime();
        }

        function updateTask(task) {
            this.socket.on(task.id, (response) => {
                for (var task in this.tasks) {
                    if(response.data.state=='Active')
                        this.progressview.selectedIndex = parseInt(task);
                    if (this.tasks[task].id == response.data.id) {
                        this.tasks[task] = response.data;
                        break;
                    }
                }
            });
        }*/
    }

    ngOnDestroy(): void {
        this.socket.emit('disconnect-client', this.process.id);
    }
}

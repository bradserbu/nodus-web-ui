import {Component, Inject, ViewEncapsulation, OnDestroy} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatDialog} from '@angular/material';
import {interval} from 'rxjs';
import {ClustersService} from "../../clusters.services";

@Component({
    selector: 'logs-dialog',
    templateUrl: './logs-dialog.component.html',
    styleUrls: ['./logs-dialog.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class LogsDialogComponent implements OnDestroy {
    dialogRef: any;
    dialogTitle: string;
    log_data: any;
    cluster: any;

    refresh_timer: any;

    /**
     * Constructor
     *
     * @param {MatDialogRef<CloudProviderFormDialogComponent>} matDialogRef
     * @param _data
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        private _clustersService: ClustersService,
        private _matDialog: MatDialog,
        public matDialogRef: MatDialogRef<LogsDialogComponent>,
        @Inject(MAT_DIALOG_DATA) private _data: any
    ) {
        this.cluster = _data.cluster;
        this.dialogTitle = 'Deploy Logs - ' + this.cluster.name;

        this.updateLogData();

        this.refresh_timer = interval(2000).subscribe(x => {
            this.updateLogData();
        });
    }

    updateLogData() {
        this._clustersService.getClusterDeployLogs(this.cluster)
            .then(response => {
                this.log_data = response;
                document.getElementById('content').innerHTML = this.log_data;
                /*let content_area = document.getElementById('content');
                content_area.scrollTop = content_area.scrollHeight - content_area.clientHeight;
                window.scrollTo(0, document.body.scrollHeight);*/
            });
    }

    ngOnDestroy(): void {
        this.refresh_timer.unsubscribe();
    }
}

import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {BehaviorSubject, Subject, Observable, interval} from 'rxjs';
import {MatSnackBar} from '@angular/material';
import {Socket} from "./interfaces";
import * as socketIo from 'socket.io-client';

import {environment} from "../../../../environments/environment";


@Injectable()
export class TestDriveService implements Resolve<any> {
    testdrive: any;
    workload: any;
    nodes: any;
    dashboard_quick_view: any;
    workload_nodes_progression: any;
    activities: any;
    template_stacks: any;
    bursting_configuration: any;
    shortcut_items: any;
    notifications: any;

    testdriveOnChanged: BehaviorSubject<any>;
    onWorkloadChanged: BehaviorSubject<any>;
    onNodesChanged: BehaviorSubject<any>;
    onDashboardQuickViewChanged: BehaviorSubject<any>;
    onWorkloadNodesProgressionChanged: BehaviorSubject<any>;
    onActivitiesChanged: BehaviorSubject<any>;
    onTemplateStacksChanged: BehaviorSubject<any>
    onBurstingConfigurationChanged: BehaviorSubject<any>;
    onShortcutItemsChanged: BehaviorSubject<any>;
    onNotificationsChanged: BehaviorSubject<any>;

    nodeList: any;
    nodeListAvailable: [];
    nodeListBusy: [];
    onNodesListUpdated: BehaviorSubject<any>;
    onNodesFilterChanged: Subject<any>;
    filterNodesBy: string;

    jobList: any;
    jobListRunning: [];
    jobListQueued: [];
    onWorkloadListUpdated: BehaviorSubject<any>;
    onWorkloadFilterChanged: Subject<any>;
    filterWorkloadBy: string;

    socket: Socket;


    refresh_timer: any;
    searchText: string;
    filterBy: string;


    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        public snackBar: MatSnackBar,
        private _httpClient: HttpClient
    ) {
        // Set the defaults
        this.testdriveOnChanged = new BehaviorSubject({});
        this.onWorkloadChanged = new BehaviorSubject([]);
        this.onNodesChanged = new BehaviorSubject([]);
        this.onDashboardQuickViewChanged = new BehaviorSubject({});
        this.onWorkloadNodesProgressionChanged = new BehaviorSubject({});
        this.onActivitiesChanged = new BehaviorSubject({});
        this.onTemplateStacksChanged = new BehaviorSubject([]);
        this.onBurstingConfigurationChanged = new BehaviorSubject({});
        this.onShortcutItemsChanged = new BehaviorSubject([]);
        this.onNotificationsChanged = new BehaviorSubject<any>([]);

        this.onNodesListUpdated = new BehaviorSubject<any>([]);
        this.onNodesFilterChanged = new Subject();
        this.onNodesFilterChanged.subscribe(filter => {
            this.filterNodesBy = filter;
        });

        this.onWorkloadListUpdated = new BehaviorSubject<any>([]);
        this.onWorkloadFilterChanged = new Subject();
        this.onWorkloadFilterChanged.subscribe(filter => {
           this.filterWorkloadBy = filter;
        });

        this.refresh_timer = interval(5000).subscribe(x => {
            this.updateDashboardData();
        });
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return new Promise((resolve, reject) => {
            Promise.all([
                this.getTestDrive(),
                this.updateDashboardData(),
                this.getTemplateStacks(),
                this.getBurstingConfiguration()
            ]).then(() => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * addCloudProvider
     */
    addCloudProvider(data): Promise<any> {
        return new Promise((resolve, reject) => {
            this._httpClient.post(`${environment.apiURL}/addCloudProvider`, data)
                .subscribe((response: any) => {

                });
            this.socket = socketIo.connect(`${environment.socketURL}`, {
                query: {
                    user_token: localStorage.getItem('TOKEN')
                }
            });
            this.socket.emit('provider-deploy-listener', localStorage.getItem('TOKEN'));
            this.socket.on('deploy-finished', () =>{
                    resolve();
                    const title = data.provider_config.provider;
                    const message = " cluster was configured.";
                    this.showNotification(title + message, 5000, 'notification');
                    this.logActivity('cloud_provider', title, 'assets/images/providers/' + data.provider_config.provider + '.png', message);
                    //this.getBurstingConfiguration();
                });
        });
    }

    deployAzureCluster(token): Promise<any> {
        console.log("adding azure cluster...");
        return new Promise((resolve, reject) => {
            this._httpClient.post(`${environment.apiURL}/deployAzureCluster`, token)
                .subscribe((response: any) => {
                    console.log("deploy finished");
                    console.log(response);
                    resolve(response);
                    const title = "Microsoft Azure";
                    const message = " cluster was configured.";
                    this.showNotification(title + message, 5000, 'notification');
                    this.logActivity('cloud_provider', title, 'assets/images/providers/Microsoft Azure.png', message);
                    this.getBurstingConfiguration();
                });
        });
    }

    deployClusterNodes(data): Promise<any> {
        return new Promise((resolve, reject) => {
            this._httpClient.post(`${environment.apiURL}/deployClusterNodes`, data)
                .subscribe((response: any) => {
                    /*const title = "Microsoft Azure Node";
                    const message = " added to the cluster.";
                    this.showNotification(title + message, 5000, 'notification');
                    this.logActivity('cloud_provider', title, 'assets/images/providers/Microsoft Azure.png', message);*/
                    this.getBurstingConfiguration();
                });
        });
    }

    destroyClusterNodes(data): Promise<any> {
        return new Promise((resolve, reject) => {
            this._httpClient.post(`${environment.apiURL}/destroyClusterNodes`, data)
                .subscribe((response: any) => {
                    /*const title = "Microsoft Azure Node";
                    const message = " was removed from the cluster.";
                    this.showNotification(title + message, 5000, 'notification');
                    this.logActivity('cloud_provider', title, 'assets/images/providers/Microsoft Azure.png', message);*/
                    this.getBurstingConfiguration();
                });
        });
    }

    destroyClusterNode(data): Promise<any> {
        return new Promise((resolve, reject) => {
           this._httpClient.post(`${environment.apiURL}/destroyClusterNode`, data)
               .subscribe((response: any) => {
                   const title = "Microsoft Azure Node";
                   const message = " was removed from the cluster.";
                   this.showNotification(title + message, 5000, 'notification');
                   this.logActivity('cloud_provider', title, 'assets/images/providers/Microsoft Azure.png', message);
                   this.getBurstingConfiguration();
               });
        });
    }

    destroyCluster(token): Promise<any> {
        return new Promise((resolve, reject) => {
            this._httpClient.post(`${environment.apiURL}/destroyCluster`, token)
                .subscribe((response: any) => {
                    /*const title = "Microsoft Azure";
                    const message = " cluster was destroyed.";
                    this.showNotification(title + message, 5000, 'notification');
                    this.logActivity('cloud_provider', title, 'assets/images/providers/Microsoft Azure.png', message);*/
                    this.getBurstingConfiguration();
                });
        });
    }

    /**
     * Get Test-Drive Data
     */
    getTestDrive(): Promise<any[]> {
        return new Promise((resolve, reject) => {
            this._httpClient.get('api/test-drive')
                .subscribe((testdrive: any) => {
                    this.testdrive = testdrive;
                    this.testdriveOnChanged.next(this.testdrive);
                    resolve(this.testdrive);
                }, reject);
        });
    }

    /**
     * getNotifications
     */
    getNotifications(): Promise<any> {
        return new Promise((resolve, reject) => {
            const user = {
                'user_token': localStorage.getItem('TOKEN')
            };

            this._httpClient.post(`${environment.apiURL}/getNotifications`, user)
                .subscribe((response: any) => {
                    this.notifications = response.data;
                    this.onNotificationsChanged.next(this.notifications);
                    resolve(this.notifications);
                }, reject);
        });
    }

    /**
     * getTemplateStack
     *
     * @returns {Promise<any>}
     */
    getTemplateStacks(): Promise<any> {
        return new Promise((resolve, reject) => {
            const user = {
                'user_token': localStorage.getItem('TOKEN')
            };

            this._httpClient.post(`${environment.apiURL}/getTemplateStack`, user)
                .subscribe((response: any) => {
                    this.template_stacks = response.data;
                    this.onTemplateStacksChanged.next(this.template_stacks);
                    resolve(this.template_stacks);
                }, reject);
        });
    }

    /**
     * getBurstingConfiguration
     *
     * @returns {Promise<any>}
     */
    getBurstingConfiguration(): Promise<any> {
        return new Promise((resolve, reject) => {
            const user = {
                'user_token': localStorage.getItem('TOKEN')
            };

            this._httpClient.post(`${environment.apiURL}/getBurstingConfiguration`, user)
                .subscribe((response: any) => {
                    this.bursting_configuration = response.data;
                    this.onBurstingConfigurationChanged.next(this.bursting_configuration);
                    resolve(this.bursting_configuration);
                }, reject);
        });
    }

    /**
     * getShortcutItems
     *
     * @returns {Promise<any>}
     */
    getShortcutItems(): Promise<any> {
        return new Promise((resolve, reject) => {
            const user = {
                'user_token': localStorage.getItem('TOKEN')
            };

            this._httpClient.post(`${environment.apiURL}/getShortcutItems`, user)
                .subscribe((response: any) => {
                    this.shortcut_items = response.data.shortcuts;
                    this.onShortcutItemsChanged.next(this.shortcut_items);
                    resolve(this.shortcut_items);
                }, reject);
        });
    }

    /**
     * updateDashboardData
     *
     * @returns {Promise<any>}
     */
    updateDashboardData(): Promise<any> {
        return new Promise((resolve, reject) => {
                const user = {
                    'user_token': localStorage.getItem('TOKEN')
                };

                this._httpClient.post(`${environment.apiURL}/updateDashboardData`, user)
                    .subscribe((response: any) => {

                        // get workload
                        this.workload = response.data.queue.jobs;
                        this.onWorkloadChanged.next(this.workload);

                        // NEW WORKLOAD LIST
                        this.jobList = response.data.queue.jobs;
                        this.jobListRunning = [];
                        this.jobListQueued = [];

                        this.jobList.forEach(function (job) {
                            switch(job.job_state) {
                                case 'R':
                                    this.jobListRunning.push(job);
                                    break;
                                case 'Q':
                                    this.jobListQueued.push(job);
                                    break;
                                default:
                                    break;
                            }
                        }.bind(this));

                        const workload = {
                            'jobList': this.jobList,
                            'jobListRunning': this.jobListRunning,
                            'jobListQueued': this.jobListQueued
                        };

                        this.onWorkloadListUpdated.next(workload);

                        // get nodes
                        this.nodes = response.data.nodes.nodes;
                        this.onNodesChanged.next(this.nodes);

                        // NEW NODES LIST
                        this.nodeList = response.data.nodes.nodes;
                        this.nodeListAvailable = [];
                        this.nodeListBusy = [];

                        this.nodeList.forEach(function (node) {
                           switch(node.state) {
                               case 'free':
                                   this.nodeListAvailable.push(node);
                                   break;
                               case 'job-exclusive':
                                   this.nodeListBusy.push(node);
                                   break;
                               default:
                                   break;
                           }
                        }.bind(this));

                        const nodes = {
                            'nodeList': this.nodeList,
                            'nodeListAvailable': this.nodeListAvailable,
                            'nodeListBusy': this.nodeListBusy
                        };

                        this.onNodesListUpdated.next(nodes);



                        // set quick view data for workload and nodes
                        this.dashboard_quick_view = {
                            'queue_total': response.data.queue.total,
                            'queue_running': response.data.queue.running,
                            'queue_queued': response.data.queue.queued,
                            'queue_blocked': response.data.queue.blocked,
                            'nodes_total': response.data.nodes.total,
                            'nodes_available': response.data.nodes.available,
                            'nodes_busy': response.data.nodes.busy,
                            'nodes_down': response.data.nodes.down,
                            'jobs_failed': 0,
                            'jobs_total': response.data.job_stats.total,
                            'jobs_today': response.data.job_stats.today,
                            'jobs_week': response.data.job_stats.week,
                            'jobs_month': response.data.job_stats.month,
                            'jobs_year': response.data.job_stats.year,
                            'cluster_total': response.data.cluster_stats.total,
                            'cluster_aws': response.data.cluster_stats.aws,
                            'cluster_google': response.data.cluster_stats.google,
                            'cluster_azure': response.data.cluster_stats.azure,
                            'cluster_oracle': response.data.cluster_stats.oracle,
                            'cluster_online': response.data.cluster_stats.online
                        };
                        this.onDashboardQuickViewChanged.next(this.dashboard_quick_view);

                        // set workload/nodes progression
                        this.workload_nodes_progression = response.data.workload_nodes_progression;
                        this.onWorkloadNodesProgressionChanged.next(this.workload_nodes_progression);

                        // set activities
                        this.activities = response.data.activities;
                        this.onActivitiesChanged.next(this.activities);

                        resolve();
                    }, error => {
                        if (error.error.error) {
                            if (error.error.error.message == "invalid token") {
                                localStorage.removeItem('TOKEN');
                                window.location.reload();
                            }
                        }
                    }, reject);
            }
        );
    }

    logNotification(notification): Promise<any> {
        return new Promise((resolve, reject) => {
            const logNotification = {
                'user_token': localStorage.getItem('TOKEN'),
                'notification': notification
            };

            this._httpClient.post(`${environment.apiURL}/logNotification`, logNotification)
                .subscribe((response: any) => {
                    this.notifications = response.data;
                    this.onNotificationsChanged.next(this.notifications);
                    resolve(this.notifications);
                }, reject);
        });
    }

    showNotification(message: string, duration: number, type: string): void {
        this.snackBar.open(message, 'Dismiss', {
            duration: duration,
            panelClass: [type],
            horizontalPosition: 'end',
            verticalPosition: 'bottom'
        });
    }

    logActivity(activity_type, activity_title, activity_icon, message) {
        const logActivity = {
            'user_token': localStorage.getItem('TOKEN'),
            'activity': {
                'type': activity_type,
                'title': {
                    'name': activity_title,
                    'icon': activity_icon
                },
                'message': message
            }
        };

        this._httpClient.post(`${environment.apiURL}/logActivity`, logActivity)
            .subscribe((response: any) => {
            });
    }

    unsubscribe(): void {
        this.refresh_timer.unsubscribe();
    }
}

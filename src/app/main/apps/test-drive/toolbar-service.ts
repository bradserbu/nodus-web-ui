import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject} from 'rxjs';

import {environment} from "../../../../environments/environment";


@Injectable()
export class ToolbarService {
    shortcut_items: any;
    onShortcutItemsChanged: BehaviorSubject<any>;


    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    ) {
        // Set the defaults
        this.onShortcutItemsChanged = new BehaviorSubject([]);
        this.getShortcutItems();
    }

    /**
     * getShortcutItems
     *
     * @returns {Promise<any>}
     */
    getShortcutItems(): Promise<any> {
        return new Promise((resolve, reject) => {
            const user = {
                'user_token': localStorage.getItem('TOKEN')
            };

            this._httpClient.post(`${environment.apiURL}/getShortcutItems`, user)
                .subscribe((response: any) => {
                    this.shortcut_items = response.data.shortcuts;
                    this.onShortcutItemsChanged.next(this.shortcut_items);
                    resolve(this.shortcut_items);
                }, reject);
        });
    }
}

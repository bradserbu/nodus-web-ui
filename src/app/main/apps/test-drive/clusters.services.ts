import {Injectable} from '@angular/core';
import {BehaviorSubject, Subject, interval} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {MatSnackBar} from '@angular/material';
import {Socket} from "./interfaces";
import * as socketIo from 'socket.io-client';
import {saveAs} from 'file-saver';

import {environment} from "../../../../environments/environment";

@Injectable()
export class ClustersService {
    clusters: any;
    clusterList: any;
    clusterListAws: [];
    clusterListGoogle: [];
    clusterListAzure: [];
    clusterListOracle: [];
    clusterListHuawei: [];
    clusterListOtc: [];
    providers: any;
    jobs: any;
    stackOptions: any;
    selected_cluster: any;
    onProvidersChanged: BehaviorSubject<any>;
    onJobsChanged: BehaviorSubject<any>;
    onClustersChanged: BehaviorSubject<any>;
    onClusterListUpdated: BehaviorSubject<any>;
    onSelectedClusterChanged: BehaviorSubject<any>;
    onSearchTextChanged: Subject<any>;
    onFilterChanged: Subject<any>;
    onStackOptionsChanged: BehaviorSubject<any>;

    searchText: string;
    filterBy: string;

    socket: Socket;

    refresh_timer: any;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        public snackBar: MatSnackBar,
        private _httpClient: HttpClient
    ) {
        this.onProvidersChanged = new BehaviorSubject(<any>[]);
        this.onJobsChanged = new BehaviorSubject(<any>[]);
        this.onClustersChanged = new BehaviorSubject<any>([]);
        this.onClusterListUpdated = new BehaviorSubject<any>([]);
        this.onSelectedClusterChanged = new BehaviorSubject<any>({});
        this.onSearchTextChanged = new Subject();
        this.onSearchTextChanged.subscribe(searchText => {
            this.searchText = searchText;
        });
        this.onFilterChanged = new Subject();
        this.onFilterChanged.subscribe(filter => {
            this.filterBy = filter;
        });
        this.onStackOptionsChanged = new BehaviorSubject(<any>[]);
        this.getProviders();
        this.getClusters();
        this.updateClusterList();
        this.updateClusterStats();
        this.getSelectedCluster();
        this.getJobs();
        this.getStackOptions();

        this.refresh_timer = interval(5000).subscribe(x => {
            this.updateClusterStats();
        });
    }

    /**
     * getProviders
     */
    getProviders(): Promise<any> {
        return new Promise((resolve, reject) => {
            this._httpClient.get('api/test-drive')
                .subscribe((config: any) => {
                    this.providers = config.providers;
                    this.onProvidersChanged.next(this.providers);
                    resolve(this.providers);
                }, reject);
        });
    }

    /**
     * getStackOptions
     */
    getStackOptions(): Promise<any> {
        return new Promise((resolve, reject) => {
            this._httpClient.get('api/test-drive')
                .subscribe((config: any) => {
                    this.stackOptions = config.stackOptions;
                    this.onStackOptionsChanged.next(this.stackOptions);
                    resolve(this.stackOptions);
                }, reject);
        })
    }

    /**
     * getJobs
     */
    getJobs(): Promise<any> {
        return new Promise((resolve, reject) => {
            this._httpClient.get('api/test-drive')
                .subscribe((config: any) => {
                    this.jobs = config.jobs;
                    this.onJobsChanged.next(this.jobs);
                    resolve(this.jobs);
                }, reject);
        });
    }

    /**
     * getClusters
     */
    getClusters(): Promise<any> {
        return new Promise((resolve, reject) => {
            const user = {
                'user_token': localStorage.getItem('TOKEN')
            };

            this._httpClient.post(`${environment.apiURL}/getClusters`, user)
                .subscribe((response: any) => {
                    this.clusters = response.data;
                    this.onClustersChanged.next(this.clusters);
                    resolve(this.clusters);
                }, error => {
                    if (error.error.error) {
                        if (error.error.error.message == "invalid token") {
                            localStorage.removeItem('TOKEN');
                            window.location.reload();
                        }
                    }
                }, reject);
        });
    }

    updateClusterList(): Promise<any> {
        return new Promise((resolve, reject) => {
            const user = {
                'user_token': localStorage.getItem('TOKEN')
            };

            this._httpClient.post(`${environment.apiURL}/getClusters`, user)
                .subscribe((response: any) => {
                    this.clusterList = response.data;

                    this.clusterListAws = [];
                    this.clusterListGoogle = [];
                    this.clusterListAzure = [];
                    this.clusterListOracle = [];
                    this.clusterListHuawei = [];
                    this.clusterListOtc = [];

                    this.clusterList.forEach(function (cluster) {
                        switch (cluster.provider) {
                            case 'Amazon Web Services':
                                this.clusterListAws.push(cluster);
                                break;
                            case 'Google Cloud':
                                this.clusterListGoogle.push(cluster);
                                break;
                            case 'Microsoft Azure':
                                this.clusterListAzure.push(cluster);
                                break;
                            case 'Oracle Cloud':
                                this.clusterListOracle.push(cluster);
                                break;
                            case 'Huawei Cloud':
                                this.clusterListHuawei.push(cluster);
                                break;
                            case 'Open Telekom Cloud':
                                this.clusterListOtc.push(cluster);
                                break;
                            default:
                                break;
                        }
                    }.bind(this));

                    const clusters = {
                        'clusterList': this.clusterList,
                        'clusterListAws': this.clusterListAws,
                        'clusterListGoogle': this.clusterListGoogle,
                        'clusterListAzure': this.clusterListAzure,
                        'clusterListOracle': this.clusterListOracle,
                        'clusterListHuawei': this.clusterListHuawei,
                        'clusterListOtc': this.clusterListOtc
                    };

                    this.onClusterListUpdated.next(clusters);
                    resolve(this.clusterList);
                }, error => {
                    if (error.error.error) {
                        if (error.error.error.message == "invalid token") {
                            localStorage.removeItem('TOKEN');
                            window.location.reload();
                        }
                    }
                }, reject);
        });
    }

    getSelectedCluster(): Promise<any> {
        return new Promise((resolve, reject) => {
            const user = {
                'user_token': localStorage.getItem('TOKEN')
            };

            this._httpClient.post(`${environment.apiURL}/getSelectedCluster`, user)
                .subscribe((response: any) => {
                    this.selected_cluster = response.data;
                    this.onSelectedClusterChanged.next(this.selected_cluster);
                    resolve(this.selected_cluster);
                }, error => {
                    if (error.error.error) {
                        if (error.error.error.message == "invalid token") {
                            localStorage.removeItem('TOKEN');
                            window.location.reload();
                        }
                    }
                }, reject);
        });
    }

    updateClusterSelection(cluster): Promise<any> {
        return new Promise((resolve, reject) => {
            const data = {
                'user_token': localStorage.getItem('TOKEN'),
                'cluster': cluster
            };

            this._httpClient.post(`${environment.apiURL}/updateSelectedCluster`, data)
                .subscribe((response: any) => {
                    this.selected_cluster = response.data;
                    this.onSelectedClusterChanged.next(this.selected_cluster);
                    resolve(this.selected_cluster);
                }, error => {
                    if (error.error.error) {
                        if (error.error.error.message == "invalid token") {
                            localStorage.removeItem('TOKEN');
                            window.location.reload();
                        }
                    }
                }, reject);
        });
    }

    checkClusterName(cluster_name): Promise<any> {
        return new Promise((resolve, reject) => {
            const data = {
                'user_token': localStorage.getItem('TOKEN'),
                'cluster_name': cluster_name
            };

            this._httpClient.post(`${environment.apiURL}/checkClusterName`, data)
                .subscribe((response: any) => {
                    resolve(response.data);
                });
        });
    }

    createCluster(data): Promise<any> {
        return new Promise((resolve, reject) => {
            this._httpClient.post(`${environment.apiURL}/addCloudProvider`, data)
                .subscribe((response: any) => {
                    this.updateClusterList();
                });
            /*this.socket = socketIo.connect(`${environment.socketURL}`, {
                query: {
                    user_token: localStorage.getItem('TOKEN')
                }
            });
            this.socket.emit('provider-deploy-listener', {
                user_token: localStorage.getItem('TOKEN'),
                cluster_name: data.cluster_name
            });
            this.socket.on(data.cluster_name + '-deploy-finished', () => {
                this.updateClusterList();
                resolve();
                const title = data.cluster_name;
                const message = " was deployed.";
                this.showNotification(title + message, 5000, 'notification');
                //this.logActivity('cloud_provider', title, 'assets/images/providers/' + data.provider_config.provider + '.png', message);
            });*/
        });
    }

    createOnDemandCluster(data): Promise<any> {
        console.log("creating on demand cluster");
        console.log(data);
        return new Promise((resolve, reject) => {
            this._httpClient.post(`${environment.apiURL}/createOnDemandCluster`, data)
                .subscribe((response: any) => {
                    this.updateClusterList();
                });
            /*this.socket = socketIo.connect(`${environment.socketURL}`, {
                query: {
                    user_token: localStorage.getItem('TOKEN')
                }
            });
            this.socket.emit('provider-deploy-listener', {
                user_token: localStorage.getItem('TOKEN'),
                cluster_name: data.cluster_name
            });
            this.socket.on(data.cluster_name + '-deploy-finished', () => {
                this.updateClusterList();
                resolve();
                const title = data.cluster_name;
                const message = " was deployed.";
                this.showNotification(title + message, 5000, 'notification');
                //this.logActivity('cloud_provider', title, 'assets/images/providers/' + data.provider_config.provider + '.png', message);
            });*/
        });
    }

    redeployCluster(cluster): Promise<any> {
        return new Promise((resolve, reject) => {
            const data = {
                'user_token': localStorage.getItem('TOKEN'),
                'provider_id': cluster.provider_id,
                'cluster_id': cluster.id
            };

            this._httpClient.post(`${environment.apiURL}/redeployCluster`, data)
                .subscribe((response: any) => {
                    this.updateClusterList();
                    this.getSelectedCluster();
                });
        });
    }

    resizeCluster(cluster_size): Promise<any> {
        return new Promise((resolve, reject) => {
            const data = {
                'user_token': localStorage.getItem('TOKEN'),
                'cluster_type': this.selected_cluster.provider,
                'cluster_id': this.selected_cluster.id,
                'cluster_name': this.selected_cluster.name,
                'cluster_size': cluster_size
            };

            this._httpClient.post(`${environment.apiURL}/resizeCluster`, data)
                .subscribe((response: any) => {
                    this.updateClusterList();
                    this.getSelectedCluster();
                });
        });
    }

    destroyCluster(cluster): Promise<any> {
        return new Promise((resolve, reject) => {
            const data = {
                'user_token': localStorage.getItem('TOKEN'),
                'cluster_type': cluster.provider,
                'cluster_id': cluster.id,
                'cluster_name': cluster.name
            };

            this._httpClient.post(`${environment.apiURL}/destroyCluster`, data)
                .subscribe((response: any) => {
                    this.updateClusterList();
                    this.getSelectedCluster();
                });
        });
    }

    getClusterDeployLogs(cluster): Promise<any> {
        return new Promise((resolve, reject) => {
            const data = {
                'user_token': localStorage.getItem('TOKEN'),
                'provider_id': cluster.provider_id,
                'cluster_id': cluster.id
            };

            this._httpClient.post(`${environment.apiURL}/getClusterDeployLogs`, data)
                .subscribe((response: any) => {
                    resolve(response.data);
                });
        });
    }

    downloadClusterKey(cluster): Promise<any> {
        return new Promise((resolve, reject) => {
            const data = {
                'user_token': localStorage.getItem('TOKEN')
            };

            this._httpClient.post(`${environment.apiURL}/downloadClusterKey`, data)
                .subscribe((response: any) => {
                    let blob = new Blob([response.data]);
                    saveAs(blob, `${cluster.name}.pem`);
                });
        });
    }

    updateClusterStats(): Promise<any> {
        return new Promise((resolve, reject) => {
            const data = {
                'user_token': localStorage.getItem('TOKEN')
            };

            this._httpClient.post(`${environment.apiURL}/updateClusterStats`, data)
                .subscribe((response: any) => {

                });
        });
    }

    showNotification(message: string, duration: number, type: string): void {
        this.snackBar.open(message, 'Dismiss', {
            duration: duration,
            panelClass: [type],
            horizontalPosition: 'end',
            verticalPosition: 'bottom'
        });
    }

    logActivity(activity_type, activity_title, activity_icon, message) {
        const logActivity = {
            'user_token': localStorage.getItem('TOKEN'),
            'activity': {
                'type': activity_type,
                'title': {
                    'name': activity_title,
                    'icon': activity_icon
                },
                'message': message
            }
        };

        this._httpClient.post(`${environment.apiURL}/logActivity`, logActivity)
            .subscribe((response: any) => {
            });
    }
}
import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {fuseAnimations} from '@fuse/animations';
import {Subject} from 'rxjs';
import {ServerManagerService} from "../../server-manager.service";
import {MatTableDataSource, MatPaginator, MatSort} from '@angular/material';

@Component({
    selector: 'server-manager-dashboard-content',
    templateUrl: './server-manager-dashboard-content.component.html',
    styleUrls: ['./server-manager-dashboard-content.component.scss'],
    animations: fuseAnimations
})
export class ServerManagerDashboardContentComponent implements OnInit, OnDestroy {
    viewState: any;

    displayedColumns = [];
    dataSource: MatTableDataSource<any>;

    nodes: [];

    data: any;
    table_config: any;

    // Private
    private _unsubscribeAll: Subject<any>;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    constructor(
        private _serverManagerService: ServerManagerService
    ) {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
        this.viewState = "default";
        this.dataSource = new MatTableDataSource(this.nodes);
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.data = [
            {
                icon: '',
                node: 'aws-compute-node-1',
                cores: 4,
                state: 'Online',
                status: 'Busy'
            },
            {
                icon: '',
                node: 'aws-compute-node-2',
                cores: 2,
                state: 'Online',
                status: 'Busy'
            },
            {
                icon: '',
                node: 'aws-compute-node-3',
                cores: 2,
                state: 'Online',
                status: 'Busy'
            },
            {
                icon: '',
                node: 'aws-compute-node-4',
                cores: 2,
                state: 'Online',
                status: 'Busy'
            },
            {
                icon: '',
                node: 'aws-compute-node-5',
                cores: 2,
                state: 'Online',
                status: 'Busy'
            },
            {
                icon: '',
                node: 'aws-compute-node-6',
                cores: 2,
                state: 'Online',
                status: 'Busy'
            },
            {
                icon: '',
                node: 'aws-compute-node-7',
                cores: 2,
                state: 'Online',
                status: 'Busy'
            },
            {
                icon: '',
                node: 'aws-compute-node-8',
                cores: 2,
                state: 'Online',
                status: 'Busy'
            },
            {
                icon: '',
                node: 'aws-compute-node-9',
                cores: 2,
                state: 'Online',
                status: 'Busy'
            },
            {
                icon: '',
                node: 'aws-compute-node-10',
                cores: 2,
                state: 'Online',
                status: 'Busy'
            },
            {
                icon: '',
                node: 'aws-compute-node-11',
                cores: 2,
                state: 'Online',
                status: 'Busy'
            },
            {
                icon: '',
                node: 'aws-compute-node-12',
                cores: 2,
                state: 'Online',
                status: 'Busy'
            },
            {
                icon: '',
                node: 'aws-compute-node-13',
                cores: 2,
                state: 'Offline',
                status: '-'
            },
            {
                icon: '',
                node: 'aws-compute-node-14',
                cores: 2,
                state: 'Provisioning',
                status: '-'
            }
        ];

        this.table_config = {
            columns: [
                {
                    id: "icon",
                    name: ""
                },
                {
                    id: "node",
                    name: "Node"
                },
                {
                    id: "cores",
                    name: "Cores"
                },
                {
                    id: "state",
                    name: "State"
                },
                {
                    id: "status",
                    name: "Status"
                }
            ]
        };

        this.table_config.columns.forEach(function(column) {
            this.displayedColumns.push(column.id);
        }.bind(this));



        this.nodes = this.data;

        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.dataSource.data = this.nodes;
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
}

import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ServerManagerDashboardComponent} from "./server-manager-dashboard/server-manager-dashboard.comonent";
import {ServerManagerDashboardContentComponent} from "./server-manager-dashboard/server-manager-dashboard-content/server-manager-dashboard-content.component";
import {ServerManagerDetailsComponent} from "./server-manager-details/server-manager-details.component";
import {ServerManagerDetailsContentComponent} from "./server-manager-details/server-manager-details-content/server-manager-details-content.component";
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {FlexLayoutModule} from '@angular/flex-layout';

import {
    MatButtonModule,
    MatIconModule,
    MatMenuModule,
    MatTabsModule,
    MatTooltipModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule
} from '@angular/material';

const routes: Routes = [
    {
        path: '',
        component: ServerManagerDashboardComponent
    },
    {
        path: 'details',
        component: ServerManagerDetailsComponent
    }
];

@NgModule({
    declarations: [
        ServerManagerDetailsComponent,
        ServerManagerDetailsContentComponent,
        ServerManagerDashboardComponent,
        ServerManagerDashboardContentComponent
    ],
    imports: [
        RouterModule.forChild(routes),

        FlexLayoutModule,

        FormsModule,
        ReactiveFormsModule,
        CommonModule,

        MatButtonModule,
        MatIconModule,
        MatMenuModule,
        MatTabsModule,
        MatTooltipModule,
        MatTableModule,
        MatPaginatorModule,
        MatSortModule,
        MatFormFieldModule,
        MatInputModule,
        MatCardModule
    ]
})
export class ServerManagerModule {

}
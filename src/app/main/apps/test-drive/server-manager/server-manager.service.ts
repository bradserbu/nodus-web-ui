import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable()
export class ServerManagerService {
    viewState: any;
    onViewStateChanged: BehaviorSubject<any>;


    /**
     * Constructor
     *
     */
    constructor(

    ) {
        this.onViewStateChanged = new BehaviorSubject<any>("");
        this.viewState = "default";
        this.onViewStateChanged.next(this.viewState);
    }

    updateServerManagerViewState(state): Promise<any> {
        return new Promise((resolve, reject) => {
            this.viewState = state;
            this.onViewStateChanged.next(this.viewState);
            resolve(this.viewState);
        });
    }
}
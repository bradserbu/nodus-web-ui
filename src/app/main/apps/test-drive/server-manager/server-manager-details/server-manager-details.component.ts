import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {Subject} from 'rxjs';
import {fuseAnimations} from '@fuse/animations';
import {Router} from '@angular/router';
import {ServerManagerService} from "../server-manager.service";

@Component({
    selector: 'server-manager',
    templateUrl: './server-manager-details.component.html',
    styleUrls: ['./server-manager-details.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class ServerManagerDetailsComponent implements OnInit, OnDestroy {
    viewState: any;
    selected_node: any;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     */
    constructor(
        private router: Router,
        private _serverManagerService: ServerManagerService
    ) {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    /**
     * On init
     */
    ngOnInit(): void {
        this._serverManagerService.onViewStateChanged
            .subscribe((viewState) => {
               this.viewState = viewState;
            });

        this.selected_node = {
            'name': 'aws-compute-node-1',
            'cluster': 'Testcluster',
            'provider': 'Amazon Web Services',
            'icon': "/assets/images/providers/Amazon Web Services.png"
        };
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {

    }

    openClusterManagement(): void {
        this.router.navigateByUrl('/apps/nodus-cloud-platform/clusters');
    }
}

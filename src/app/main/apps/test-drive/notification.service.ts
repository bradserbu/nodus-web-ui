import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Observer} from 'rxjs/Observer';
import * as socketIo from 'socket.io-client';

import {Socket} from "./interfaces";

import {environment} from "../../../../environments/environment";

declare var io: {
    connect(url: string): Socket;
};

@Injectable()
export class NotificationService {
    notifications: any;
    notificationsHistory: any;
    onNotificationsChanged: BehaviorSubject<any>;
    onNotificationsHistoryChanged: BehaviorSubject<any>;
    currentSessionNotificationHistory: any;


    socket: Socket;
    observer: Observer<any>;

    getNotifications(): Observable<number> {
        this.socket = socketIo.connect(`${environment.socketURL}`, {
            query: {
                user_token: localStorage.getItem('TOKEN')
            }
        });
        this.socket.on('notification', (res) => {
            let isNewNotification = true;
            for (var i = 0; i < this. currentSessionNotificationHistory.length; i++) {
                if (this.currentSessionNotificationHistory[i].id == res.data.id)
                    isNewNotification = false;
            }
            if (isNewNotification) {
                this.currentSessionNotificationHistory.push(res.data);
                this.observer.next(res.data);
            }
        });

        return this.createObservable();
    }

    createObservable(): Observable<number> {
        return new Observable(observer => {
            this.observer = observer;
        });
    }

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    ) {
        // Set the defaults
        this.onNotificationsChanged = new BehaviorSubject<any>({});
        this.onNotificationsHistoryChanged = new BehaviorSubject<any>([]);
        this.currentSessionNotificationHistory = [];
        this.initializeNotifications();
        this.getNotificationsHistory();
    }

    /**
     * initializeNotifications
     */
    initializeNotifications(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.notifications = {
                counter: 0,
                color: "accent"
            };

            this.onNotificationsChanged.next(this.notifications);
            resolve(this.notifications);
        });
    }

    getNotificationsHistory(): Promise<any> {
        return new Promise((resolve, reject) => {
            const user = {
                'user_token': localStorage.getItem('TOKEN')
            };

            this._httpClient.post(`${environment.apiURL}/getNotifications`, user)
                .subscribe((response: any) => {
                    this.notificationsHistory = response.data;
                    this.onNotificationsHistoryChanged.next(this.notificationsHistory);
                    resolve(this.notificationsHistory);
                }, reject);
        });
    }

    logNotification(notification): Promise<any> {
        return new Promise((resolve, reject) => {
            const logNotification = {
                'user_token': localStorage.getItem('TOKEN'),
                'notification': notification
            };

            this._httpClient.post(`${environment.apiURL}/logNotification`, logNotification)
                .subscribe((response: any) => {
                    this.notifications = response.data;
                    this.onNotificationsHistoryChanged.next(this.notifications);
                    resolve(this.notifications);
                }, reject);
        });
    }

    /**
     * updateNotificationColor
     */
    updateNotificationColor(color): Promise<any> {
        return new Promise((resolve, reject) => {
            this.notifications.color = color;
        });
    }


    /**
     * incrementNotificationCounter
     */
    incrementNotificationCounter(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.notifications.counter++;
            this.onNotificationsChanged.next(this.notifications);
            resolve(this.notifications);
        });
    }

    /**
     * decrementNotificationCounter
     */
    decrementNotificationCounter(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.notifications.counter--;
            this.onNotificationsChanged.next(this.notifications);
            resolve(this.notifications);
        });
    }

    /**
     * clearNotificationCounter
     */
    clearNotificationCounter(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.notifications.counter = 0;
            this.notifications.color = "accent";
            this.onNotificationsChanged.next(this.notifications);
            resolve(this.notifications);
        });
    }


}
import {Injectable} from '@angular/core';
import {BehaviorSubject, Subject, interval} from 'rxjs';
import {HttpClient} from '@angular/common/http';

import {environment} from "../../../../environments/environment";

@Injectable()
export class ProcessesService {
    processes: any;
    onProcessesChanged: BehaviorSubject<any>;
    onSearchTextChanged: Subject<any>;
    onFilterChanged: Subject<any>;

    searchText: string;
    filterBy: string;

    refresh_timer: any;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    ) {
        this.onProcessesChanged = new BehaviorSubject<any>([]);
        this.onSearchTextChanged = new Subject();
        this.onSearchTextChanged.subscribe(searchText => {
            this.searchText = searchText;
        });
        this.onFilterChanged = new Subject();
        this.onFilterChanged.subscribe(filter => {
            this.filterBy = filter;
        });
        /*this.getActiveProcesses();

        this.refresh_timer = interval(1000).subscribe(x => {
            this.getActiveProcesses();
        });*/
    }

    /**
     * getActiveProcesses
     */
    getActiveProcesses(): Promise<any> {
        return new Promise((resolve, reject) => {
            const user = {
                'user_token': localStorage.getItem('TOKEN')
            };

            this._httpClient.post(`${environment.apiURL}/getActiveProcesses`, user)
                .subscribe((response: any) => {
                    this.processes = response.data;
                    this.onProcessesChanged.next(this.processes);
                    resolve(this.processes);
                }, error => {
                    if (error.error.error) {
                        if (error.error.error.message == "invalid token") {
                            localStorage.removeItem('TOKEN');
                            window.location.reload();
                        }
                    }
                }, reject);
        });
    }
}
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {StacksComponent} from "./stacks.component";
import {StacksListComponent} from "./stacks-list/stacks-list.component";
import {StacksSidebarComponent} from "./stacks-sidebar/stacks-sidebar.component";
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { CommonModule } from '@angular/common';
import {FlexLayoutModule} from '@angular/flex-layout'

import {
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatFormFieldModule,
    MatTableModule,
    MatCardModule,
    MatDialogModule,
    MatMenuModule,
    MatSnackBarModule,
    MatSortModule,
    MatTooltipModule
} from '@angular/material';

const routes: Routes = [
    {
        path: '**',
        component: StacksComponent
    }
];

@NgModule({
    declarations: [
        StacksComponent,
        StacksListComponent,
        StacksSidebarComponent
    ],
    imports: [
        RouterModule.forChild(routes),

        FlexLayoutModule,

        FormsModule,
        ReactiveFormsModule,
        CommonModule,

        MatButtonModule,
        MatIconModule,
        MatInputModule,
        MatFormFieldModule,
        MatTableModule,
        MatCardModule,
        MatDialogModule,
        MatMenuModule,
        MatSnackBarModule,
        MatSortModule,
        MatTooltipModule
    ]
})
export class StacksModule
{

}
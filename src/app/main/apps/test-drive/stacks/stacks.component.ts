import {Component, ViewEncapsulation, OnInit, OnDestroy} from '@angular/core';
import {fuseAnimations} from '@fuse/animations';
import {FormControl} from '@angular/forms';
import {Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged, takeUntil} from 'rxjs/operators';
import {Router} from '@angular/router';
import {MatDialog} from '@angular/material';
import {StacksService} from "../stacks.service";


/**
 * @title Data table with sorting, pagination, and filtering.
 */
@Component({
    selector: 'stacks',
    templateUrl: './stacks.component.html',
    styleUrls: ['./stacks.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})

export class StacksComponent implements OnInit, OnDestroy{
    searchInput: FormControl;

    dialogRef: any;

    private _unsubscribeAll: Subject<any>;

    constructor(
        private _matDialog: MatDialog,
        private _stacksService: StacksService,
        private router: Router
    ) {
        this.searchInput = new FormControl('');
        this._unsubscribeAll = new Subject();
    }

    ngOnInit(): void {
        this.searchInput.valueChanges
            .pipe(takeUntil(this._unsubscribeAll),
                debounceTime(300),
                distinctUntilChanged()
            )
            .subscribe(searchText => {
                this._stacksService.onSearchTextChanged.next(searchText);
            });
    }

    openClusterManagement(): void {
        this.router.navigateByUrl('/apps/nodus-cloud-platform/clusters');
    }

    ngOnDestroy(): void {
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

}

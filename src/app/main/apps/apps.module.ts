import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TestDriveModule } from 'app/main/apps/test-drive/test-drive.module';

const routes = [
    {
        path: 'nodus-cloud-platform',
        loadChildren: './test-drive/test-drive.module#TestDriveModule'
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        // TestDrive
        TestDriveModule
    ]
})
export class AppsModule
{

}

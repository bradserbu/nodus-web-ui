import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {RouterModule, Routes} from '@angular/router';
import {MatMomentDateModule} from '@angular/material-moment-adapter';
import {MatButtonModule, MatIconModule} from '@angular/material';
import {InMemoryWebApiModule} from 'angular-in-memory-web-api';
import {TranslateModule} from '@ngx-translate/core';
import 'hammerjs';

import {FuseModule} from '@fuse/fuse.module';
import {FuseSharedModule} from '@fuse/shared.module';
import {FuseProgressBarModule, FuseSidebarModule, FuseThemeOptionsModule} from '@fuse/components';

import {fuseConfig} from 'app/fuse-config';

import {DbService} from 'app/db/db.service';
import {AppComponent} from 'app/app.component';
import {LayoutModule} from 'app/layout/layout.module';


import {AuthGuard} from "./main/authentication/login-auth.guard";
import {NotificationService} from "./main/apps/test-drive/notification.service";
import {ProcessesService} from "./main/apps/test-drive/processes.service";
import {ClustersService} from "./main/apps/test-drive/clusters.services";
import {ServerManagerService} from "./main/apps/test-drive/server-manager/server-manager.service";
import {StacksService} from "./main/apps/test-drive/stacks.service";


const appRoutes: Routes = [
    {
        path: 'apps',
        canActivate: [AuthGuard],
        loadChildren: './main/apps/apps.module#AppsModule'
    },
    {
        path: 'auth',
        loadChildren: './main/authentication/authentication.module#AuthenticationModule'
    },
    {
        path: '**',
        redirectTo: 'apps/nodus-cloud-platform/clusters'
    }
];

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        RouterModule.forRoot(appRoutes),

        TranslateModule.forRoot(),
        InMemoryWebApiModule.forRoot(DbService, {
            delay: 0,
            passThruUnknownUrl: true
        }),

        // Material moment date module
        MatMomentDateModule,

        // Material
        MatButtonModule,
        MatIconModule,

        // Fuse modules
        FuseModule.forRoot(fuseConfig),
        FuseProgressBarModule,
        FuseSharedModule,
        FuseSidebarModule,
        FuseThemeOptionsModule,

        // App modules
        LayoutModule
    ],
    providers: [
        AuthGuard,
        NotificationService,
        ProcessesService,
        ClustersService,
        ServerManagerService,
        StacksService
    ],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule {
}

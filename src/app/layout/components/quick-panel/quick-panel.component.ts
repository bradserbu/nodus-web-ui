import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {NotificationService} from "../../../main/apps/test-drive/notification.service";

@Component({
    selector: 'quick-panel',
    templateUrl: './quick-panel.component.html',
    styleUrls: ['./quick-panel.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class QuickPanelComponent implements OnInit {
    date: Date;
    events: any[];
    notes: any[];
    settings: any;

    notifications: NotificationData[] = [];

    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     */
    constructor(
        private _notificationService: NotificationService
    ) {
        if (localStorage.getItem('NOTIFICATION') == null)
            localStorage.setItem('NOTIFICATION', 'true');

        // Set the defaults
        this.date = new Date();
        this.settings = {
            notify: (localStorage.getItem('NOTIFICATION') == 'true'),
            cloud: false,
            retro: true
        };

        this._unsubscribeAll = new Subject();
    }

    ngOnInit() {
        const user = {
            'user_token': localStorage.getItem('TOKEN')
        };

        this._notificationService.onNotificationsHistoryChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(notifications => {
               this.notifications = notifications;
               this.notifications.reverse();
            });
    }

    updateNotificationSettings(): void {
        if (localStorage.getItem('NOTIFICATION') == 'true')
            localStorage.setItem('NOTIFICATION', 'false');
        else
            localStorage.setItem('NOTIFICATION', 'true');
    }
}

export interface NotificationData {
    id: string;
    type: string;
    title: string;
    subtitle: string;
    message: string;
    actions: [];
    date: string;
}

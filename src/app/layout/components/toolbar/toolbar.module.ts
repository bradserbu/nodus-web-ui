import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {
    MatButtonModule,
    MatDatepickerModule,
    MatTooltipModule,
    MatDialogModule,
    MatSnackBarModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatMenuModule,
    MatToolbarModule,
    MatBadgeModule
} from '@angular/material';

import {FuseSearchBarModule, FuseShortcutsModule} from '@fuse/components';
import {FuseSharedModule} from '@fuse/shared.module';

import {ToolbarComponent} from 'app/layout/components/toolbar/toolbar.component';
import {ProfileFormDialogComponent} from './dialogs/profile-form/profile-form.component';
import {FuseConfirmDialogComponent2} from "../../../../@fuse/components/confirm-dialog-2/confirm-dialog.component";

@NgModule({
    declarations: [
        ToolbarComponent,
        ProfileFormDialogComponent,
        FuseConfirmDialogComponent2
    ],
    imports: [
        RouterModule,
        MatButtonModule,
        MatDatepickerModule,
        MatTooltipModule,
        MatDialogModule,
        MatSnackBarModule,
        MatFormFieldModule,
        MatInputModule,
        MatIconModule,
        MatMenuModule,
        MatToolbarModule,
        MatBadgeModule,

        FuseSharedModule,
        FuseSearchBarModule,
        FuseShortcutsModule
    ],
    exports: [
        ToolbarComponent
    ],
    entryComponents: [
        ProfileFormDialogComponent,
        FuseConfirmDialogComponent2
    ]
})
export class ToolbarModule {
}

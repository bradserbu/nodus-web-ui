import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import * as _ from 'lodash';

import { FuseConfigService } from '@fuse/services/config.service';
import { FuseSidebarService } from '@fuse/components/sidebar/sidebar.service';

import { navigation } from 'app/navigation/navigation';

import {ProfileFormDialogComponent} from "./dialogs/profile-form/profile-form.component";
import {AuthenticationService} from "../../../main/authentication/authentication.service";
import {MatDialog} from '@angular/material';

import {NotificationService} from "../../../main/apps/test-drive/notification.service";
import {ProcessesService} from "../../../main/apps/test-drive/processes.service";
import {Router} from '@angular/router';
import {ServerManagerService} from "../../../main/apps/test-drive/server-manager/server-manager.service";

@Component({
    selector     : 'toolbar',
    templateUrl  : './toolbar.component.html',
    styleUrls    : ['./toolbar.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class ToolbarComponent implements OnInit, OnDestroy
{
    viewState: any;

    horizontalNavbar: boolean;
    rightNavbar: boolean;
    hiddenNavbar: boolean;
    languages: any;
    navigation: any;
    selectedLanguage: any;
    userStatusOptions: any[];

    dialogRef: any;
    user: any;

    notifications: any;
    processes: any;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param {FuseSidebarService} _fuseSidebarService
     * @param {TranslateService} _translateService
     */
    constructor(
        private _serverManagerService: ServerManagerService,
        private _fuseConfigService: FuseConfigService,
        private _fuseSidebarService: FuseSidebarService,
        private _translateService: TranslateService,
        private _matDialog: MatDialog,
        private _authenticationService: AuthenticationService,
        private _notificationService: NotificationService,
        private _processesService: ProcessesService,
        private router: Router
    )
    {
        // Set the defaults
        this.userStatusOptions = [
            {
                'title': 'Online',
                'icon' : 'icon-checkbox-marked-circle',
                'color': '#4CAF50'
            },
            {
                'title': 'Away',
                'icon' : 'icon-clock',
                'color': '#FFC107'
            },
            {
                'title': 'Do not Disturb',
                'icon' : 'icon-minus-circle',
                'color': '#F44336'
            },
            {
                'title': 'Invisible',
                'icon' : 'icon-checkbox-blank-circle-outline',
                'color': '#BDBDBD'
            },
            {
                'title': 'Offline',
                'icon' : 'icon-checkbox-blank-circle-outline',
                'color': '#616161'
            }
        ];

        this.languages = [
            {
                id   : 'en',
                title: 'English',
                flag : 'us'
            },
            {
                id   : 'tr',
                title: 'Turkish',
                flag : 'tr'
            }
        ];

        this.navigation = navigation;

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        this._serverManagerService.onViewStateChanged
            .subscribe((viewState) => {
                this.viewState = viewState;
            });

        // Subscribe to the config changes
        this._fuseConfigService.config
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((settings) => {
                this.horizontalNavbar = settings.layout.navbar.position === 'top';
                this.rightNavbar = settings.layout.navbar.position === 'right';
                this.hiddenNavbar = settings.layout.navbar.hidden === true;
            });

        this._notificationService.onNotificationsChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(notifications => {
                this.notifications = notifications;
                this.notifications = notifications;
            });

        this._processesService.onProcessesChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(processes => {
                this.processes = processes;
            });

        // Set the selected language from default languages
        this.selectedLanguage = _.find(this.languages, {'id': this._translateService.currentLang});

        this.setUser();
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Toggle sidebar open
     *
     * @param key
     */
    toggleSidebarOpen(key): void
    {
        this._fuseSidebarService.getSidebar(key).toggleOpen();
        this._notificationService.clearNotificationCounter();

    }

    /**
     * Search
     *
     * @param value
     */
    search(value): void
    {
        // Do your search here...
        console.log(value);
    }

    /**
     * Set the language
     *
     * @param lang
     */
    setLanguage(lang): void
    {
        // Set the selected language for the toolbar
        this.selectedLanguage = lang;

        // Use the selected language for translations
        this._translateService.use(lang.id);
    }

    /**
     * setUser
     */
    setUser(): void {
        const token = localStorage.getItem('TOKEN');
        if(token != null) {
            var base64Url = token.split('.')[1];
            var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
            this.user = JSON.parse(window.atob(base64));
        }
        else {
            this.user = {};
        }
    }

    /**
     * Open Profile
     */
    openProfile(): void {

        this.dialogRef = this._matDialog.open(ProfileFormDialogComponent, {
            panelClass: 'profile-form-dialog',
            data: {
                action: 'edit'
            }
        });
    }


    logout(): void {
        this._authenticationService.removeToken();
        window.location.reload();
    }

    openProcessesView(): void {
        this.router.navigateByUrl('/apps/nodus-cloud-platform/processes');
    }
}

import {Component, Inject, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatSnackBar} from '@angular/material';
import {Subject} from 'rxjs';
import {HttpClient} from '@angular/common/http';

import {Profile} from './profile.model';
import {FuseConfirmDialogComponent2} from "../../../../../../@fuse/components/confirm-dialog-2/confirm-dialog.component";
import {Router} from '@angular/router';
import {AuthenticationService} from "../../../../../main/authentication/authentication.service";

import {environment} from "../../../../../../environments/environment";

@Component({
    selector: 'profile-form-dialog',
    templateUrl: './profile-form.component.html',
    styleUrls: ['./profile-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class ProfileFormDialogComponent {
    action: string;
    user: Profile;
    profileForm: FormGroup;
    dialogTitle: string;
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent2>;

    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {MatDialogRef<ProfileFormDialogComponent>} matDialogRef
     * @param _data
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        public _matDialog: MatDialog,
        public matDialogRef: MatDialogRef<ProfileFormDialogComponent>,
        @Inject(MAT_DIALOG_DATA) private _data: any,
        private _formBuilder: FormBuilder,
        private _httpClient: HttpClient,
        public snackBar: MatSnackBar,
        private _authenticationService: AuthenticationService,
        private router: Router
    ) {
        this.getUser();

        // Set the defaults
        this.action = _data.action;
        this._unsubscribeAll = new Subject();

        if (this.action === 'edit') {
            this.dialogTitle = 'Edit Profile';
        }
        else {
            this.dialogTitle = 'Edit Profile';
            this.user = new Profile({});
        }

        this.profileForm = this.createProfileForm();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Get user
     */
    getUser(): Promise<any[]> {
        return new Promise((resolve, reject) => {
            const token = localStorage.getItem('TOKEN');
            var base64Url = token.split('.')[1];
            var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
            this.user = JSON.parse(window.atob(base64));
            resolve();
        });
    }


    /**
     * createProfileForm
     *
     * @returns {FormGroup}
     */
    createProfileForm(): FormGroup {
        return this._formBuilder.group({
            id: [{value: this.user.username, disabled: true}],
            name: [this.user.name],
            lastName: [this.user.lastName],
            avatar: [this.user.avatar],
            //nickname: [this.user.nickname],
            company: [this.user.company],
            jobTitle: [this.user.jobTitle],
            email: [this.user.email],
            phone: [this.user.phone],
            /*address : [this.user.address],
            birthday: [this.user.birthday],
            notes   : [this.user.notes]*/
        });
    }

    onFileSelected(event) {
        const reader = new FileReader();
        reader.onload = function () {
            var data = reader.result;
            this._updateProfilePicture(data);
        }.bind(this);
        reader.readAsDataURL(event.target.files[0]);
    }

    /**
     * updateProfilePicture
     */
    _updateProfilePicture(image_data) {
        const uploadData = {
            'user_token': localStorage.getItem('TOKEN'),
            'image': image_data
        };

        this._httpClient.post(`${environment.apiURL}/updateProfilePicture`, uploadData)
            .subscribe((response: any) => {
                localStorage.setItem('TOKEN', response.data);
                this.showNotification('Profile Picture updated.', 5000, 'notification');
                this.logActivity('profile', "Profile Picture", 'assets/images/avatars/profile.jpg', " was updated.");
            }, error => {
                this.showNotification('ERROR: Profile Picture Update was Unsuccessful.', 60000, 'warning');
            });
    }

    /**
     * updateProfileInformation
     */
    updateProfileInformation(): void {
        const user = {
            'user_token': localStorage.getItem('TOKEN'),
            'name': this.profileForm.value.name,
            'lastName': this.profileForm.value.lastName,
            'company': this.profileForm.value.company,
            'jobTitle': this.profileForm.value.jobTitle,
            'email': this.profileForm.value.email,
            'phone': this.profileForm.value.phone
        };

        this._httpClient.post(`${environment.apiURL}/updateProfileInformation`, user)
            .subscribe((response: any) => {
                localStorage.setItem('TOKEN', response.data);
                //window.dispatchEvent( new Event('storage') );
                this.matDialogRef.close();
                this.showNotification('Profile Information updated.', 5000, 'notification');
                this.logActivity('profile', "Profile Information", 'assets/images/avatars/profile.jpg', " was updated.");
            }, error => {
                this.showNotification('ERROR: Profile Information Update was Unsuccessful.', 60000, 'warning');
            });
    }

    /**
     * deleteProfile
     */
    deleteProfile(): void {
        this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent2, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete your account from the system? This action cannot be undone.';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                const token = {
                    'token': localStorage.getItem('TOKEN')
                };

                this._httpClient.post(`${environment.apiURL}/deleteUser`, token)
                    .subscribe((response: any) => {
                        this.matDialogRef.close();
                        this._authenticationService.removeToken();
                        this.router.navigateByUrl('auth/login');
                    }, error => {
                        this.showNotification('ERROR: Account Deletion was Unsuccessful.', 60000, 'warning');
                    });
            }
            this.confirmDialogRef = null;
        });
    }

    showNotification(message: string, duration: number, type: string): void {
        this.snackBar.open(message, 'Dismiss', {
            duration: duration,
            panelClass: [type],
            horizontalPosition: 'end',
            verticalPosition: 'bottom'
        });
    }

    logActivity(activity_type, activity_title, activity_icon, message) {
        const logActivity = {
            'user_token': localStorage.getItem('TOKEN'),
            'activity': {
                'type': activity_type,
                'title': {
                    'name': activity_title,
                    'icon': activity_icon
                },
                'message': message
            }
        };

        this._httpClient.post(`${environment.apiURL}/logActivity`, logActivity)
            .subscribe((response: any) => {
            });
    }
}

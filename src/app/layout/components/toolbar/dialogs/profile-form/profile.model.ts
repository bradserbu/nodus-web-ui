import { FuseUtils } from '@fuse/utils';

export class Profile
{
    username: string;
    name: string;
    lastName: string;
    avatar: string;
    nickname: string;
    company: string;
    jobTitle: string;
    email: string;
    phone: string;
    address: string;
    birthday: string;
    notes: string;

    /**
     * Constructor
     *
     * @param contact
     */
    constructor(profile)
    {
        {
            this.username = profile.id || FuseUtils.generateGUID();
            this.name = profile.name || '';
            this.lastName = profile.lastName || '';
            this.avatar = profile.avatar || 'assets/images/avatars/profile.jpg';
            this.nickname = profile.nickname || '';
            this.company = profile.company || '';
            this.jobTitle = profile.jobTitle || '';
            this.email = profile.email || '';
            this.phone = profile.phone || '';
            this.address = profile.address || '';
            this.birthday = profile.birhday || '';
            this.notes = profile.notes || '';
        }
    }
}

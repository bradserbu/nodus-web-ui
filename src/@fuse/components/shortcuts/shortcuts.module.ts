import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatToolbarModule, MatCardModule, MatRadioModule, MatSliderModule, MatSlideToggleModule, MatCheckboxModule, MatButtonModule, MatDividerModule, MatFormFieldModule, MatIconModule, MatInputModule, MatListModule, MatMenuModule, MatTooltipModule } from '@angular/material';
import { CookieService } from 'ngx-cookie-service';

import { FuseShortcutsComponent } from './shortcuts.component';
import {ToolbarService} from "../../../app/main/apps/test-drive/toolbar-service";
import {SubmitJobShortcutDialogComponent} from "./dialogs/submit-job/submit-job-shortcut.component";
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
    declarations: [
        FuseShortcutsComponent,
        SubmitJobShortcutDialogComponent
    ],
    imports     : [
        CommonModule,
        RouterModule,

        FlexLayoutModule,
        ReactiveFormsModule,
        FormsModule,

        MatToolbarModule,
        MatCardModule,
        MatRadioModule,
        MatSliderModule,
        MatSlideToggleModule,
        MatCheckboxModule,
        MatButtonModule,
        MatDividerModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatListModule,
        MatTooltipModule
    ],
    exports     : [
        FuseShortcutsComponent
    ],
    providers   : [
        CookieService,
        ToolbarService
    ],
    entryComponents : [
        SubmitJobShortcutDialogComponent
    ]
})
export class FuseShortcutsModule
{
}

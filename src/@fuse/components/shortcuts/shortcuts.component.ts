import {Component, ElementRef, Input, OnDestroy, OnInit, Renderer2, ViewChild} from '@angular/core';
import {ObservableMedia} from '@angular/flex-layout';
import {CookieService} from 'ngx-cookie-service';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {FuseMatchMediaService} from '@fuse/services/match-media.service';
import {FuseNavigationService} from '@fuse/components/navigation/navigation.service';
import {ToolbarService} from "../../../app/main/apps/test-drive/toolbar-service";
import {FuseProgressBarService} from "../progress-bar/progress-bar.service";
import {HttpClient} from '@angular/common/http';
import {MatSnackBar} from '@angular/material';
import {SubmitJobShortcutDialogComponent} from "./dialogs/submit-job/submit-job-shortcut.component";
import {MatDialog} from '@angular/material';

import {environment} from "../../../environments/environment";

@Component({
    selector: 'fuse-shortcuts',
    templateUrl: './shortcuts.component.html',
    styleUrls: ['./shortcuts.component.scss']
})
export class FuseShortcutsComponent implements OnInit, OnDestroy {
    dialogRef: any;

    shortcutItems: any[];
    navigationItems: any[];
    filteredNavigationItems: any[];
    searching: boolean;
    mobileShortcutsPanelActive: boolean;

    @Input()
    navigation: any;

    @ViewChild('searchInput')
    searchInputField;

    @ViewChild('shortcuts')
    shortcutsEl: ElementRef;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {Renderer2} _renderer
     * @param {CookieService} _cookieService
     * @param {FuseMatchMediaService} _fuseMatchMediaService
     * @param {FuseNavigationService} _fuseNavigationService
     * @param {ObservableMedia} _observableMedia
     */
    constructor(
        private _matDialog: MatDialog,
        private _cookieService: CookieService,
        private _fuseMatchMediaService: FuseMatchMediaService,
        private _fuseNavigationService: FuseNavigationService,
        private _observableMedia: ObservableMedia,
        private _renderer: Renderer2,
        private _toolbarService: ToolbarService,
        private _httpClient: HttpClient,
        public snackBar: MatSnackBar,
        private _fuseProgressBarService: FuseProgressBarService
    ) {
        // Set the defaults
        this.shortcutItems = [];
        this.searching = false;
        this.mobileShortcutsPanelActive = false;

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // Get the navigation items and flatten them
        //this.filteredNavigationItems = this.navigationItems = this._fuseNavigationService.getFlatNavigation(this.navigation);


        this._toolbarService.onShortcutItemsChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(shortcuts => {
                this.shortcutItems = shortcuts;
            });

        this._fuseMatchMediaService.onMediaChange
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(() => {
                if (this._observableMedia.isActive('gt-sm')) {
                    this.hideMobileShortcutsPanel();
                }
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Search
     *
     * @param event
     */
    search(event): void {
        const value = event.target.value.toLowerCase();

        this.navigationItems = this.shortcutItems;

        if (value === '') {
            this.searching = false;
            this.filteredNavigationItems = this.navigationItems;

            return;
        }

        this.searching = true;

        this.filteredNavigationItems = this.navigationItems.filter((navigationItem) => {
            return navigationItem.title.toLowerCase().includes(value);
        });
    }

    /**
     * Toggle shortcut
     *
     * @param event
     * @param itemToToggle
     */
    removeShortcut(event, job, shortcut_item): boolean {
        const shortcut = {
            'user_token': localStorage.getItem('TOKEN'),
            'item_index': shortcut_item
        };

        this._httpClient.post(`${environment.apiURL}/removeShortcutItem`, shortcut)
            .subscribe((response: any) => {
		const job_title = "'" + job.job_name + "'";
		const message = " was removed from the quick launch list.";
		this.showNotification(job_title + message, 5000, 'notification');
		this.logActivity('job-quick-launch', job_title, 'assets/images/test-drive/quick-launch.png', message);

                const user = {
                    'user_token': localStorage.getItem('TOKEN')
                };

                this._httpClient.post(`${environment.apiURL}/getShortcutItems`, user)
                    .subscribe((response: any) => {
                        this.shortcutItems = response.data.shortcuts;
                    });
            }, error => {

            });

	return false;
    }

    /**
     * Is in shortcuts?
     *
     * @param navigationItem
     * @returns {any}
     */
    isInShortcuts(navigationItem): any {
        return this.shortcutItems.find(item => {
            return item.url === navigationItem.url;
        });
    }

    /**
     * On menu open
     */
    onMenuOpen(): void {
        setTimeout(() => {
            this.searchInputField.nativeElement.focus();
        });
    }

    /**
     * Show mobile shortcuts
     */
    showMobileShortcutsPanel(): void {
        this.mobileShortcutsPanelActive = true;
        this._renderer.addClass(this.shortcutsEl.nativeElement, 'show-mobile-panel');
    }

    /**
     * Hide mobile shortcuts
     */
    hideMobileShortcutsPanel(): void {
        this.mobileShortcutsPanelActive = false;
        this._renderer.removeClass(this.shortcutsEl.nativeElement, 'show-mobile-panel');
    }

    submitJob(event, job): void {

        this.dialogRef = this._matDialog.open(SubmitJobShortcutDialogComponent, {
            panelClass: 'submit-job-form-dialog',
            data: {
                action: 'shortcut-job',
                job: job
            }
        });

        /*this._fuseProgressBarService.show();
        const data = {
            'user_token': localStorage.getItem('TOKEN'),
            'job': job
        };

        this._httpClient.post(`${environment.apiURL}/submitJob`, data)
            .subscribe((response: any) => {
                this._fuseProgressBarService.hide();
                const job_title = response.data.job_name + " (ID: " + response.data.job_id + ")";
                const message = " was submitted to the workload.";
                this.showNotification(job_title + message, 5000, 'notification');
                //this.logActivity('job', job_title, 'assets/images/test-drive/job.png', message);
            }, error => {
                this.showNotification('ERROR: Job Submit Request was Unsuccessful.', 60000, 'warning');
            });*/
    }

    showNotification(message: string, duration: number, type: string): void {
        this.snackBar.open(message, 'Dismiss', {
            duration: duration,
            panelClass: [type],
            horizontalPosition: 'end',
            verticalPosition: 'bottom'
        });
    }

    logActivity(activity_type, activity_title, activity_icon, message) {
        const logActivity = {
            'user_token': localStorage.getItem('TOKEN'),
            'activity': {
                'type': activity_type,
                'title': {
                    'name': activity_title,
                    'icon': activity_icon
                },
                'message': message
            }
        };

        this._httpClient.post(`${environment.apiURL}/logActivity`, logActivity)
            .subscribe((response: any) => {
            });
    }
}

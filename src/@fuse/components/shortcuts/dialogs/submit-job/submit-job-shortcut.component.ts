import {Component, OnInit, OnDestroy, Inject, ViewEncapsulation} from '@angular/core';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef, MatSnackBar} from '@angular/material';
import {MatDialog} from '@angular/material';
//import {TestDriveService} from "../../test-drive.service";
import {HttpClient} from '@angular/common/http';
import {FuseProgressBarService} from "../../../progress-bar/progress-bar.service";
import {ClustersService} from "../../../../../app/main/apps/test-drive/clusters.services";
//import {CloudProviderFormDialogComponent} from "../cloud-provider-form/cloud-provider-form.component";

import {environment} from "../../../../../environments/environment";

@Component({
    selector: 'submit-job-shortcut-dialog',
    templateUrl: './submit-job-shortcut.component.html',
    styleUrls: ['./submit-job-shortcut.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class SubmitJobShortcutDialogComponent implements OnInit, OnDestroy {
    action: string;
    job: any;
    clusterConfigurationForm: FormGroup;
    dialogTitle: string;
    cluster_configuration: any;
    formState: string;

    dialogRef: any;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {MatDialogRef<CloudProviderFormDialogComponent>} matDialogRef
     * @param _data
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        private _matDialog: MatDialog,
        public matDialogRef: MatDialogRef<SubmitJobShortcutDialogComponent>,
        public snackBar: MatSnackBar,
        @Inject(MAT_DIALOG_DATA) private _data: any,
        private _formBuilder: FormBuilder,
        private _clustersService: ClustersService,
        private _httpClient: HttpClient,
        private _fuseProgressBarService: FuseProgressBarService
    ) {
        this._unsubscribeAll = new Subject();
        this.action = _data.action;
        this.job = _data.job;
        this.formState = "cluster-config";

        if (this.action === 'shortcut-job') {
            this.dialogTitle = 'Submit Job - Cluster Selection';
        }
    }

    /**
     * On init
     */
    ngOnInit(): void {
        this._clustersService.getClusters()
            .then(clusters => {
                this.cluster_configuration = clusters;
            });

        this.clusterConfigurationForm = this.createClusterConfigurationForm();
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------


    createClusterConfigurationForm(): FormGroup {
        return new FormGroup({
            clusters: new FormControl('', Validators.required)
        });
    }

    /**
     * submitJob
     */
    submitJob(): void {
        this.matDialogRef.close();

        const job_object = this.job;

        if (this.clusterConfigurationForm.value.clusters == "new-cluster") {
            /*this.dialogRef = this._matDialog.open(CloudProviderFormDialogComponent, {
                panelClass: 'cloud-provider-form-dialog',
                data: {
                    action: 'on-demand',
                    job: job_object
                }
            });*/
        } else {
            this._fuseProgressBarService.show();

            const data = {
                'user_token': localStorage.getItem('TOKEN'),
                'job': job_object,
                'cluster': this.clusterConfigurationForm.value.clusters
            };

            this._httpClient.post(`${environment.apiURL}/submitJob`, data)
                .subscribe((response: any) => {
                    this._fuseProgressBarService.hide();
                    const job_title = response.data.job_name + " (ID: " + response.data.job_id + ")";
                    const message = " was submitted to the workload.";
                    this.showNotification(job_title + message, 5000, 'notification');
                }, error => {
                    this._fuseProgressBarService.hide();
                    this.showNotification('ERROR: Job Submit Request was Unsuccessful.', 60000, 'warning');
                });
        }
    }

    showNotification(message: string, duration: number, type: string): void {
        this.snackBar.open(message, 'Dismiss', {
            duration: duration,
            panelClass: [type],
            horizontalPosition: 'end',
            verticalPosition: 'bottom'
        });
    }
}

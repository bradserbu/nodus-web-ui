import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'secondsToDateTime'})

export class SecondsToDateTimePipe implements PipeTransform
{
    transform(seconds)
    {
        var date = new Date(null);
        date.setSeconds(seconds);
        var result = date.toISOString().substr(11, 8);
        return result;
    }
}

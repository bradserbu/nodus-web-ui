export const environment = {
    production: true,
    hmr       : false,
    apiURL: 'http://localhost:3000/api',
    socketURL: 'http://localhost:3000/socket'
};
